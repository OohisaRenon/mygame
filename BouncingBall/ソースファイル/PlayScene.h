#include "Engine/global.h"
#include <vector>

class Map;
class Player;
class BackGround;
const int TIME_PICT_MAX = 10;	//タイマー用UI画像の枚数
const int SAVE_DATA_MAX = 5;	//保存するクリアタイムのデータ数(何位まで保存するか)

//何桁目(どの位の数字を出すか)
enum timeTable
{
	MINUTE_TABLE,		//x<-ここ : xx 
	TEN_SECOND_TABLE,	//x : x<-ここx 
	ONE_SECOND_TABLE,	//x : xx<-ここ
	TIME_TABLE_MAX,
};

//ゲームの状態
enum gameState
{
	STATE_PLAY,
	STATE_CLEAR,
	STATE_RESULT,
	STATE_MAX
};

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	int hPictClear_;		//クリア時に表示する画像のハンドル
	int hPictColon_;		//タイマー用UI画像「:」のハンドル
	int hPictTime_[TIME_TABLE_MAX][TIME_PICT_MAX];	//どのタイマー用UI画像のハンドル
	int hPictNewRecord_;	//NewRecordの画像データのハンドル
	int targetCnt_;	//現在プレイシーンに存在しているターゲットの数
	bool canPlaySE_;//クリア時のSEを鳴らしてよいかどうか
	time_t start_;	//計測開始時のクロック
	time_t end_;	//計測終了時のクロック
	double time_;	//ゲームのタイマー
	double clearTimeData_[SAVE_DATA_MAX];	//テキストから読み込んだクリアタイムのtop5
	gameState state_;		//ゲームの状態(プレイ中か、クリアか、リザルトか)
	D3DXCOLOR pictColor_;	//画像の色変更用
	Point targetPos_;		//的の生成位置

	Map* pMap_;
	Player* pPlayer_;
	BackGround* pBackGround_;
	

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//テキストから読み込んだクリアタイムを表示
	//引数 : rank 何位まで表示するか(ループ回数の上限)
	//※変更したいときは SAVE_DATA_MAX を変更
	void DrawClearTime(int rank);

	//タイマーUIの描画
	void DrawTime();

	//開放
	void Release() override;

	//ゲームクリア時にUpdate内で行う処理
	void GameCrear();

	//クリア時間の書き込み
	void ClearTimeWrite();

	//開発、デバック用のコマンド
	void CreatersCommand();

	//マップのゲッター
	Map* GetMap() const;

	//プレイヤーのゲッター
	Player* GetPlayer() const;

	//ターゲットカウントを減らす
	void KillTarget();

	//ターゲットの生成予定の位置のゲッター
	Point GetTargetPos() const;

};
