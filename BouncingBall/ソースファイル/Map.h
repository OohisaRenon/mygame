#pragma once
#include "Engine/Global.h"

class Collider;
class BoxCollider;

struct table
{
	int height;	//何段積んでいるか
	int type;	//何のブロックか
};

enum
{
	BOX_DEFAULT,	//デフォ
	BOX_GRASS,		//草
	BOX_SAND,		//砂
	BOX_WATER,		//水
	BOX_BRICK,		//石
	BOX_MAX,
};


class Map : public IGameObject
{

	table table_[MAP_ROW][MAP_COL];	//マップの情報(平面で見た時の縦と横の大きさ)
	int hModels_[BOX_MAX];			//マップチップのハンドル
	int index_;		//マップチップに当たり判定を付ける時に使うカウント用変数
	BoxCollider* pCollision_[MAP_ROW][MAP_COL];	//マップチップにつけるコライダー

public:
	
	//コンストラクタ
	Map(IGameObject* parent);

	//デストラクタ
	~Map();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//マップエディターで作成したマップを読み込み
	void LoadMap();

	//コンマまで読み込んで数値に変換して返す
	//戻り値:読み込んだ文字を数値に変換した値
	//引数 : data 読み込むデータ
	//引数 : index どこから読み込むかのアドレス
	int GetToComma(char* data, int &index) const;

	//指定したコライダーのハンドルを返す
	//引数：col ハンドルが知りたいコライダー
	//戻り値：　ハンドル(コライダー番号)
	int GetColNum(Collider* col) const;
	
	//高さのゲッター
	//引数：pos テーブルのX,Z座標
	//指定したテーブルの高さを取得する
	int GetHeight(Point pos) const;

};

