#pragma once
#include "Collider.h"


//球体の当たり判定
class SphereCollider : public Collider
{
	friend class Collider;

	//判定サイズ（半径）
	float		radius_;

public:
	//コンストラクタ（当たり判定の作成）
	//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
	//引数：radius	当たり判定のサイズ（半径）
	SphereCollider(D3DXVECTOR3 center, float radius);

	//半径のゲッター
	//戻り値：半径
	float GetRadius() const;

private:
	//接触判定
	//引数：target	相手の当たり判定
	//戻値：接触してればtrue
	bool IsHit(Collider* target) override;
};

