#pragma once
#include "Global.h"

//ポリゴンを表示するクラス(四角形)

class Quad
{
	//頂点情報の構造体
	struct Vertex
	{
		D3DXVECTOR3 pos;		//位置
		D3DXVECTOR3 normal;		//法線(面の表がどっち側かの情報)
		D3DXVECTOR2 uv;			//貼り付けるテクスチャの座標(二次元座標)
	};

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;
	LPDIRECT3DTEXTURE9 pTexture_;		//テクスチャ
	D3DMATERIAL9         material_;		//マテリアル

public:
	Quad();
	~Quad();

	void Load(const char* pPicture);
	void Draw(const D3DXMATRIX &matrix);
};