#include <vector>
#include "Image.h"
#include "Global.h"

namespace Image
{
	std::vector<ImageData*> dataList;

	int Load(std::string fileName)
	{
		ImageData* pData = new ImageData;
		pData->fileName_ = fileName;

		//既に読み込まれている画像データかどうかのフラグ
		bool isExist = false;
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i]->fileName_ == fileName)
			{
				//引数で受け取ったfileNameがdataListのfilename_と同じなら
				//今見ているdataListの番地をpSpriteに伝える
				pData->pSprite_ = dataList[i]->pSprite_;		//fileName = pSprite
				isExist = true;
				break;
			}
		}

		if (isExist == false)
		{
			//画像データ生成
			pData->pSprite_ = new Sprite;
			pData->pSprite_->Load(fileName.c_str());
		}


		//画像データが読み込まれるたびにdatalistに追加される
		dataList.push_back(pData);

		return dataList.size() - 1;

	}

	//2D画像の描画
	//引数 : handle 画像に割り当てられた番号
	//引数 : color 色を変えたいときは指定する  指定しない時はデフォルト(1,1,1,1)で画像データの色のまま表示
	void Draw(int handle, D3DXCOLOR color)
	{
		dataList[handle]->pSprite_->Draw(dataList[handle]->matrix_, color);
	}

	//移動、回転、拡大率をいじった後どう動くか確定させる
	//引数 : handle 画像番号
	//引数 : matrix 移動、回転、拡大縮小用の行列
	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		dataList[handle]->matrix_ = matrix;
	}

	//解放
	//引数 : handle 画像番号
	void Release(int handle)
	{
		bool isExist = false;
		//今消そうとしてる画像データが他のオブジェクトで読み込まれていないか確認
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//同じ画像データが読み込まれていないか確認する
			//handle(今消そうとしている画像データ)は無視する
			//dataListが空の時は比較しない
			//消そうとしている画像データと、今見ている画像データが同じなら消さない
			if (i != handle && dataList[i] != nullptr && dataList[i]->pSprite_ == dataList[handle]->pSprite_)
			{
				//今消そうとしている画像データが他で使われていたら消さずにループを抜ける
				isExist = true;
				break;
			}

		}

		//他のオブジェクトで同じ画像データが使われていなければ
		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pSprite_);
		}

		SAFE_DELETE(dataList[handle]);

	}

	void AllRelease()
	{
		//ゲーム終了時にすべてのモデル解放
		for(unsigned int i = 0; i < dataList.size(); i++)
		{
			//データリストの中に何か入っていたら
			if (dataList[i] != nullptr)
			{
				//入っている要素を解放
				Release(i);
			}
		}

		//最後にベクターを空にする
		dataList.clear();
	}

	//画像データの詳細情報のゲッター
	//引数 : handle 画像番号
	void GetPictData(int handle, D3DXIMAGE_INFO &data)
	{
		dataList[handle]->pSprite_->GetTexData(data);
	}

}
