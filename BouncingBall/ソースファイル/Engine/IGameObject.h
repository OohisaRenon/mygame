#pragma once
#include "d3dx9.h"
#include <list>
#include <string>
#include <time.h>

class Collider;

class IGameObject
{

protected:
	//純粋仮想関数を含むクラスはオブジェクトを作れない
	IGameObject* pParent_;		//親の情報(親はいつも一人)
	std::list<IGameObject*> childList_;	//子供は何人いるか分からない
	std::string name_;		//文字列が入れれる型

	D3DXVECTOR3 position_;		//位置
	D3DXVECTOR3 rotate_;		//角度
	D3DXVECTOR3 scale_;			//拡大率

	D3DXMATRIX localMatrix_;		//合成後の最終的な行列(自分視点の移動、回転、拡大縮小)
	D3DXMATRIX worldMatrix_;		//合成後の最終的な行列


	D3DXVECTOR3 colPos_;	//衝突位置
	D3DXVECTOR3 moveVec_;	//移動ベクトル(速度)
	double updateTime_;		//ポジションの更新から次の更新直前までの時間
	double colTime_;		//衝突した時間
	clock_t moveStart_, moveEnd_;	//計測開始時間と終了時間用の変数


	//移動、回転、拡大縮小の行列を合成
	void Transform();

	std::list<Collider*>	colliderList_;	//衝突判定リスト

	bool dead_;		//KillMe()用の自殺フラグ
	bool isVisible_;	//Drawを許可するかどうか

public:
	//コンストラクタ
	IGameObject();
	IGameObject(IGameObject* parent);
	IGameObject(IGameObject* parent, const std::string& name);		//参照渡しでバイト数削減

	//子クラスのデストラクタが呼ばれるように、デストラクタもオーバーライド
	virtual ~IGameObject();

	//初期化
	virtual void Initialize() = 0;

	//更新
	virtual void Update() = 0;

	//描画
	virtual void Draw() = 0;

	//開放
	virtual void Release() = 0;

	// Drawを拒否
	void Invisible();

	// Drawを許可
	void Visible();

	//IGameObjectと子クラスのUpdataを呼ぶ
	void UpdateSub();
	
	//IGameObjectと子クラスのDrawを呼ぶ
	void DrawSub();
	
	//IGameObjectと子クラスのReleaseを呼ぶ
	void ReleaseSub();
	
	//自分を殺すフラグを立てる
	void KillMe();
	




	//コライダー（衝突判定）を追加する
	void AddCollider(Collider* collider);

	//何かと衝突した場合に呼ばれる（オーバーライド用）
	//引数：pTarget	衝突した相手のコライダー
	virtual void OnCollision(Collider* pTarget) {};

	//衝突判定
	//引数：pTarget	衝突してるか調べる相手
	void Collision(IGameObject* pTarget);

	//テスト用の衝突判定枠を表示
	void CollisionDraw() const;


	//各クラスのゲームオブジェクト生成
	//引数には生成したクラスの親を指定
	//※この関数を呼ぶクラス
	template <class T>
	T* CreateGameObject(IGameObject* parent)	//処理は同じだが型が違うときに使う(テンプレート)
	{
		T* p = new T(parent);		//各クラスのオブジェクト生成＆親設定
		parent->PushBackChild(p);	//子供のリストの末尾に追加
		p->Initialize();		//生成した子クラスのポインタからイニシャライズを呼び初期設定を行う
		return p;		//生成した子クラス型のポインタを返す
	}

	//ほしいクラスのインスタンスをとってくる(対象は<  >の子供から)
	//<  >：呼びだす側のクラス
	//引数：name 探したいクラスの名前
	//戻り値：nameで指定したクラスのインスタンス
	template <class T>
	T* GetInstance(std::string name)
	{
		T* p = nullptr;
		for (auto it = pParent_->childList_.begin(); it != pParent_->childList_.end(); it++)
		{
			if ((*it)->GetObjectName() == name)
			{
				p = (T*)(*it);
				break;
			}
		}
		return  p;
	}


	//生成したオブジェクトをchildListに追加する(親が子を管理するため)
	//引数:追加する子供のオブジェクト
	void PushBackChild(IGameObject* pObj);

	//オブジェクトの名前のゲッター
	const std::string& GetObjectName() const;

	////////////////
	//	セッター
	///////////////

	void SetPosition(D3DXVECTOR3 position);
	void SetRotate(D3DXVECTOR3 rotate);
	void SetScale(D3DXVECTOR3 scale);
	void SetMoveVec(D3DXVECTOR3& moveVec);
	void SetColPos(D3DXVECTOR3& colPos);
	double SetUpdateTime(double time);
	double SetColTime(double time);

	///////////////
	//	ゲッター
	//////////////

	D3DXVECTOR3 GetPosition() const;
	D3DXVECTOR3 GetRotate() const;
	D3DXVECTOR3 GetScale() const;
	D3DXVECTOR3 GetMoveVec() const;
	D3DXVECTOR3 GetColPos() const;
	double GetUpdateTime() const;
	double GetColTime() const;
	IGameObject* GetParent() const;

};
