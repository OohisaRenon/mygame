#include "SphereCollider.h"
#include "BoxCollider.h"
#include "Direct3D.h"


//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
SphereCollider::SphereCollider(D3DXVECTOR3 center, float radius)
{
	center_ = center;
	radius_ = radius;
	type_ = COLLIDER_SPHERE;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateSphere(Direct3D::pDevice, radius, 8, 4, &pMesh_, 0);
#endif
}

//半径のゲッター
//戻り値：半径
float SphereCollider::GetRadius() const
{
	return radius_;
}

//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool SphereCollider::IsHit(Collider* target)
{
	//当たった相手のコライダーの形を見て
	//どの当たり判定を呼び出すか変える
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsCircle((BoxCollider*)target, *this);
	else
		//return IsHitCircleVsCircle((SphereCollider*)target, this);
		return IsHitCircleVsCircle(*((SphereCollider*)target), *this, &this->GetGameObject()->GetColPos(),
		((SphereCollider*)target)->GetCenter(), this->GetCenter());
}