#pragma once
#define DIRECTINPUT_VERSION 0x0800		//Inputのどのバージョンを使うのか指定

#include <dInput.h>

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;} 

namespace Input
{
	void Initialize(HWND hWnd);			//何のキーが押されたか
	void Update();
	bool IsKey(int keyCode);		//特定のキーが押されているかどうか(長押しを含む)
	bool IsKeyDown(int keyCode);	//特定のキーが「今」押されたかどうか(今までは押されていなくて、今この瞬間押されたかどうか)
	bool IsKeyUp(int keyCode);		//特定のキーから指が「今」離れたか
	void Release();
};