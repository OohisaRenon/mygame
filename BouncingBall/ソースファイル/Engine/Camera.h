#pragma once
#include "IGameObject.h"
#include "Direct3D.h"

//カメラを管理するクラス
class Camera : public IGameObject
{
	//※今回はカメラを傾けないのでどこが上向きかを示す変数作らない
	//視点はカメラの位置なのでIGameObjectのposition_を使う
	D3DXVECTOR3 target_;		//視点

public:
	//コンストラクタ
	Camera(IGameObject* parent);

	//デストラクタ
	~Camera();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//視点のセッター
	//引数：target   設定したいカメラの視点
	void SetTarget(D3DXVECTOR3 target);

	//視点のゲッター
	//戻り値：現在のカメラの視点
	D3DXVECTOR3 GetTarget() const;
};