#include <Windows.h>
#include "Global.h"
#include "Direct3D.h"
#include "RootJob.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"
#include <stdlib.h>

//////////////////////////////////////////////////
//  LP~...という名前の型はポインタを指す        //
//  I~...という名前の型はインタフェースを指す   //
//////////////////////////////////////////////////

//リンカ	※これの場所はどこでもいい(1箇所)
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib, "winmm.lib")

//メモリリーク検出用ヘッダー
#ifdef _DEBUG			//デバックモードの時のみ実行
#include <crtdbg.h>
#endif

//ウィンドウ関係の定数
const char* WIN_CLASS_NAME = "Bouncing Ball";
const int	WINDOW_WIDTH = 800;		//ウィンドウの幅
const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ

//時間計測関係の定数
const float FRAME = 60.0f;		//フレームレート
const float SECOND = 1000.0f;	//ミリ秒単で計測した時の1秒の数値

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//グローバル変数

Global g;
RootJob* pRootJob;

//エントリーポイント(main関数みたいなもの)
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef _DEBUG
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//乱数生成用
	srand((unsigned int)time(NULL));

	//LPSTR ロングポインタSTR : char形のポインタと同じ
	//hInstance インスタンスハンドル : オブジェクトの識別番号
	//hPrevInst 昔使ってた。今はいらないけど、消すと前のプログラムが動かなくなるので消さない
	//lpCmdLine : プログラムを起動したときに
	//nCmdShow : 全画面表示orウィンドで表示()

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	//ウィンドウクラスの中身を初期化
	wc.cbSize = sizeof(WNDCLASSEX);	            //この構造体(クラス)のサイズ
	wc.hInstance = hInstance;	                //インスタンスハンドル(オブジェクトの番号)
	wc.lpszClassName = WIN_CLASS_NAME;	        //ウィンドウクラス名(今から作ろうとしてるウィンドウの名前)
	wc.lpfnWndProc = WndProc;	                //ウィンドウプロシージャ(何か操作が行われたときに呼ばれる関数をセット)
	wc.style = CS_VREDRAW | CS_HREDRAW;	        //スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);	//アイコン(自作可能)
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);	  //小さいアイコン(自作可能)
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);	  //マウスカーソル(自作も可能だが、windousプログラムでは白黒しか作れない)
	wc.lpszMenuName = NULL;	                    //メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）※ゲーム画面出すから変更の必要なし
	//RegisterClassEx : クラスを割り当てる
	RegisterClassEx(&wc);	//クラスを登録
	
	//ウィンドウの白い部分のサイズを指定
	RECT winRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };

	//ウィンドウの白い描画するところをちょうどよく合わせる
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);

	//ウィンドウを作成

	HWND hWnd = CreateWindow(	//HWND : ウィンドウハンドル
		//hWnd作られたウィンドウの番号を入れる
		WIN_CLASS_NAME,	       //ウィンドウクラス名(上で初期化した内容と同じじゃなきゃだめ)
		WIN_CLASS_NAME,    	//タイトルバーに表示する内容(ゲームの名前)
		WS_OVERLAPPEDWINDOW,	//スタイル（普通のウィンドウ）※最大化最小化ボタン、閉じるときの×ボタン等の有無
		CW_USEDEFAULT,	      //表示位置左（おまかせ）※0 = 左端
		CW_USEDEFAULT,	      //表示位置上（おまかせ）※0 = 上
		winRect.right - winRect.left,	      //ウィンドウ幅
		winRect.bottom - winRect.top,        //ウィンドウ高さ
		NULL,	               //親ウインドウ（なし）※親ウィンドウを消すとこのウィンドウも消える
		NULL,	               //メニュー（なし）
		hInstance,	          //インスタンス
		NULL	                //パラメータ（なし）
	);

	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);	//第一引数でどのウィンドウを表示するか指定

	assert(hWnd != NULL);

	//ウィンドウの描画するところのサイズを設定
	//externを使っているので、どこのcppからでもアクセス可能
	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT; 

	//呼び出し,初期化Direct3D
	Direct3D::Initilize(hWnd);

	//呼び出し,初期化Input
	Input::Initialize(hWnd);

	//呼び出し,初期化Audio
	Audio::Initialize();

	pRootJob = new RootJob;
	pRootJob->Initialize();

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	//指定した構造体やクラスを0に初期化する
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))	//windousからメッセージがきたら...
		{
			//OSからメッセージあればそっちを優先
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//ゲームの処理

			// エンドピリオドまでちょっと細かく時間計測
			timeBeginPeriod(1);
			int nowTime = timeGetTime();		//計測開始
			static int lastDispTime = nowTime;	//今のフレーム時間
			static int lastUpdateTime = nowTime;//前フレームの時間
			static int fps = 0;

			//経過時間が1秒を超えたらFPSカウントと時間をリセット
			if (nowTime - lastDispTime > SECOND)
			{
				//FPS表示(タイトルバーに)
				char str[16];
				wsprintf(str, "FPS=%d", fps);
				SetWindowText(hWnd, str);
				fps = 0;
				lastDispTime = nowTime;
			}

			//1秒を60フレームに固定
			if ((nowTime - lastUpdateTime) * FRAME >= SECOND)
			{
				fps++;
				lastUpdateTime = nowTime;	//前フレームの時間に更新

				Input::Update();	//入力のアップデート関数

				pRootJob->UpdateSub();

				Direct3D::BeginDraw();		//描画開始

				pRootJob->DrawSub();

				Direct3D::EndDraw();		//描画終了

				timeEndPeriod(1);	//細かく時間計測するのをやめる
			}
		}
	}

	/////////////////
	//  解放処理  //
	////////////////

	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Audio::Release();
	Input::Release();
	Direct3D::Release();
	Model::AllRelease();
	Image::AllRelease();

	return 0;
}

//ウィンドウプロシージャ（何かあった時によばれる関数）-> 
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//LRESULT ローリザルト :  
	//CALLBACK : 何かあったときに呼ばれる関数
	//msg:メッセージ。何が起きたのか
	//wParam, lParam : 追加情報msgの内容によって変わる

	switch (msg)
	{
	case WM_DESTROY:		//ウィンドウが閉じられたとき
		PostQuitMessage(0);	//プログラム終了　※ウィンドウ閉じる = プログラム終了にはならない
		return 0;//自分で処理を追加したら...return0:
	}
	//return DefWindowProc : デフォルト処理のままでいい場合...returnで返す
	return DefWindowProc(hWnd, msg, wParam, lParam);
}