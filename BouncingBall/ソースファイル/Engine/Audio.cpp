#include <string>
#include "Global.h"
#include "Audio.h"

namespace Audio
{

	IXACT3Engine* pXactEngine;		//本体(xact3から関数を呼ぶときに使う)
	IXACT3WaveBank* pWaveBank;		//WaveBank
	IXACT3SoundBank*pSoundBank;		//SoundBank

	void* soundBankData;

	//xactエンジン作成
	void Audio::Initialize()
	{
		CoInitializeEx(NULL, COINIT_MULTITHREADED);

		XACT3CreateEngine(0, &pXactEngine);
		XACT_RUNTIME_PARAMETERS xactParam = { 0 };
		xactParam.lookAheadTime = XACT_ENGINE_LOOKAHEAD_DEFAULT;
		pXactEngine->Initialize(&xactParam);
	}

	//WaveBank読み込み
	//引数 : waveBankName 読み込むウェーブバンクのファイル名
	void Audio::WaveBankLoad(std::string waveBankName)
	{
		//ファイルを開く
		HANDLE hFile = CreateFile(waveBankName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		//開いたファイルのサイズ
		DWORD fileSize = GetFileSize(hFile, NULL);

		//ファイルをマッピングする
		HANDLE hMapFile = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, fileSize, NULL);
		void* mapWaveBank;
		mapWaveBank = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);

		//WAVEバンク作成
		pXactEngine->CreateInMemoryWaveBank(mapWaveBank, fileSize, 0, 0, &pWaveBank);

		//ファイルを閉じる
		CloseHandle(hMapFile);
		CloseHandle(hFile);
	}

	//SoundBank読み込み
	//引数 : soundBankName 読み込むサウンドバンクのファイル名
	void Audio::SoundBankLoad(std::string soundBankName)
	{
		//ファイルを開く
		HANDLE hFile = CreateFile(soundBankName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

		//ファイルのサイズを調べる
		DWORD fileSize = GetFileSize(hFile, NULL);

		//ファイルの中身をいったん配列に入れる
		soundBankData = new BYTE[fileSize];
		DWORD byteRead;
		ReadFile(hFile, soundBankData, fileSize, &byteRead, NULL);

		//サウンドバンク作成
		pXactEngine->CreateSoundBank(soundBankData, fileSize, 0, 0, &pSoundBank);

		//ファイルを閉じる
		CloseHandle(hFile);
	}

	//再生
	//引数 : cueName　再生したいキュー(音源)の名前
	void Audio::Play(std::string cueName)
 	{
		XACTINDEX cueIndex = pSoundBank->GetCueIndex(cueName.c_str());
		pSoundBank->Play(cueIndex, 0, 0, NULL);
	}

	//停止
	//引数 : cueName　停止したいキュー(音源)の名前
	void Audio::Stop(std::string cueName)
	{
		XACTINDEX cueIndex = pSoundBank->GetCueIndex(cueName.c_str());
		pSoundBank->Stop(cueIndex, XACT_FLAG_CUE_STOP_IMMEDIATE);
	}

	//解放
	void Audio::Release()
	{
		SAFE_DESTROY(pSoundBank)
		SAFE_DELETE_ARRAY(soundBankData);
		SAFE_DESTROY(pWaveBank);
		pXactEngine->ShutDown();
		CoUninitialize();
	}

}

