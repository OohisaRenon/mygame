#pragma once
#include "Collider.h"
#include "Global.h"

//箱型の当たり判定
class BoxCollider : public Collider
{
	friend class Collider;

	D3DXVECTOR3 size_;		//判定サイズ（幅、高さ、奥行き）
	float axisLen_[BOX_COL_AXIS_MAX];		//コライダーのx,y,z軸方向の面の長さの半分
	D3DXVECTOR3 axisVec_[BOX_COL_AXIS_MAX];	//axisLenを単位ベクトルにしたもの(コライダーの向きを表す)

public:
	//コンストラクタ（当たり判定の作成）
	//引数：basePos	当たり判定の中心位置
	//引数：size	当たり判定のサイズ（幅、高さ、奥行き）
	BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size);

	//引数で指定した軸の長さのゲッター
	//引数：axis  x,y,zどの軸か
	float GetAxisLen(int axis) const;

	//引数で指定した軸のベクトルのゲッター
	//引数：axix  x,y,zどの軸か
	D3DXVECTOR3 GetAxisVec(int axis) const;

	//OBBコライダーの大きさのゲッター
	//戻り値：コライダーのサイズ
	D3DXVECTOR3 GetSize() const;

	//軸の長さのセッター
	//引数：len   回転、拡大縮小後のx,y,z軸の長さ
	//引数：num   Axisの配列の添え字( x = 0, y = 1, z = 0 )
	void SetAxisLen(float len, int num);

	//軸の長さのセッター
	//引数：vec   回転、拡大縮小後のx,y,z軸の長さ
	//引数：num   Axisの配列の添え字( x = 0, y = 1, z = 0 )
	void SetAxisVec(const D3DXVECTOR3 &vec, int num);

private:
	//接触判定
	//引数：target	相手の当たり判定
	//戻値：接触してればtrue
	bool IsHit(Collider* target) override;
};

