#pragma once
#include "Global.h"

class Sprite
{
	LPD3DXSPRITE         pSprite_;	//スプライト
	LPDIRECT3DTEXTURE9 pTexture_;	//テクスチャ
	D3DXIMAGE_INFO texData_;			//読み込んだ画像データの詳細情報

public:
	Sprite();
	~Sprite();

	//スプライトとテクスチャの作成
	//引数 : pPicture 読み込むファイルのデータ(winmainの中でファイル名指定)
	void Load(const char* pPicture);

	//2D画像の描画
	//引数 : matrix 配置する場所を変更するための移動、回転、拡大縮小後の行列
	//引数 : color 色を変えたいときは指定するデフォルトは(1,1,1)画像データのまま表示
	//※D3DXCOLOR(R, G, B, A)  A = 透明度
	void Draw(const D3DXMATRIX& matrix, D3DXCOLOR color) const;
	//※D3DXMATRIXの値は大きいので参照渡しにすると4バイト(ポインタ)しか使わなくてすむ

	//画像データの詳細情報のゲッター
	//引数 : data 書き込み用変数
	void GetTexData(D3DXIMAGE_INFO &data) const;
};

