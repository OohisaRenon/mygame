#pragma once
#include "Global.h"

//namespace内の変数、関数は他で同名のものを作ってもOK
namespace Direct3D
{
	extern LPDIRECT3D9			pD3d;	    //Direct3Dオブジェクト(ポインタ)
	extern LPDIRECT3DDEVICE9	pDevice;	//Direct3Dデバイスオブジェクト(ポインタ,画面に表示する時使う)
	extern bool	isDrawCollision;	//コリジョンを表示するか

	//オブジェクトの生成、アルファブレンド、ライティング、カメラの初期設定
	//戻り値、引数hWnd : ウィンドウハンドル
	void Initilize(HWND hWnd);

	//画面クリア、描画開始
	//戻り値、引数なし
	void BeginDraw();

	//描画終了、スワップ(裏と表示中の描画した絵を入れ替え)
	//戻り値、引数なし
	void EndDraw();

	//開放処理
	//戻り値、引数なし
	void Release();
}