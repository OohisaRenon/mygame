#pragma once
#include "Engine/IGameObject.h"
#include "Engine/Camera.h"

class Collider;
class BoxCollider;
class PlayScene;

const int WALL_HEIGHT = 2;	//当たり判定を行う壁の高さ(高さ1は床)

class CharacterObject : public IGameObject
{

protected:
	int hModel_;	//的のモデルの番号
	D3DXVECTOR3 prePos_;	//当たり判定を行う前のポジション
	BoxCollider* pCollider_;	//的の当たり判定
	D3DXVECTOR3 targetNormal_;	//衝突した相手の面の法線を入れる

public:
	//コンストラクタ
	CharacterObject(IGameObject* parent);

	//デストラクタ
	~CharacterObject();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たったら
	//引数：当たった相手
	void OnCollision(Collider* pTarget) override;

	//操作
	//プレイヤーなら移動、回転、ボールを投げるなど
	//的なら移動
	virtual void Command() = 0;
};
