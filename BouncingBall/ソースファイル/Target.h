#pragma once
#include "Engine/IGameObject.h"
#include "CharacterObject.h"

//どう動くか
enum moveType
{
	MOVE_TYPE_ESCAPE,	//逃走
	MOVE_TYPE_TRACKING,	//追跡
	MOVE_TYPE_RANDOM,	//4方向ランダム
	MOVE_TYPE_MAX
};


//動く方向
enum command 
{
	COMMAND_FRONT,
	COMMAND_BACK,
	COMMAND_RIGHT,
	COMMAND_LEFT,
	COMMAND_MAX
};


class Target : public CharacterObject
{
	moveType moveType_;	//動きのパターン
	command moveDir_;	//選択されたコマンド番号
	Point createPos_;	//生成したい座標
	unsigned int timeCount_;	//更新時間

public:
	//コンストラクタ
	Target(IGameObject* parent);

	//デストラクタ
	~Target();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//移動処理
	void Command() override;

	//移動する方向を選択
	void SelectMoveDir();

	//何かに当たったら
	//引数：pTarget 当たった相手
	void OnCollision(Collider* pTarget) override;

};
