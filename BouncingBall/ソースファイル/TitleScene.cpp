#include "TitleScene.h"
#include "Engine/Image.h"
#include "Engine/Audio.h"

//文字列定数
//画像データ
const std::string TITLE_PICT = "TitleScene";

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), hPict_(-1)
{

}

//初期化
void TitleScene::Initialize()
{

	hPict_ = Image::Load(UI_PATH + TITLE_PICT + DATA_TYPE[TYPE_PNG]);
	assert(hPict_ >= 0);

	Audio::WaveBankLoad(SOUND_PATH + WAVE_BANK + DATA_TYPE[TYPE_XWB]);
	Audio::SoundBankLoad(SOUND_PATH + SOUND_BANK + DATA_TYPE[TYPE_XSB]);

	Audio::Play(CUE_TITLE_BGM);
}

//更新
void TitleScene::Update()
{
	if (Input::IsKeyDown(DIK_RETURN))
	{
		SceneManager* pScneManager = (SceneManager*)pParent_;
		Audio::Play(CUE_CHANGE_SECENE_SE);
		pScneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
	Image::Draw(hPict_);
}

//開放
void TitleScene::Release()
{
	Audio::Stop(CUE_TITLE_BGM);
}
