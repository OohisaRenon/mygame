#include "BackGround.h"
#include "Engine/Model.h"


//モデルのファイルパス
const std::string BACK_GROUND_MODEL = "BackGround";


//コンストラクタ
BackGround::BackGround(IGameObject * parent)
	:IGameObject(parent, "BackGround"), hModel_(-1)
{
}

//デストラクタ
BackGround::~BackGround()
{
}

//初期化
void BackGround::Initialize()
{
	hModel_ = Model::Load(MODELS_PATH + BACK_GROUND_MODEL + DATA_TYPE[TYPE_FBX]);
	assert(hModel_ >= 0);
}

//更新
void BackGround::Update()
{

}

//描画
void BackGround::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void BackGround::Release()
{
}
