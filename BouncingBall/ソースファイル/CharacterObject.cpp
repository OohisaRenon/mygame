#include "CharacterObject.h"
#include "Engine/Model.h"
#include "Ball.h"
#include "Engine/BoxCollider.h"
#include "Engine/Collider.h"
#include "Map.h" 
#include "PlayScene.h"

//コンストラクタ
CharacterObject::CharacterObject(IGameObject * parent)
	:IGameObject(parent, "CharacterObject"), hModel_(-1), prePos_(0, 0, 0), pCollider_(nullptr), targetNormal_(0, 0, 0)

{
}

//デストラクタ
CharacterObject::~CharacterObject()
{
}

//初期化
void CharacterObject::Initialize()
{
}

//更新
void CharacterObject::Update()
{
}

//描画
void CharacterObject::Draw()
{
}

//開放
void CharacterObject::Release()
{
}

//何かに当たったら
//引数：当たった相手
void CharacterObject::OnCollision(Collider * pTarget)
{
}
