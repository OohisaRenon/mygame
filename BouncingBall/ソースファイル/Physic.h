#pragma once
#include "Engine/global.h"

namespace Physic
{

	//重力を考慮した運動全般の速度ベクトルを算出
	//引数 time：現在の時間(フレーム単位)
	//引数 moveVec：速度ベクトル
	//引数 mass：質量
	//引数 size：運動する物体が箱型のものの時の投影面積計算用
	//引数 radius：運動する物体が球体の時の投影面積計算用
	void CalcGravity(const double time, D3DXVECTOR3* moveVec, const float &mass, const D3DXVECTOR3 &size = D3DXVECTOR3(0, 0, 0), const float radius = 0);

	//ボール同士の反射
	//引数：weightA  物体Aの質量	//引数：weightB  物体Bの質量
	//引数：refA  物体Aの反射率		//引数：refB  物体Bの反射率
	//引数：ballPosA  衝突した時の物体Aの中心位置		//引数：ballPosB  衝突した時の物体Bの中心位置
	//引数：veloA  衝突する瞬間の物体Aの移動速度		//引数：veloB  衝突する瞬間の物体Bの移動速度
	//引数：reflectVecA  物体Aの衝突後の反射ベクトル	//引数：reflectVecB  物体Bの衝突後の反射ベクトル
	void ReflectBallVsBall(const float weightA, const float weightB, const float refA, const float refB,
		const D3DXVECTOR3 &ballPosA, const D3DXVECTOR3 &ballPosB, const D3DXVECTOR3 &veloA, const D3DXVECTOR3 &veloB,
		D3DXVECTOR3* reflectVecA, D3DXVECTOR3* reflectVecB);

	//ボールと箱が当たった時の反射
	//引数：weight  物体Aの質量
	//引数：reflect  物体の反射率
	//引数：moveVec  衝突する瞬間の物体の速度ベクトル
	//引数：reflectVec  物体の衝突後の反射ベクトル
	//引数：normal  衝突した壁の法線ベクトル
	//引数：collisionAxis  衝突軸(角に衝突した時のみ使用)
	//引数：isCorner 衝突地点が角かそれ以外か
	void ReflectBallVsBox(const float weight, const float reflect, const D3DXVECTOR3 &moveVec,
		D3DXVECTOR3 &reflectVec, const D3DXVECTOR3 &normal, const D3DXVECTOR3 &collisionAxis, bool isCorner);

	//摩擦を考慮したボールの移動速度を計算
	//引数：weight  物体の質量
	//引数：moveVec 書き込み用ベクトル		※摩擦を適用した後の移動ベクトル
	//引数：size    物体が球以外の物体だった場合のサイズ
	//引数：radius  球体だった場合の半径
	void CalcFriction(float weight, D3DXVECTOR3* moveVec, const D3DXVECTOR3 &size = D3DXVECTOR3(0, 0, 0), const float radius = 0);

};

