#include "Target.h"
#include "Engine/Model.h"
#include "Ball.h"
#include "Engine/BoxCollider.h"
#include "Map.h" 
#include "PlayScene.h"
#include "Player.h"

const float MOVE_SPEED = 0.03f;	//移動速度
const D3DXVECTOR3 MOVE_FRONT = D3DXVECTOR3(0, 0, 1.0f);				//前後移動速度
const D3DXVECTOR3 MOVE_HORIZONTAL = D3DXVECTOR3(1.0f, 0, 0);		//左右移動速度
const D3DXVECTOR3 TARGET_COL_SIZE = D3DXVECTOR3(0.9f, 0.9f, 0.25f);			//コライダーのサイズ

const D3DXVECTOR3 TARGET_POS = D3DXVECTOR3(6.0f, 1.5f, 6.0f);	//出現位置

const unsigned int UPDATE_TIME = 300;	//的が動く時間(actTime_がこの数値に達したら動く方向をランダムで決めなおす)
const float PLAYER_DISTANCE = 5.0f;	//プレイヤーまでの距離を維持するための目安

//モデルのファイルパス
const std::string TARGET_MODEL = "Target";


//コンストラクタ
Target::Target(IGameObject * parent)
	:CharacterObject(parent), timeCount_(0), createPos_()
{
	name_ = "Target";
}

//デストラクタ
Target::~Target()
{
}

//初期化
void Target::Initialize()
{
	hModel_ = Model::Load(MODELS_PATH + TARGET_MODEL + DATA_TYPE[TYPE_FBX]);
	assert(hModel_ >= 0);

	//プレイシーンから生成予定の位置取ってきて
	createPos_ = ((PlayScene*)pParent_)->GetTargetPos();

	//↑をポジションにセットする
	position_ = D3DXVECTOR3((createPos_.x + TARGET_POS.x), TARGET_POS.y, (createPos_.z + TARGET_POS.z));

	pCollider_ = new BoxCollider(D3DXVECTOR3(0, 0, 0), TARGET_COL_SIZE);
	AddCollider(pCollider_);

	//どう動くか決める
	moveType_ = (moveType)(rand() % MOVE_TYPE_MAX);

	//最初に進む方向を決める
	moveDir_ = (command)(rand() % COMMAND_MAX);
	SelectMoveDir();

}

//更新
void Target::Update()
{
	//時間計測
	timeCount_++;

	//移動処理
	Command();

	//X軸回転して動いたとき空中に浮かないように
	if (position_.y < TARGET_POS.y || position_.y > TARGET_POS.y)
	{
		position_.y = TARGET_POS.y;
	}

	prePos_ = position_;	//速度ベクトル保存用

}

//描画
void Target::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Target::Release()
{
}

//移動処理
void Target::Command()
{
	//一定時間経過したら
	if (timeCount_ > UPDATE_TIME)
	{
		//プレイヤーの位置取得
		D3DXVECTOR3 playerPos = ((Player*)((PlayScene*)pParent_)->GetPlayer())->GetPosition();

		//プレイヤーの位置 - 自身の位置でベクトルを求める
		D3DXVECTOR3 vec = playerPos - position_;

		D3DXMATRIX matRY;	//ベクトル回転用行列
		float len = 0;		//ベクトルの長さ

		switch (moveType_)
		{
		//逃走
		case MOVE_TYPE_ESCAPE:

			//↑のベクトルを正規化
			D3DXVec3Normalize(&vec, &vec);

			//正規化したベクトルの符号を反転
			vec = -vec;

			//反転したベクトルを-90〜90度の範囲でランダムにY軸回転
			D3DXMatrixRotationY(&matRY, D3DXToRadian( (rand() % 180) - 90) );
			D3DXVec3TransformCoord(&vec, &vec, &matRY);

			//回転したベクトルに移動速度をかけて移動ベクトルとする
			moveVec_ = vec * MOVE_SPEED;
			break;

		//追跡
		case MOVE_TYPE_TRACKING:

			//↑のベクトルを正規化
			D3DXVec3Normalize(&vec, &vec);

			//正規化したベクトルに移動速度をかけて移動ベクトルとする
			moveVec_ = vec * MOVE_SPEED;
			break;

		//ランダム
		case MOVE_TYPE_RANDOM:
		default:
			//進む方向を決める
			moveDir_ = (command)(rand() % COMMAND_MAX);
			SelectMoveDir();
			break;
		}

		timeCount_ = 0;
	}

	//移動させる
	position_ += moveVec_;

}

//移動する方向を選択
void Target::SelectMoveDir()
{
	//動かす
	D3DXVECTOR3 moveFront = MOVE_FRONT * MOVE_SPEED;	//前後移動
	D3DXVECTOR3 moveHorizontal = MOVE_HORIZONTAL * MOVE_SPEED;	//左右移動

	switch (moveDir_)
	{
	case COMMAND_LEFT:
		moveVec_ = -moveHorizontal;	//左移動
		break;

	case COMMAND_RIGHT:
		moveVec_ = moveHorizontal;	//右移動
		break;

	case COMMAND_FRONT:
		moveVec_ = moveFront;	//前移動
		break;

	case COMMAND_BACK:
		moveVec_ = -moveFront;	//後ろ移動
		break;
	}
}

//何かに当たったら
//引数：当たった相手
void Target::OnCollision(Collider * pTarget)
{
	IGameObject* targetObj = pTarget->GetGameObject();

	//当たった相手がマップチップなら
	if (targetObj == GetInstance<CharacterObject>("Map"))
	{
		bool isCorner = false;

		BoxCollider* targetBox = (BoxCollider*)pTarget;
		//コライダーがめり込んだ時にもとに戻す処理
		D3DXVECTOR3 collisionAxis;
		isCorner = pCollider_->CalcNormalVec(*targetBox, &collisionAxis, &targetNormal_);

		float len = 0;	//めり込んだ時に動かす距離
		//めり込んだコライダーの位置調整
		len = pCollider_->AdjustColliderPosition(targetNormal_, *targetBox);
		position_ += targetNormal_ * len;	//実際に戻す距離のベクトル
		timeCount_ = UPDATE_TIME;	//壁に当たったら進む方向をもう一度選ぶ
	}
}
