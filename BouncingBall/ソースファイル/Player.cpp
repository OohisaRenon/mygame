#include "Player.h"
#include "Engine/Model.h"
#include "Ball.h"
#include "Engine/BoxCollider.h"
#include "Map.h" 
#include "PlayScene.h"


const D3DXVECTOR3 PLAYER_PERSON = D3DXVECTOR3(0, 0.5, 0);	//一人称視点のカメラの位置
//const D3DXVECTOR3 PLAYER_PERSON = D3DXVECTOR3(0, 0.5, -3);	//三人称視点(後ろから)
//const D3DXVECTOR3 PLAYER_PERSON = D3DXVECTOR3(0, 10, 0);	//三人称視点(真上から)
const D3DXVECTOR3 FIRST_PERSON_TARGET = D3DXVECTOR3(0, 0.5, 1);	//一人称視点のカメラの注視点


const D3DXVECTOR3 PLAYER_COL_SIZE = D3DXVECTOR3(0.9f, 2.3f, 1);			//プレイヤーにつけるコライダーのサイズ

const float MOVE_SPEED = 0.07f;	//プレイヤーの移動速度(1フレームあたりの移動距離)
const float ROTATE_SPEED = 1.5f;	//プレイヤーの回転速度
const float ROTATE_MAX = 25.0f;		//回転角度の限界値
const D3DXVECTOR3 RELEASE_POINT = D3DXVECTOR3(1.0f, 1.1f, 1.25f);	//ボールの出す位置
const D3DXVECTOR3 RELEASE_POINT_ROOT = D3DXVECTOR3(1.0f, 0.7f, -9);	//ボールを投げるときの角度を決めるために用意する点

const float PlAYER_POS_Y = 1.8f;	//プレイヤーの初期位置(地面の上に来るようにY座標調整)
const int CREATE_BALL_TIME = 30;	//ボールが次に生成されるまでのクールタイム

//モデルのファイルパス
const std::string PLAYER_MODEL = "Player";
const std::string SHADER_FILE = "CommonShader";


//コンストラクタ
Player::Player(IGameObject * parent)
	:CharacterObject(parent), pCam_(nullptr), createBallCnt_(0), pBallEffect_(nullptr)
{
	name_ = "Player";
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	hModel_ = Model::Load(MODELS_PATH + PLAYER_MODEL + DATA_TYPE[TYPE_FBX]);
	assert(hModel_ >= 0);

	pCollider_ = new BoxCollider(D3DXVECTOR3(0,0,0), PLAYER_COL_SIZE);
	AddCollider(pCollider_);

	//シェーダー作成失敗時のエラーメッセージ用
	LPD3DXBUFFER err = 0;

	//HLSLで書いたシェーダーをロード
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice,
		(SHADER_FILE + DATA_TYPE[TYPE_HLSL]).c_str(), NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pBallEffect_, &err)))
	{
		//シェーダー作成失敗したときにテキストボックス表示
		//第一引数：デバイスオブジェクト
		//第二引数：エラーメッセージの内容
		//第三引数：ステータスバーに表示する内容
		//第四引数：なんのボタンを出すか
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダー作成エラー", MB_OK);
	}

	//カメラ出す
	//一人称視点に設定
	pCam_ = CreateGameObject<Camera>(this);
	pCam_->SetPosition(PLAYER_PERSON);
	pCam_->SetTarget(FIRST_PERSON_TARGET);

}

//更新
void Player::Update()
{
	createBallCnt_++;

	//移動処理
	Command();

	//X軸回転して動いたとき空中に浮かないように
	if (position_.y < PlAYER_POS_Y || position_.y > PlAYER_POS_Y)
	{
		position_.y = PlAYER_POS_Y;
	}

	prePos_ = position_;	//速度ベクトル保存用

}

//描画
void Player::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
	SAFE_RELEASE(pBallEffect_);
}

void Player::Command()
{
	//向いてる方向を前とする
	D3DXMATRIX moveMat, matRX, matRY;
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	moveMat = matRX * matRY;

	//キーボード操作
	//移動
	Move(moveMat);

	//視点回転
	ViewRotation();

	//衝突確認
	FollowToWall();

	if (createBallCnt_ >= CREATE_BALL_TIME)
	{
		//ボール投げる
		Throw(moveMat);
	}
}

//移動した先のマスが壁なら位置を壁沿いに固定する
void Player::FollowToWall()
{
	Map* pMap = ((PlayScene*)pParent_)->GetMap();
	Point currentPos = Point();		//プレイヤーが現在いるマス
	float axisX = pCollider_->GetAxisLen(0);	//プレイヤーにセットしたコライダーのX軸
	float axisZ = pCollider_->GetAxisLen(2);	//プレイヤーにセットしたコライダーのZ軸

	/////////////////////////////////////////////////
	//※左と後ろに移動して壁に当たった場合は
	//プレイヤーのコライダーの左or後ろ面を
	//衝突相手の右or上面に合わせるため、+1する
	/////////////////////////////////////////////////

	//右移動で衝突
	currentPos.x = (int)(position_.x + axisX);	//現在地 + セットしたコライダーのX軸
	currentPos.z = (int)(position_.z);	//現在地 + セットしたコライダーのZ軸
	
	////////////////////////////////////////////////////////////////////////////
	//※WALL_HEIGHTはプレイヤークラスでも使用するためCharacterObject.hにて宣言
	////////////////////////////////////////////////////////////////////////////

	//移動後の位置が壁にめり込んでいるなら
	if (pMap->GetHeight(currentPos) >= WALL_HEIGHT)
	{
		position_.x = currentPos.x - axisX;
	}

	//左移動で衝突
	currentPos.x = (int)(position_.x - axisX);	//現在地 + セットしたコライダーのX軸
	currentPos.z = (int)(position_.z);	//現在地 + セットしたコライダーのZ軸
	
	//移動後の位置が壁にめり込んでいるなら
	if (pMap->GetHeight(currentPos) >= WALL_HEIGHT)
	{
		position_.x = currentPos.x + 1 + axisX;
	}

	//前移動で衝突
	currentPos.x = (int)(position_.x);	//現在地 + セットしたコライダーのX軸
	currentPos.z = (int)(position_.z + axisZ);	//現在地 + セットしたコライダーのZ軸
	
	//移動後の位置が壁にめり込んでいるなら
	if (pMap->GetHeight(currentPos) >= WALL_HEIGHT)
	{
		position_.z = currentPos.z - axisZ;
	}

	//後ろ移動で衝突
	currentPos.x = (int)(position_.x);	//現在地 + セットしたコライダーのX軸
	currentPos.z = (int)(position_.z - axisZ);	//現在地 + セットしたコライダーのZ軸
	
	//移動後の位置が壁にめり込んでいるなら
	if (pMap->GetHeight(currentPos) >= WALL_HEIGHT)
	{
		position_.z = currentPos.z + 1 + axisZ;
	}
}

//ボール投げる
void Player::Throw(const D3DXMATRIX &moveMat)
{
	if (Input::IsKey(DIK_SPACE))
	{
		//ボールのリリースポイント(投げる時の手から離れた瞬間の位置)
		D3DXVECTOR3 releasePoint = RELEASE_POINT;
		D3DXVec3TransformCoord(&releasePoint, &releasePoint, &moveMat);
		releasePoint += position_;

		//生成時リリースポイントからボール出るように設置
		Ball* pBall = CreateGameObject<Ball>(pParent_);
		pBall->SetPosition(releasePoint);

		//ボールにシェーダー渡す
		pBall->SetEffect(pBallEffect_);

		//投げるボールの軌道ベクトルを作成
		D3DXVECTOR3 releasePointRoot = RELEASE_POINT_ROOT;
		D3DXVec3TransformCoord(&releasePointRoot, &releasePointRoot, &moveMat);
		releasePointRoot += position_;

		//ボールの軌道ベクトルを計算して渡す
		D3DXVECTOR3 moveVec = releasePoint - releasePointRoot;
		//moveVec = D3DXVec3Length(&moveVec) * moveVec;
		pBall->SetMoveVec(moveVec);

		createBallCnt_ = 0;	//生成したらカウントリセット
	}
}

//視点回転
void Player::ViewRotation()
{
	if (Input::IsKey(DIK_LEFT))
	{
		rotate_.y -= ROTATE_SPEED;
	}
	if (Input::IsKey(DIK_RIGHT))
	{
		rotate_.y += ROTATE_SPEED;
	}
	if (Input::IsKey(DIK_UP))
	{
		rotate_.x -= ROTATE_SPEED;
	}
	if (Input::IsKey(DIK_DOWN))
	{
		rotate_.x += ROTATE_SPEED;
	}

	//カメラ回転の上限、下限設定
	if (rotate_.x > ROTATE_MAX)
	{
		rotate_.x = ROTATE_MAX;
	}
	if (rotate_.x < -ROTATE_MAX)
	{
		rotate_.x = -ROTATE_MAX;
	}
}

//移動
void Player::Move(const D3DXMATRIX &mat)
{
	D3DXVECTOR3 moveFront = D3DXVECTOR3(0, 0, 1);	//前後移動
	D3DXVECTOR3 moveHorizontal = D3DXVECTOR3(1, 0, 0);	//左右移動

	//進んでる方向のベクトルを変換
	D3DXVec3TransformCoord(&moveFront, &moveFront, &mat);
	D3DXVec3TransformCoord(&moveHorizontal, &moveHorizontal, &mat);

	//左
	if (Input::IsKey(DIK_A))
	{
		moveVec_ = -moveHorizontal;
		position_ += moveVec_ * MOVE_SPEED;
	}

	//右
	if (Input::IsKey(DIK_D))
	{
		moveVec_ = moveHorizontal;
		position_ += moveVec_ * MOVE_SPEED;
	}

	//前
	if (Input::IsKey(DIK_W))
	{
		moveVec_ = moveFront;
		position_ += moveVec_ * MOVE_SPEED;
	}

	//後ろ
	if (Input::IsKey(DIK_S))
	{
		moveVec_ = -moveFront;
		position_ += moveVec_ * MOVE_SPEED;
	}

}
