#include <fstream>
#include "PlayScene.h"
#include "Player.h"
#include "Map.h"
#include "BackGround.h"
#include "Ball.h"
#include "Target.h"
#include "Engine/Image.h"
#include "Engine/Audio.h"


//背景のモデルを描画する位置
const float BACK_GROUND_POS_X = MAP_COL / 2;
const float BACK_GROUND_POS_Z = MAP_ROW / 2;

const D3DXVECTOR3 PlAYER_POS = D3DXVECTOR3(2.0f, 1.8f, 2.0f);		//プレイヤーの初期位置
const int CREATE_TARGET = 3;		//生成する的の数
const int RANDOM_MAX = 18;			//的を生成する場所を決める時に使う乱数の最大値

const double TIME_PER_MINUTE = 60.0;//タイマー用「分」の計算のため
const double TIME_MAX = 600.00;		//タイマーの上限値(9:59)
const D3DXVECTOR3 TIME_DRAW_INTERVAL = D3DXVECTOR3(30.0f, 0, 0);	//プレイ中のタイマーの描画位置調整用(移動行列の設定時に使用)
const D3DXVECTOR3 CLEAR_TIME_DRAW_INTERVAL = D3DXVECTOR3(330.0f, 80.0f, 0);	//クリア後のタイマーの描画位置調整用(移動行列の設定時に使用)
const D3DXVECTOR3 NEW_RECORD_POS = D3DXVECTOR3(200.0f, 0, 0);		//state_ = resultの時にNewRecordのUIを描画する位置

//読み込む画像やモデルのデータ
const std::string CLEAR_PICT = "ClearLogo";
const std::string TIME_PICT = "Num_";
const std::string COLON_PICT = "Colon";
const std::string NEW_RECORD_PICT = "NewRecord";
const std::string CLEAR_TIME_TXT = "ClearTime";


//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)	
	: IGameObject(parent, "PlayScene"), pMap_(nullptr), pPlayer_(nullptr), pBackGround_(nullptr), hPictClear_(-1), hPictColon_(-1), hPictNewRecord_(-1),
	targetCnt_(0), targetPos_(Point()), canPlaySE_(true), start_(0), end_(0), time_(0.0), state_(STATE_PLAY), pictColor_(D3DXCOLOR(255, 255, 255, 1))
{
}

//初期化
void PlayScene::Initialize()
{
	//データの読み込み
	for (int i = 0; i < TIME_TABLE_MAX; i++)
	{
		for (int j = 0; j < TIME_PICT_MAX; j++)
		{
			hPictTime_[i][j] = Image::Load(UI_PATH + TIME_PICT + std::to_string(j) + DATA_TYPE[TYPE_PNG]);
			assert(hPictTime_[i][j] >= 0);
		}
	}

	hPictClear_ = Image::Load(UI_PATH + CLEAR_PICT + DATA_TYPE[TYPE_PNG]);
	assert(hPictClear_ >= 0);

	hPictColon_ = Image::Load(UI_PATH + COLON_PICT + DATA_TYPE[TYPE_PNG]);
	assert(hPictColon_ >= 0);

	hPictNewRecord_ = Image::Load(UI_PATH + NEW_RECORD_PICT + DATA_TYPE[TYPE_PNG]);
	assert(hPictNewRecord_ >= 0);

	Audio::WaveBankLoad(SOUND_PATH + WAVE_BANK + DATA_TYPE[TYPE_XWB]);
	Audio::SoundBankLoad(SOUND_PATH + SOUND_BANK + DATA_TYPE[TYPE_XSB]);

	//インスタンス生成
	pBackGround_ = CreateGameObject<BackGround>(this);
	pBackGround_->SetPosition( D3DXVECTOR3(BACK_GROUND_POS_X, 0.0f, BACK_GROUND_POS_X) );
	pMap_ = CreateGameObject<Map>(this);
	pPlayer_ = CreateGameObject<Player>(this);
	pPlayer_->SetPosition(PlAYER_POS);


	while (targetCnt_ < CREATE_TARGET)
	{
		targetPos_.x = rand() % RANDOM_MAX;
		targetPos_.z = rand() % RANDOM_MAX;

		//高さが1より大きい(壁になっているところには生成しない)
		//生成する範囲は内側の枠の中になるようにTargetクラス内生成位置の初期値を設定
		int height = pMap_->GetHeight(targetPos_);
		if (pMap_->GetHeight((targetPos_)) < 2)
		{
			CreateGameObject<Target>(this);
			targetCnt_++;
		}
	}

	//プレイシーンのBGM再生
	Audio::Play(CUE_PLAY_BGM);

}

//更新
void PlayScene::Update()
{
	end_ = clock();	//計測終了

	//開発、デバック用のコマンド
	CreatersCommand();

	if (targetCnt_ <= 0 && state_ == STATE_PLAY)
	{
		//的が0ならクリア状態
		state_ = STATE_CLEAR;
	}

	if (state_ == STATE_CLEAR)
	{
		//ゲームクリア時に行う処理
		GameCrear();
	}

	else if (state_ == STATE_RESULT)
	{
		if (Input::IsKeyDown(DIK_RETURN))
		{
			//タイトルに戻す
			SceneManager* pScneManager = (SceneManager*)pParent_;
			Audio::Play(CUE_CHANGE_SECENE_SE);
			pScneManager->ChangeScene(SCENE_ID_TITLE);
		}
	}

	else if(state_ == STATE_PLAY)
	{
		//時間計測が開始してから
		if (start_ != 0)
		{
			time_ += ((double)(end_ - start_) / CLOCKS_PER_SEC);

			//10分以降は9:59で時間を固定
			if (time_ >= TIME_MAX)
			{
				time_ = TIME_MAX - 1;
			}
		}
	}

	start_ = clock();	//計測開始
}

//描画
void PlayScene::Draw()
{
	//クリア時に表示する画像データ
	if (state_ == STATE_CLEAR)
	{
		Image::Draw(hPictClear_);	//クリア画面
	}

	else if (state_ == STATE_RESULT)
	{
		//クリアタイムをUIで表示
		//今のところtop5まで
		for (int rank = 0; rank < SAVE_DATA_MAX; rank++)
		{
			DrawClearTime(rank);
		}
	}

	//時間の描画はゲームプレイ中のみ(クリア時はクリアタイムのランキング表示)
	else if(state_ == STATE_PLAY)
	{
		//タイマーが計測開始してから
		if (start_ != 0)
		{
			DrawTime();
		}
	}

}

//テキストから読み込んだクリアタイムを表示
//引数 : rank 何位まで表示するか(ループ回数の上限)
//※変更したいときは SAVE_DATA_MAX を変更
void PlayScene::DrawClearTime(int rank)
{
	//タイマー用UIハンドルの添え字
	int i = 0, j = 0, k = 0;
	int second = (int)clearTimeData_[rank] % (int)TIME_PER_MINUTE;	//秒単位での経過時間
	i = (int)clearTimeData_[rank] / (int)TIME_PER_MINUTE;		//分単位での経過時間
	j = second / 10;	//経過時間(秒)の10の位の数値
	k = second % 10;	//経過時間(秒)の1の位の数値


	//位置調整
	D3DXVECTOR3 clearInterval = CLEAR_TIME_DRAW_INTERVAL;
	clearInterval.y *= rank + 1;	//1から順に縦に並べるための位置調整

	//1秒台(3桁目)
	D3DXMATRIX matK;
	D3DXMatrixTranslation(&matK, (TIME_DRAW_INTERVAL.x * (ONE_SECOND_TABLE + 1)) + clearInterval.x,
		clearInterval.y, clearInterval.z);	//UIの表示間隔 * 何桁目か
	Image::SetMatrix(hPictTime_[ONE_SECOND_TABLE][k], matK);
	Image::Draw(hPictTime_[ONE_SECOND_TABLE][k]);

	//10秒台(2桁目)
	D3DXMATRIX matJ;
	D3DXMatrixTranslation(&matJ, (TIME_DRAW_INTERVAL.x * (TEN_SECOND_TABLE + 1)) + clearInterval.x,
		clearInterval.y, clearInterval.z);	//UIの表示間隔 * 何桁目か
	Image::SetMatrix(hPictTime_[TEN_SECOND_TABLE][j], matJ);
	Image::Draw(hPictTime_[TEN_SECOND_TABLE][j]);

	//「:」の位置調整
	D3DXMATRIX matColon;
	D3DXMatrixTranslation(&matColon, (TIME_DRAW_INTERVAL.x) + clearInterval.x,
		clearInterval.y, clearInterval.z);
	Image::SetMatrix(hPictColon_, matColon);
	Image::Draw(hPictColon_);

	//1分台(1桁目)　※原点位置
	D3DXMATRIX matI;
	D3DXMatrixTranslation(&matI, (TIME_DRAW_INTERVAL.x * (MINUTE_TABLE)) + clearInterval.x,
		clearInterval.y, clearInterval.z);	//UIの表示間隔 * 何桁目か
	Image::SetMatrix(hPictTime_[MINUTE_TABLE][i], matI);
	Image::Draw(hPictTime_[MINUTE_TABLE][i]);

	////////////////////////////////////////////////////////
	//今回のタイムがtop5に入るなら、入ったところを強調表示
	////////////////////////////////////////////////////////

	if (time_ == clearTimeData_[rank])
	{
		//NewRecordの画像を出す
		D3DXMATRIX matNewRecord;
		D3DXMatrixTranslation(&matNewRecord, NEW_RECORD_POS.x,
			clearInterval.y, clearInterval.z);		//UIの表示間隔
		Image::SetMatrix(hPictNewRecord_, matNewRecord);
		Image::Draw(hPictNewRecord_);

		//黄色っぽくする(協調表示)
		pictColor_.b = 0;

		Image::Draw(hPictTime_[ONE_SECOND_TABLE][k], pictColor_);
		Image::Draw(hPictTime_[TEN_SECOND_TABLE][j], pictColor_);
		Image::Draw(hPictColon_, pictColor_);
		Image::Draw(hPictTime_[MINUTE_TABLE][i], pictColor_);
	}

}

//タイマーUIの描画
void PlayScene::DrawTime()
{
	//タイマー用UIハンドルの添え字
	int i = 0, j = 0, k = 0;

	int second = (int)time_ % (int)TIME_PER_MINUTE;	//秒単位での経過時間
	i = (int)time_ / (int)TIME_PER_MINUTE;		//分単位での経過時間
	j = second / 10;	//経過時間(秒)の10の位の数値
	k = second % 10;	//経過時間(秒)の1の位の数値

	//描画位置調整
	//1秒台(3桁目)
	D3DXMATRIX matK;
	D3DXMatrixTranslation(&matK, TIME_DRAW_INTERVAL.x * (ONE_SECOND_TABLE + 1), TIME_DRAW_INTERVAL.y, TIME_DRAW_INTERVAL.z);	//UIの表示間隔 * 何桁目か
	Image::SetMatrix(hPictTime_[ONE_SECOND_TABLE][k], matK);
	Image::Draw(hPictTime_[ONE_SECOND_TABLE][k]);

	//10秒台(2桁目)
	D3DXMATRIX matJ;
	D3DXMatrixTranslation(&matJ, TIME_DRAW_INTERVAL.x * (TEN_SECOND_TABLE + 1), TIME_DRAW_INTERVAL.y, TIME_DRAW_INTERVAL.z);	//UIの表示間隔 * 何桁目か
	Image::SetMatrix(hPictTime_[TEN_SECOND_TABLE][j], matJ);
	Image::Draw(hPictTime_[TEN_SECOND_TABLE][j]);

	//「:」の位置調整
	D3DXMATRIX matColon;
	D3DXMatrixTranslation(&matColon, TIME_DRAW_INTERVAL.x, TIME_DRAW_INTERVAL.y, TIME_DRAW_INTERVAL.z);
	Image::SetMatrix(hPictColon_, matColon);
	Image::Draw(hPictColon_);

	//1分台(1桁目)　※原点位置
	Image::Draw(hPictTime_[MINUTE_TABLE][i]);
}

//開放
void PlayScene::Release()
{
	Audio::Stop(CUE_PLAY_BGM);
}

//ゲームクリア時にUpdate内で行う処理
void PlayScene::GameCrear()
{
	//クリアした瞬間に一度だけSEを鳴らしたい
	if (canPlaySE_)
	{
		Audio::Play(CUE_CREAR_SE);
		canPlaySE_ = false;
		
		//クリア時間をテキストに書き込み
		//BGMの再生が一度だけなので、このタイミングでtime書き込み
		ClearTimeWrite();
	}

	//スペースキー入力でリザルトへ
	if (Input::IsKeyDown(DIK_RETURN))
	{
		state_ = STATE_RESULT;
	}

}

//クリア時間の書き込み
void PlayScene::ClearTimeWrite()
{
	//クリアタイムをテキストに書き込み
	std::ifstream inPutFile(CLEAR_TIME_TXT + DATA_TYPE[TYPE_TXT]);	//クリア時のタイム読み込み用
	assert(inPutFile);	//ファイルの読み込み失敗時assert

	//読み込んだデータと比較
	int index = 0;	//配列の添え字
	while (index < SAVE_DATA_MAX)
	{
		std::string time;
		std::getline(inPutFile, time);	//文字列として書き込み
		clearTimeData_[index] = atof(time.c_str());	//文字列を数値に変換して書き込み
		index++;
	}
	inPutFile.close();

	for (int i = SAVE_DATA_MAX - 1; i >= 0; i--)
	{
		//今回のタイムが読み込んだタイムのどれかより小さいなら
		if (time_ <= clearTimeData_[i])
		{
			//最大値を削除し、今回のタイムを追加
			clearTimeData_[SAVE_DATA_MAX - 1] = time_;
		}
	}

	//昇順にソート
	for (int i = 0; i < SAVE_DATA_MAX; i++)
	{
		for (int j = SAVE_DATA_MAX - 1; j > i; j--)
		{
			if (clearTimeData_[j] < clearTimeData_[j - 1])
			{
				double work = clearTimeData_[j];
				clearTimeData_[j] = clearTimeData_[j - 1];
				clearTimeData_[j - 1] = work;
			}
		}
	}

	//データ書き込み
	std::ofstream outPutFile(CLEAR_TIME_TXT + DATA_TYPE[TYPE_TXT]);
	for (int i = 0; i < SAVE_DATA_MAX; i++)
	{
		outPutFile << clearTimeData_[i] << "\n";
	}
	outPutFile.close();
}

//開発、デバック用のコマンド
void PlayScene::CreatersCommand()
{
	//デバック用のコマンド

	if ( (Input::IsKeyDown(DIK_1)) && (Input::IsKeyDown(DIK_2)) )
	{
		//クリア時の処理確認用
		state_ = STATE_CLEAR;
	}

	if ((Input::IsKeyDown(DIK_2)) && (Input::IsKeyDown(DIK_3)))
	{
		//リザルトの処理確認用
		state_ = STATE_RESULT;
	}

}

//マップのゲッター
Map * PlayScene::GetMap() const
{
	return pMap_;
}

//プレイヤーのゲッター
Player * PlayScene::GetPlayer() const
{
	return pPlayer_;
}

//ターゲットカウントを減らす
void PlayScene::KillTarget()
{
	targetCnt_--;
}

//ターゲットの生成予定の位置のゲッター
Point PlayScene::GetTargetPos() const
{
	return targetPos_;
}

