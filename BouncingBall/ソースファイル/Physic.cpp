#include "Physic.h"

#define _USE_MATH_DEFINES
#include <math.h>

namespace Physic
{
	//重力加速度
	const D3DXVECTOR3 GRAVITY = D3DXVECTOR3(0, -9.8f, 0);

	//転がり摩擦係数の定数
	//※今回は転がり抵抗のwikipediaに掲載されている
	//　自動車用タイヤとコンクリート舗装炉の値を使用
	const float ROLL_FRICTION_BRICK = 0.005f;

	//滑り摩擦係数と静止摩擦係数の定数
	//※今回は摩擦のwikipediaに掲載されている
	//　ゴムとコンクリートの値を使用
	const float STATIC_FRICTOIN_BRICK = 1.0f;
	const float SLIDING_FRICTOIN_BRICK = 0.6f;


	//重力を考慮した運動全般の速度ベクトルを算出
	void CalcGravity(const double time, D3DXVECTOR3* moveVec, const float &mass, const D3DXVECTOR3 &size, const float radius)
	{
		const float AIR_DENSITY = 1.15f;	//気温20℃程度の時の空気密度
		float resistanceArea = 0;		//風を受ける物体の面積
		D3DXVECTOR3 moveSpeed;	//約1フレームでどの程度移動するか
		D3DXVec3Normalize(&moveSpeed, moveVec);
		float speed = D3DXVec3Length(&moveSpeed);	//移動速度

		//運動する物体が球体の時
		if (radius != 0)
		{
			resistanceArea = radius * radius * M_PI;
		}

		//運動する物体が球体以外の時
		if(size != D3DXVECTOR3(0, 0, 0))
		{
			//今後実装予定
		}

		//空気抵抗の計算
		//空気抵抗 = 空気密度 * 投影面積(風を受ける面の表面積) * 速度 * 速度 / 2
		float airResistance = AIR_DENSITY * resistanceArea * speed * speed / 2;

		//速度ベクトル算出
		*moveVec += GRAVITY * time;

		//減衰させるベクトル
		D3DXVECTOR3 resistanceVec = *moveVec * airResistance * time;

		*moveVec -= resistanceVec;

		////////////////////
		//↓積分使うやつ
		//動きそれっぽいけど
		//空気抵抗強すぎる
		/////////////////////

		////空気抵抗の比例定数算出
		//float constant = 0.9f;
		//D3DXVECTOR3 drag = -constant * moveVec;	//空気抵抗(重力とは逆方向に引っ張る力)

		////空気抵抗 + 重力
		//moveVec += GRAVITY * time + drag * time;

	}

	//ボール同士の反射
	//引数：weightA  物体Aの質量	//引数：weightB  物体Bの質量
	//引数：refA  物体Aの反射率		//引数：refB  物体Bの反射率
	//引数：ballPosA  衝突した時の物体Aの中心位置		//引数：ballPosB  衝突した時の物体Bの中心位置
	//引数：veloA  衝突する瞬間の物体Aの移動速度		//引数：veloB  衝突する瞬間の物体Bの移動速度
	//引数：reflectVecA  物体Aの衝突後の反射ベクトル	//引数：reflectVecB  物体Bの衝突後の反射ベクトル
	void ReflectBallVsBall(const float weightA, const float weightB, const float refA, const float refB,
		const D3DXVECTOR3 &ballPosA, const D3DXVECTOR3 &ballPosB, const D3DXVECTOR3 &veloA, const D3DXVECTOR3 &veloB,
		D3DXVECTOR3* reflectVecA, D3DXVECTOR3* reflectVecB)
	{

		//※すべての衝突を正面衝突に置き換えて考える
		//※reflectについては衝突した2つの物体が持つ反発率(仮)から
		//疑似的な反発率を計算する

		//二つの物体の重さの合計
		float totalWeight = weightA + weightB;

		//反発率(2つの物体が衝突したとき生まれる反発係数)
		float reflect = (1 + refA * refB);

		//衝突軸のベクトル(Aの中心からBの中心までのベクトル)
		D3DXVECTOR3 collisionAxis = ballPosB - ballPosA;

		//衝突軸のベクトルを正規化しておく
		D3DXVec3Normalize(&collisionAxis, &collisionAxis);

		//衝突軸と衝突前の物体のベクトルとの内積(衝突時のスピード)
		//veloA - veloB : 衝突直前の相対速度
		//※片方の物体を止まっているものとして考えて、もう一方の物体をAのベクトル + Bのベクトル分だけ動かす
		float speed = D3DXVec3Dot(&(veloA - veloB), &collisionAxis);

		//衝突後の衝突軸方向のベクトル = 反発係数 * 衝突軸前の物体の速度(衝突軸に沿って動く) / 2つの物体の質量の合計 * 衝突軸(衝突後に飛ぶ方向)

		//反発係数 * 衝突前の物体の速度 / 質量の合計-> 衝突後の跳ね返りの速度
		//↑(※ 方向はまだわからんのでスカラー float)  (※物体の質量で割らないと鉄球がピンポン玉みたいに飛ぶことになる)
		//衝突後の跳ね返りの速度 / 衝突軸ベクトル -> 反射する方向(ベクトル)を出すための
		//↑衝突軸は物体の中心間を結んだベクトルなので、運動量保存の法則によりどっちの物体でも同じ
		D3DXVECTOR3 commonRefVec = reflect * speed / totalWeight * collisionAxis;

		//衝突後の物体ごとの移動ベクトルを求める
		//物体の質量 * 共通の衝突後のベクトル + 衝突前の速度
		//物体の質量 * 共通の衝突後のベクトル -> 衝突後に衝突軸方向にどの程度反射するのか
		//↑衝突前に働いていたベクトルとベクトルを合成して、実際のベクトルを出す

		*reflectVecA = -weightB * commonRefVec + veloA;	//Bの逆方向に飛ぶ
		*reflectVecB = weightA * commonRefVec + veloB;	//衝突後のベクトルの基準
	}

	//ボールと箱が当たった時の反射
	//引数：weight  物体Aの質量
	//引数：reflect  物体の反射率
	//引数：moveVec  衝突する瞬間の物体の速度ベクトル
	//引数：reflectVec  物体の衝突後の反射ベクトル
	//引数：normal  衝突した壁の法線ベクトル
	//引数：collisionAxis  衝突軸(角に衝突した時のみ使用)
	void ReflectBallVsBox(const float weight, const float reflect,
		const D3DXVECTOR3 &moveVec, D3DXVECTOR3 &reflectVec, const D3DXVECTOR3 &normal, const D3DXVECTOR3 &collisionAxis, bool isCorner)
	{
		//角だった場合
		if (isCorner)
		{
			D3DXVECTOR3 colAxis;
			D3DXVec3Normalize(&colAxis, &collisionAxis);

			//速度ベクトルの方向を衝突軸方向に変更
			D3DXVECTOR3 moveVecAfter = moveVec * *colAxis;

			//x,zは衝突後も変わらない
			moveVecAfter.y *= -1;
		}

		else
		{
			//反射後の速度を算出
			D3DXVECTOR3 normalVec;		//壁の法線ベクトル
			D3DXVec3Normalize(&normalVec, &normal);		//引数の法線ベクトルを正規化
			reflectVec = moveVec - (1 + reflect) * D3DXVec3Dot(&normalVec, &moveVec) * normalVec;	//反射後の物体の速度ベクトル
		}

	}

	//摩擦を考慮したボールの移動速度を計算
	//引数：weight  物体の質量
	//引数：moveVec 書き込み用ベクトル		※摩擦を適用した後の移動ベクトル
	//引数：size    物体が球以外の物体だった場合のサイズ
	//引数：radius  球体だった場合の半径
	void CalcFriction(float weight, D3DXVECTOR3* moveVec, const D3DXVECTOR3 &size, const float radius)
	{
		//外力 : 今回のフレームで動く方向と距離(ベクトル)
		//摩擦 : 物体に働く移動させようとする、もしくは移動している方向とは逆の方向に働く力
		//静止摩擦 : 物体が止まっているときに物体に働く力
		//最大静止摩擦 : 物体が動いた瞬間にかかっていた力
		//動摩擦 : 物体が動き出してから物体に働く力
		//↑動摩擦は転がり摩擦と滑り摩擦に分かれる


		//摩擦の計算
		//条件分岐で転がり摩擦か、滑り摩擦の値が入る
		float moveFriction = 0;

		//球体の場合は転がり摩擦を計算
		if (radius != 0)
		{
			//転がり摩擦 = 転がり摩擦係数(定数) * 物体の重さ
			moveFriction = ROLL_FRICTION_BRICK * weight;
		}

		//球体以外の場合は滑り摩擦を計算
		if (size != D3DXVECTOR3(0, 0, 0))
		{
			//今後実装予定

			//最大静止摩擦

			//ベクトルと比較
				//最大静止摩擦よりベクトルが大きければ
				//滑り摩擦を計算

		}

		//ベクトルに動摩擦を適用
		//※ y軸は影響を受けないように
		(*moveVec).x -= (*moveVec).x * moveFriction;
		(*moveVec).z -= (*moveVec).z * moveFriction;

	}

}
