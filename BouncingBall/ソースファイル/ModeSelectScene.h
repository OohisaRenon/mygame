#include "Engine/global.h"


//プレイシーンを管理するクラス
class ModeSelectScene : public IGameObject
{

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ModeSelectScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
