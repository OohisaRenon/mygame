#pragma once
#include "Engine/IGameObject.h"
#include "Engine/Camera.h"
#include "CharacterObject.h"

class BackGround : public IGameObject
{
	int hModel_;	//背景のモデル番号

public:
	//コンストラクタ
	BackGround(IGameObject* parent);

	//デストラクタ
	~BackGround();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};
