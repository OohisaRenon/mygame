#pragma once
#include "Engine/IGameObject.h"
#include "Engine/Camera.h"
#include "CharacterObject.h"

class Player : public CharacterObject
{
	Camera* pCam_;	//カメラオブジェクト
	int createBallCnt_;	//1フレームごとにカウントし、ボールを生成されるまでのクールタイムを計測

	//適用するのはボールクラスだが
	LPD3DXEFFECT pBallEffect_;	//シェーダーアクセス用

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//実際の操作
	void Command() override;

	//移動した先のマスが壁なら位置を壁沿いに固定する
	void FollowToWall();

	//投げる
	//引数 : moveMat 移動、回転後の行列
	void Throw(const D3DXMATRIX &moveMat);

	//視点回転
	//※Command()内で呼ばれる
	void ViewRotation();

	//移動
	//※Command()内で呼ばれる
	void Move(const D3DXMATRIX &mat);

};
