#pragma once
#include "Engine/IGameObject.h"

class SphereCollider;
class Map;

class Ball : public IGameObject
{
	int hModel_;	//ボールモデルの番号
	float mass_;	//質量(物体によって値が違う)
	float reflect_;	//反射率(物体によって値が違う)
	float friction_;//摩擦(物体によって値が違う)
	SphereCollider* pCollider_;	//セットしたコライダー

	bool isCorner_;	//衝突点が箱の角かどうか
	bool isCalcFriction_;	//摩擦を計算するかどうか
	unsigned int deleteCnt_;	//ボールが消える時間を計測する

	//シェーダー適用時に使用
	LPD3DXEFFECT pEffect_;	//シェーダーアクセス用


#ifdef _DEBUG
	bool isCol_;	//衝突したかどうか
#endif // _DEBUG

	//衝突相手の情報
	Ball* pBall_;

	//衝突時の情報
	D3DXVECTOR3 targetPos_;			//衝突相手のコライダーの中心位置
	D3DXVECTOR3 targetVec_;			//衝突相手の進行ベクトル(衝突前)
	D3DXVECTOR3 targetAfterVec_;	//衝突相手の衝突後の進行ベクトル
	D3DXVECTOR3 afterVec_;			//衝突後の自分(ボール)の進行ベクトル

	//箱型のコライダーと衝突した時に使う
	//衝突したポリゴンの法線を入れる
	D3DXVECTOR3 collisionNormal_;

	D3DXVECTOR3 prePos_;	//前の位置

public:

	//コンストラクタ
	Ball(IGameObject* parent);

	//デストラクタ
	~Ball();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//質量のゲッター
	float GetMass() const;

	//反発率のゲッター
	float GetReflect() const;

	//コライダーのゲッター
	Collider* Ball::GetCollider() const;

	//入力を受け付けて移動とか回転とか
	void Command();

	//何かに当たったら
	//引数：pTarget  当たった相手
	void OnCollision(Collider* pTarget) override;

	//シェーダーアクセス用変数のセッター
	//※Ballのモデルロードと一緒に生成するとpEffectをセットしたボールを消すときに
	//他のボールインスタンスも同じpEffectを使っているので
	//消してしまったpEffectを参照しようとしてエラーが起きる
	void SetEffect(LPD3DXEFFECT effect);
};
