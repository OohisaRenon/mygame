#include <string>
#include <fstream>
#include "Engine/Model.h"
#include "Engine/Global.h"
#include "MapChip.h"
#include "Engine/BoxCollider.h"
#include "Player.h"
#include "Map.h"

MapChip::MapChip(IGameObject* parent) : IGameObject(parent, "MapChip"), pCollision_(nullptr)
{
}


MapChip::~MapChip()
{
}

void MapChip::Initialize()
{
	//マップチップの名前
	std::string MapChipChipName[BOX_MAX]
	{
		"BoxBrick",
		"BoxDefault",
		"BoxGrass",
		"BoxSand",
		"BoxWater",
	};

	for (int i = 0; i < BOX_MAX; i++)
	{
		//マップチップを読み込む
		hMapChipChip_[i] = Model::Load("Data/Models/" + MapChipChipName[i] + ".fbx");
		assert(hMapChipChip_[i] >= 0);

	}

	//マップエディターで作成したマップを読み込む
	//LoadMapChip();

}

//更新
void MapChip::Update()
{
}

//描画
void MapChip::Draw()
{
	//for (int x = 0; x < MapChip_COL; x++)
	//{
	//	for (int z = 0; z < MapChip_ROW; z++)
	//	{
	//		for (int y = 0; y < table_[x][z].height; y++)
	//		{
	//			//移動行列作成
	//			D3DXMATRIX mat;
	//			D3DXMatrixTranslation(&mat, (float)x, (float)y, (float)z);

	//			//描画
	//			Model::SetMatrix(hMapChipChip_[table_[x][z].type], mat);
	//			Model::Draw(hMapChipChip_[table_[x][z].type]);
	//		}
	//	}
	//}

	//高さの分だけ表示
	for (int y = 0; y < table_.height; y++)
	{
		Model::SetMatrix(hMapChipChip_[table_.type], worldMatrix_);
		Model::Draw(hMapChipChip_[table_.type]);
	}

}

//解放
void MapChip::Release()
{
}

//tableのセッター
//引数：tableData  高さとマップチップの種類(type)
void MapChip::SetTableData(table tableData)
{
	
	table_ = tableData;
	
	SetCollider();
}

//読み込んだデータをもとにコライダーをセット
void MapChip::SetCollider()
{
	//データをセットして、それをもとに当たり判定追加
	pCollision_ = new BoxCollider(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, (float)(table_.height / 2), 1));
	AddCollider(pCollision_);
}

