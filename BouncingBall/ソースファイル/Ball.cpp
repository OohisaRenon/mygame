#include <fstream>
#include "Ball.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Physic.h"
#include "Player.h"
#include "PlayScene.h"
#include "Engine/SceneManager.h"
#include "Map.h"


const float BALL_RADIUS = 0.27f;	//ボールの半径(コライダーセット時に使用)

//ボールの進行方向ベクトル
const D3DXVECTOR3 MOVE_VEC_X = D3DXVECTOR3(0.1f, 0, 0);
const D3DXVECTOR3 MOVE_VEC_Y = D3DXVECTOR3(0, 0.1f, 0);
const D3DXVECTOR3 MOVE_VEC_Z = D3DXVECTOR3(0, 0, 0.1f);
const int DELETE_CNT = 210;		//ボールを消すための時間計測用(フレーム単位)

//モデルなどのファイルパス
const std::string BALL_MODEL = "Ball";
const std::string MOVE_TXT = "moveVec";

//コンストラクタ
Ball::Ball(IGameObject * parent)
	:IGameObject(parent, "Ball"), hModel_(-1),mass_(1), reflect_(0.7f), friction_(0.7), pCollider_(nullptr), pBall_(nullptr),
	afterVec_(D3DXVECTOR3(0, 0, 0)), targetPos_(D3DXVECTOR3(0, 0, 0)), targetVec_(D3DXVECTOR3(0, 0, 0)),
	targetAfterVec_(D3DXVECTOR3(0, 0, 0)), collisionNormal_(D3DXVECTOR3(0, 0, 0)),
	isCorner_(false), isCalcFriction_(true), deleteCnt_(0), prePos_(D3DXVECTOR3(0, 0, 0)), pEffect_(0)
{

#ifdef _DEBUG
	isCol_ = false;
#endif // _DEBUG

}

//デストラクタ
Ball::~Ball()
{
}

//初期化
void Ball::Initialize()
{

	hModel_ = Model::Load(MODELS_PATH + BALL_MODEL + DATA_TYPE[TYPE_FBX], pEffect_);
	assert(hModel_ >= 0);

	Model::SetEffect(pEffect_, hModel_);

	pCollider_ = new SphereCollider(D3DXVECTOR3(0, 0, 0), BALL_RADIUS);
	AddCollider(pCollider_);

#ifdef _DEBUG

	//起動時にテキストデータ消去
	//常に最新のものをテキストとして保存
	std::remove( (MOVE_TXT + DATA_TYPE[TYPE_TXT]).c_str());

#endif // _DEBUG

}

//更新
void Ball::Update()
{
	deleteCnt_++;

	//一定時間経過で消す
	if (deleteCnt_ > DELETE_CNT)
	{
		KillMe();
	}

	//計測終了
  	moveEnd_ = clock();

	//startに値が入ってから計算
	if (moveStart_ != 0)
	{
		//計測時間をdoubleにキャスト
		updateTime_ = (double)(moveEnd_ - moveStart_) / CLOCKS_PER_SEC;
	}


	//重力ありの運動関係
	Physic::CalcGravity(updateTime_, &moveVec_, mass_, D3DXVECTOR3(0, 0, 0), pCollider_->GetRadius());

	//更新前の位置を記憶しておく
	prePos_ = position_;

	//物理演算やった後に位置情報更新
	//衝突後にベクトルの向きが変わってから位置情報もおかしくなってる
	position_ += (moveVec_ * updateTime_);

	//位置更新直後に計測スタート
	moveStart_ = clock();



#ifdef _DEBUG

	//フレーム単位で行われる処理はファイルに書き出す

	std::ofstream outPutFile( (MOVE_TXT + DATA_TYPE[TYPE_TXT]).c_str(), std::ios::app);

	if (isCol_)
	{
		outPutFile << "<衝突>" << "\n";
	}

	//ボールの速度ベクトルを1フレームごとに書き出し
	outPutFile << "pos: " << position_.x << ", " << position_.y << ", " << position_.z << "\n\n";
	//outPutFile << "vec: " << moveVec_.x << "," << moveVec_.y << "," << moveVec_.z << "\n\n";
	//outPutFile << "targetPos: " << targetPos_.x << "," << targetPos_.y << "," << targetPos_.z << "\n";
	//outPutFile << "targetVec: " << targetVec_.x << "," << targetVec_.y << "," << targetVec_.z << "\n\n";
	outPutFile.close();

#endif // _DEBUG

}

//描画
void Ball::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::SetEffect(pEffect_, hModel_);

	//プロジェクション行列
	D3DXMATRIX proj;
	//最後に設定したプロジェクション行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//ビュー行列
	D3DXMATRIX view;
	//最後に設定したビュー行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//シェーダーに渡すための行列を合成
	//ワールド x ビュー x プロジェクション
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//シェーダー側に合成した行列をセット
	//第1引数：HLSLのグローバル変数名
	//第2引数：オブジェクト側から渡す行列
	pEffect_->SetMatrix("WVP", &matWVP);

	//いったんワールド行列をコピー
	D3DXMATRIX mat = worldMatrix_;

	//ワールド行列から移動行列だけ消す
	//mat._41 = 0;		//   _1_2_3_4
	//mat._42 = 0;		// 1| 1 0 0 0
	//mat._43 = 0;		// 2| 0 1 0 0
						// 3| 0 0 1 0
						// 4| 0 0 0 1

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	//面が横に伸びたら法線は縦向きに
	//面が縦に伸びたら法線は横向きにしたい
	//拡大縮小行列は逆行列にしたい
	D3DXMatrixInverse(&scale, nullptr, &scale);

	//回転行列と拡大縮小行列の逆行列を合成してシェーダーに渡す
	//※拡大縮小行列の逆行列をかけたので、このベクトルの長さは1じゃない
	mat = scale * rotateZ * rotateX * rotateY;
	pEffect_->SetMatrix("RS", &mat);

	////////////////////////////////////////
	//アプリ側でライトの方向を変えたとき
	//シェーダーでも反映されるように
	///////////////////////////////////////

	//ライトの方向を決めてるベクトルをDirect3Dから取ってくる
	D3DLIGHT9 lightState;
	Direct3D::pDevice->GetLight(0, &lightState);
	//ライトの方向を決めてるベクトルを渡す
	//ライト->物体へのベクトル
	//※LambertShaderは物体->ライトのベクトルを使うので反転させる必要あり
	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	////////////////////////////
	//色をアプリ側から指定する
	///////////////////////////

	//ライトから拡散反射光の情報だけ取り出す
	//D3DCOLORVALUE color = lightState.Diffuse;
	//D3DXVECTOR4 objectColor(1,0,0,1);
	//※第2引数：物体の色を渡す(Mayaで設定した色を渡す)
	//pEffect_->SetVector("DIFFUSE_COLOR", &D3DXVECTOR4(1,1,0,1));

	//カメラの位置をシェーダーに渡す
	//カメラの位置をプレイヤーにアクセスして取ってくる
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&position_);

	//ワールド行列を渡す
	pEffect_->SetMatrix("W", &worldMatrix_);

	//BeginからEndまでの範囲にシェーダーを適用する
	pEffect_->Begin(NULL, 0);

	//モデル表示
	pEffect_->BeginPass(0);
	Model::Draw(hModel_);
	pEffect_->EndPass();

	//ここでシェーダー適用終了
	pEffect_->End();
}

//開放
void Ball::Release()
{
}

//質量のゲッター
float Ball::GetMass() const
{
	return mass_;
}

//反射率のゲッター
float Ball::GetReflect() const
{
	return reflect_;
}

//コライダーのゲッター
Collider* Ball::GetCollider() const
{
	return pCollider_;
}

//移動とか回転とか
void Ball::Command()
{
	//移動
	if (Input::IsKey(DIK_W))
	{
		position_ += MOVE_VEC_Z;
	}

	if (Input::IsKey(DIK_S))
	{
		position_ -= MOVE_VEC_Z;
	}

	if (Input::IsKey(DIK_A))
	{
		position_ -= MOVE_VEC_X;
	}

	if (Input::IsKey(DIK_D))
	{
		position_ += MOVE_VEC_X;
	}

}


//何かに当たったら
//引数 pTarget：当たった相手
void Ball::OnCollision(Collider* pTarget)
{
	//衝突した瞬間にベクトルの向きが変わる
	//この瞬間だけは外力を無視する
#ifdef _DEBUG
	isCol_ = true;
#endif // _DEBUG

	IGameObject* targetObj = pTarget->GetGameObject();
	{
		//当たった相手が壁や床(Map)なら
		if (targetObj == GetInstance<Ball>("Map"))
		{

			//球体と箱の反射
			BoxCollider* targetBox = ((BoxCollider*)pTarget);
			D3DXVECTOR3 collisionAxis = D3DXVECTOR3(0, 0, 0);
			pCollider_->CalcNormalVec(*targetBox, &collisionAxis, &collisionNormal_);

			float len = 0;	//めり込んだ時に動かす距離
			//めり込んだコライダーの位置調整
			len = pCollider_->AdjustColliderPosition(collisionNormal_, *targetBox);
			position_ += collisionNormal_ * len;	//実際に戻す距離のベクトル

			//壁とボールの当たり判定
			Physic::ReflectBallVsBox(mass_, reflect_, moveVec_, afterVec_, collisionNormal_, collisionAxis, isCorner_);
			moveVec_ = afterVec_;

			//摩擦の計算
			Physic::CalcFriction(mass_, &moveVec_, D3DXVECTOR3(0, 0, 0), pCollider_->GetRadius());

		}
	}

	if (targetObj == GetInstance<Ball>("Ball"))
	{
		//球体同士の反射テスト
		//※どっちも動く
 		pBall_ = (Ball*)targetObj;
		targetPos_ = pBall_->GetPosition();
		targetVec_ = pBall_->GetMoveVec();

		//colStart_ = clock();

		//※仮引数のA = 自分, B = pTargetとする
		Physic::ReflectBallVsBall(mass_, pBall_->GetMass(), reflect_, pBall_->GetReflect(),
			colPos_, pBall_->GetColPos(), moveVec_, targetVec_,
			&afterVec_, &targetAfterVec_);

		//colEnd_ = clock();
		//colTime_ = (colEnd_ - colStart_) / CLOCKS_PER_SEC;
		//計算したベクトルと位置をセットする
  		moveVec_ = afterVec_;
		pBall_->SetMoveVec(targetAfterVec_);
		position_ += moveVec_ * colTime_;
	}

	if (targetObj->GetObjectName() == "Target")
	{
		((PlayScene*)pParent_)->KillTarget();
		KillMe();
		targetObj->KillMe();
	}

}

//シェーダーアクセス用変数のセッター
void Ball::SetEffect(LPD3DXEFFECT effect)
{
	pEffect_ = effect;
}
