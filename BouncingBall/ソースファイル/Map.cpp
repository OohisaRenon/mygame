#include <string>
#include <fstream>
#include "Engine/Model.h"
#include "Engine/Global.h"
#include "Map.h"
#include "Engine/BoxCollider.h"
#include "Player.h"

const D3DXVECTOR3 BLOCK_COL_SIZE = D3DXVECTOR3(1, 1, 1);	//ブロックの当たり判定の大きさ

//マップデータのファイルパス
const char* MAP_DATA = "無題.map";

Map::Map(IGameObject* parent) : IGameObject(parent, "Map"), index_(0)
{
}


Map::~Map()
{
	
}

void Map::Initialize()
{

	//マップチップの名前
	std::string mapChipName[BOX_MAX]
	{
		"BoxDefault",
		"BoxGrass",
		"BoxSand",
		"BoxWater",
		"BoxBrick",
	};

	for (int i = 0; i < BOX_MAX; i++)
	{
		//マップチップを読み込む
		hModels_[i] = Model::Load(MODELS_PATH + mapChipName[i] + DATA_TYPE[TYPE_FBX]);
		assert(hModels_[i] >= 0);
	}

	//マップエディターで作成したマップを読み込む
	LoadMap();
}

//更新
void Map::Update()
{

}

//描画
void Map::Draw()
{
	for (int x = 0; x < MAP_COL; x++)
	{
		for (int z = 0; z < MAP_ROW; z++)
		{
			for (int y = 0; y < table_[x][z].height; y++)
			{
				//移動行列作成
				D3DXMATRIX mat;
				D3DXMatrixTranslation(&mat, (float)x, (float)y, (float)z);

				//描画
				Model::SetMatrix(hModels_[table_[x][z].type], mat);
				Model::Draw(hModels_[table_[x][z].type]);
			}
		}
	}
}

//解放
void Map::Release()
{
}


//マップエディターで作成したマップを読み込む
void Map::LoadMap()
{

	HANDLE hFile;        //ファイルのハンドル
	hFile = CreateFile(
		MAP_DATA,                 //ファイル名
		GENERIC_READ,           //アクセスモード（読み込み用）
		0,                      //共有（なし）
		NULL,                   //セキュリティ属性（継承しない）
		OPEN_EXISTING,           //作成方法
		FILE_ATTRIBUTE_NORMAL,  //属性とフラグ（設定なし）
		NULL);                  //拡張属性（なし）

	//ファイルのサイズを取得
	DWORD fileSize = GetFileSize(hFile, NULL);

	//ファイルのサイズ分メモリを確保
	char* data;
	data = new char[fileSize];

	DWORD dwBytes = 0; //読み込み位置
	ReadFile(
		hFile,     //ファイルハンドル
		data,      //データを入れる変数
		fileSize,  //読み込むサイズ
		&dwBytes,  //読み込んだサイズ
		NULL);     //オーバーラップド構造体（今回は使わない）


	//読み込んだデータをmapに反映する
	int index = 0;
	for (int x = 0; x < MAP_COL; x++)
	{
		for (int z = 0; z < MAP_ROW; z++)
		{
			//コンマ区切りで読み込む
			table_[x][z].height = GetToComma(data, index);
			index++;

			//高さ分繰り返して設置
			for (int i = 0; i < table_[x][z].height; i++)
			{
				//読み込んだマップチップ一つ一つに当たり判定をつける
				pCollision_[x][z] = new BoxCollider(D3DXVECTOR3((float)x, (float)i, (float)z), BLOCK_COL_SIZE);
				AddCollider(pCollision_[x][z]);
			}

			table_[x][z].type = GetToComma(data, index);
			index++;
		}
	}

	SAFE_DELETE(data);
	CloseHandle(hFile);
}

//コンマ区切りでマップを読み込む
//戻り値:読み込んだ文字を数値に変換した値
//第一引数:読み込むデータ
//第二引数:どこから読み込むかのアドレス
int Map::GetToComma(char* data, int &index) const
{
	//書き込むために一時的に入れる配列
	//コンマまでの数字を書き込んだら添え字は初期化しておｋ
	char str[5] = { 0 };

	//書き込み用配列の添え字関数が呼ばれるたびにリセット
	//上書きして使用
	int i = 0;
	while (data[index] != ',')
	{
		str[i] = data[index];
		index++;
		i++;
	}
	if (data[index] == ',')
	{
		return atoi(str);
	}
}

//指定したコライダーのハンドルを返す
//引数 col：ハンドルが知りたいコライダー
int Map::GetColNum(Collider * col) const
{
	return col->GetColHandle();
}

//高さのゲッター
//引数：pos テーブルのX,Z座標
//指定したテーブルの高さを取得する
int Map::GetHeight(Point pos) const
{
	return table_[pos.x][pos.z].height;
}
