#pragma once
#include "Engine/Global.h"


class BoxCollider;

class MapChip : public IGameObject
{
	table table_;	//マップの情報(平面で見た時の縦と横の大きさ)
	int hMapChipChip_[BOX_MAX];			//マップチップのハンドル
	BoxCollider* pCollision_;	//マップチップにつけるコライダー
	int index_;		//マップチップ読み込み時に使用

public:

	//コンストラクタ
	MapChip(IGameObject* parent);

	//デストラクタ
	~MapChip();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//tableのセッター
	//引数：tableData  高さとマップチップの種類(type)
	void SetTableData(table tableData);

	//読み込んだデータをもとにコライダーをセット
	void SetCollider();

};

