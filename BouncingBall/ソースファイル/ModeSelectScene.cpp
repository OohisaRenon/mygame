#include "ModeSelectScene.h"

//コンストラクタ
ModeSelectScene::ModeSelectScene(IGameObject * parent)
	: IGameObject(parent, "ModeSelectScene")
{
}

//初期化
void ModeSelectScene::Initialize()
{
}

//更新
void ModeSelectScene::Update()
{

}

//描画
void ModeSelectScene::Draw()
{
}

//開放
void ModeSelectScene::Release()
{
}
