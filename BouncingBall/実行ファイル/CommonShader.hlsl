//グローバル変数はHLSL内では変更できない
//実質定数

float4x4 WVP;	//ローカル行列, ワールド行列＊ビュー行列＊プロジェクション行列
float4x4 RS;	//回転行列＊拡大縮小行列を受け取る
float4x4 W;		//ワールド行列を受け取る

float4	LIGHT_DIR;	//光の方向ベクトル(受け取った時点では光->物体のベクトル)
float4	DIFFUSE_COLOR;	//アプリ側から物体の色を受け取る
float4	AMBIENT_COLOR;	//アプリ側から環境光を受け取る
float4	SPECULAR_COLOR;	//アプリ側から鏡面反射光の色を受け取る
float	SPECULAR_POWER;	//アプリ側から鏡面反射光の強さを受け取る
float4  CAMERA_POS;		//視点(カメラの位置)
texture TEXTURE;		//テクスチャの情報(色、UV座標)
bool IS_SET_TEXTURE;		//テクスチャ貼ったか、貼ってないか

//サンプラー(テクスチャを貼るための設定)
sampler texSampler = sampler_state
{
	//どの値をテクスチャとして設定するのか(今回はグローバル変数のTEXTURE)
	Texture = <TEXTURE>;

	//アンチエリアスをかけるかどうか：LINEAR
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;

	//※テクスチャを繰り返して何枚も貼って使う場合
	//AddressU：UVの「U」
	//AddressV：UVの「V」

	//AddressU = Clamp;		//テクスチャの切れ目以降はテクスチャの左右端の色をずっと表示
	//AddressV = Clamp;		//テクスチャの切れ目以降はテクスチャの上下端の色をずっと表示
	//AddressU = Mirror;	//テクスチャの左右を反転して表示
	//AddressV = Mirror;	//テクスチャの上下を反転して表示
	AddressU = Wrap;		//テクスチャの左右方向に繰り返して何枚も表示
	AddressV = Wrap;		//テクスチャの上下に繰り返して何枚も表示

};



//色と頂点の位置を戻り値として使うための構造体
//頂点シェーダーの出力＆ピクセルシェーダーの入力
//※頂点シェーダーからピクセルシェーダーに渡す情報
struct VS_OUT
{
	//セマンティクスで何のデータとして使うのか先に指定する
	//決められたセマンティクスが存在しないときは(TEXCOORD1って書いとけばいい)
	//セマンティクスは絶対に書かなきゃいけない
	float4 pos   : SV_POSITION;	//位置
	float4 normal: NORMAL;		//法線
	float4 eye	 : TEXCOORD1;	//視線ベクトル
	float2 uv	 : TEXCOORD0;	//uv座標
};


////////////////////////////////////////////////////////////////////////
//頂点シェーダーの戻り値をそのままピクセルシェーダーに渡すので
//頂点シェーダーの戻り値とピクセルシェーダーの引数は同じじゃなきゃだめ
/////////////////////////////////////////////////////////////////////////


//頂点シェーダー(バーテクスシェーダー)
//各頂点がどの位置に表示されるのか計算する
//引数：pos (ローカル座標の位置情報)
//引数：normal(頂点の法線情報)
//引数：uv (テクスチャを貼るUV座標)
//戻り値: 3次元座標を2次元座標に変換した
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{
	//出力データ
	VS_OUT outData;		 //↓掛け算の関数(ローカル行列, ワールド行列＊ビュー行列＊プロジェクション行列)
	outData.pos = mul(pos, WVP);	//mulはベクトルと数値と行列の掛け算をしてくれる関数

	//法線ベクトルに回転、拡大縮小済みの行列をかける
	//どんなシェーディングするときでもやる
	normal = mul(normal, RS);
	normal = normalize(normal);	//法線情報を正規化
	outData.normal = normal;

	////////////////////////
	//視線ベクトルを求める//
	////////////////////////

	//ローカル座標(pos)にワールド座標(W)をかける
	float4 worldPos = mul(pos, W);
	outData.eye = normalize(CAMERA_POS - worldPos);	//カメラの位置からワールド座標を引いて視線ベクトルを求める


	//アプリ側から受け取ったUV座標を戻り値用の構造体に登録
	outData.uv = uv;


	//頂点シェーダーの出力
	return outData;
}

//ピクセルシェーダー
//引数：pos (3次元座標を2次元座標に直した頂点の位置情報)
//戻り値：ピクセル色
float4 PS(VS_OUT inData) : COLOR
{

	//法線ベクトルを正規化
	inData.normal = normalize(inData.normal);

	//視線ベクトルも正規化
	inData.eye = normalize(inData.eye);

	//ライトの方向を指すベクトル(方向だけ知りたい)
	float4 lightDir = LIGHT_DIR;	//光の降り注ぐ方向
	lightDir = normalize(lightDir);		//方向だけ知りたいときは正規化(ベクトルの長さを1にする)

	//色の明るさを計算
	float4 diffuse = dot(inData.normal, -lightDir);	//法線とライトのベクトルの内積
	diffuse.a = 1;	//透明になると困るのでaだけは1で固定しておく


	//テクスチャ貼ってたら
	if (IS_SET_TEXTURE)
	{
		//tex2D：テクスチャを貼る
		//引数：texSampler  サンプラーの名前
		//引数：inDataUV  uv座標
		diffuse *= tex2D(texSampler, inData.uv);
	}

	else
	{
		diffuse *= DIFFUSE_COLOR;	//アプリ側から受け取った色を設定
	}

	//アプリ側から受け取った環境光を設定
	float4 ambient = AMBIENT_COLOR;

	//鏡面反射光
	//反射ベクトルを求める(使用する関数：reflect)
	//引数:lightDir  光の降り注ぐ方向ベクトル
	//引数:normal  法線ベクトル
	float4 R = reflect(lightDir,inData.normal);

	//鏡面反射光の計算		視線ベクトルと鏡面反射の内積	 //↓ハイライトの濃さは固定で2としておく
	float4 speculer = pow(dot(R, inData.eye), SPECULAR_POWER) * 2 * SPECULAR_COLOR;


	//頂点シェーダーで計算した色のデータをそのまま返す
	//グローシェーディング
	return ambient + diffuse + speculer;
}

//関数をピクセルシェーダー、頂点シェーダーとして登録
//エントリーポイントみたいなもの
technique
{
	//※車のバックミラーを表示するには、前の景色と後ろの景色を描画するため「pass」は2つ必要
	//0番目のパス
	pass
	{
		//実行するたびにコンパイルするので、実行ファイル+シェーダーのソースを入れないとだめ
		//頂点シェーダーのバージョン3.0でコンパイルしてね！
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}