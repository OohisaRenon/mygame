//グローバル変数
texture TEXTURE;	//テクスチャを受け取る

//↑のサンプラー
sampler texsampler = sampler_state
{
	Texture = <TEXTURE>;

	//アンチエリアスをかけるかどうか：LINEAR
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};

//色と頂点の位置を戻り値として使うための構造体
//頂点シェーダーの出力＆ピクセルシェーダーの入力
//※頂点シェーダーからピクセルシェーダーに渡す情報
struct VS_OUT
{
	//セマンティクスで何のデータとして使うのか先に指定する
	//セマンティクスは絶対に書かなきゃいけない
	float4 pos   : SV_POSITION;	//位置
	float2 uv	 : TEXCOORD0;	//uv座標
};


//頂点シェーダー
VS_OUT VS(float4 pos : POSITION, float2 uv : TEXCOORD0)
{
	VS_OUT outData;

	//ポジションとUVをそのまま渡す
	outData.pos = pos;
	outData.uv = uv;

	return outData;
}


//普通に表示する用のピクセルシェーダー
float4 PS_Hard(VS_OUT inData) : COLOR
{
	return tex2D(texsampler, inData.uv);
}


//ぼかす用のピクセルシェーダー
float4 PS_Soft(VS_OUT inData) : COLOR
{
	//テクスチャに対応した色を出す
	//基準のピクセルを中心として周囲の9マス分の平均値を取って
	//基準値のピクセルの色を決める
	float4 color = float4(0, 0, 0, 0);
	int cnt = 0;		//ループカウント
	int power = 15;		//どのくらい「ぼかし」を入れるか
	for (int x = -power; x < power; x++)
	{
		for (int y = -power; y < power; y++)
		{
			// 1ピクセルごとに計算するために uvを「 1 / 画面サイズ」
			float4 c = tex2D(texsampler, float2(inData.uv.x + (1.0 / 1280) * x, inData.uv.y + (1.0 / 720) * y));
			
			//cの色が明かるかどうか(最大3)
			//r,g,bの合計値で判定
			if (c.r + c.g + c.b < 1.7)
			{
				//暗いピクセルは真っ黒にする
				c = float4(0, 0, 0, 1);
			}
			else
			{
				c = float4(1, 1, 1, 1);
			}
			
			color += c;
			cnt++;
		}
	}

	//オセロのAIみたいに評価値表みたいなの用意しとくと
	//強めにモザイクかけたり、弱くしたり自由にいじれる
	color /= cnt;
	return color;
}


//テクニック
technique
{
	//普通に表示
	pass
	{
		//実行するたびにコンパイルするので、実行ファイル+シェーダーのソースを入れないとだめ
		//頂点シェーダーのバージョン3.0でコンパイルしてね！
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS_Hard();
	}

	//ぼかして表示
	pass
	{
		//実行するたびにコンパイルするので、実行ファイル+シェーダーのソースを入れないとだめ
		//頂点シェーダーのバージョン3.0でコンパイルしてね！
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS_Soft();
	}

};
