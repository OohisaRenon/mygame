#include "CheckPointSub.h"
#include "Stage.h"


CheckPointSub::CheckPointSub(IGameObject * parent)
	:CheckPoint(parent)
{
	_pStage = (Stage*)FindObject("Stage");
}

CheckPointSub::~CheckPointSub()
{
}

void CheckPointSub::ModelLoad()
{
}

void CheckPointSub::OnCollision(IGameObject * pTarget)
{
	GetParent()->OnCollision(pTarget);
}
