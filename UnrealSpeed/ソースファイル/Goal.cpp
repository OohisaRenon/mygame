#include "Goal.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Image.h"

#include "UnrealSpeedProjectDefine.h"

#include "Runner.h"
#include "Stage.h"
#include "GoalSub.h"

//コンストラクタ
Goal::Goal(IGameObject * parent)
	:Wall(parent), _isGoal(false)
{
}

//デストラクタ
Goal::~Goal()
{
}

void Goal::ModelLoad()
{
	GoalSub* pGoalSubLeft = CreateGameObject<GoalSub>(this);
	pGoalSubLeft->_position.x = 0 * ONEBLOCK_LENGTH;

	GoalSub* pGoalSubRight = CreateGameObject<GoalSub>(this);
	pGoalSubRight->_position.x = 2 * ONEBLOCK_LENGTH;

	_hModel = Model::Load("data/Models/Goal.fbx");
	assert(_hModel >= 0);
}

void Goal::OnCollision(IGameObject *pTarget)
{
	if (pTarget == FindObject("Runner"))
	{
		_isGoal = true;
		((Runner*)pTarget)->SetIsGoal(_isGoal);
		((Stage*)FindObject("Stage"))->SetCheckPointNum(0);
		((Stage*)FindObject("Stage"))->SetRestartRow(0);
	}
}