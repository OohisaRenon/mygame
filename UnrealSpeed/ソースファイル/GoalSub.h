#pragma once
#include "Engine/GameObject/GameObject.h"

#include "Goal.h"

//ゴール(両端)を管理するクラス
class GoalSub : public Goal
{

public:
	//コンストラクタ
	GoalSub(IGameObject* parent);

	//デストラクタ
	~GoalSub();

	//初期化
	void ModelLoad() override;

};