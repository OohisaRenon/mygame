#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Sencer : public IGameObject
{
	bool _sencerFlg;
	int _detectKey;

public:
	//コンストラクタ
	Sencer(IGameObject* parent);

	//デストラクタ
	~Sencer();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	bool IsInputted()
	{
		return _sencerFlg;
	}

	void SetDetectKey(int);
};