#pragma once
#include "Engine/GameObject/GameObject.h"

//ステージの下の床を管理するクラス
class Ground : public IGameObject
{
	int _hModel;
public:
	//コンストラクタ
	Ground(IGameObject* parent);

	//デストラクタ
	~Ground();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};