#include "Stage.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"

#include "stdlib.h"
#include "time.h"

#include "UnrealSpeedProjectDefine.h"
#include "PlayScene.h"
#include "Wall.h"
#include "JumpWall.h"
#include "SlidingWall.h"
#include "CheckPoint.h"
#include "Goal.h"

#include "DataDefine.h"

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage"), _hModel(-1), _waitCnt(0), _row(0), _col(0), _hSound(-1), _reStartRow(0), _isCreateGoal(true), _checkPointNum(0)
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{

	//モデルデータのロード
	_hModel = Model::Load("data/Models/Stage.fbx");
	assert(_hModel >= 0);

	srand((unsigned)time(NULL));

	_stage = new StageData(1);
	_stage->LoadData();
}

//更新
void Stage::Update()
{
	if (GetParent()->GetObjectName() != "TitleScene")
	{
		//ブロック生成
		if (_waitCnt < _stage->GetSpeed())
		{
			_waitCnt++;
		}
		//ランナーが死んでいなければ
		else if(!((PlayScene*)GetParent())->IsDeadRunner())
		{
			OneRowDataCreate();

			_waitCnt = 0;
		}
	}
}

//描画
void Stage::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Stage::Release()
{
	delete(_stage);
}

void Stage::OneRowDataCreate()
{
	if (_row < _stage->GetRowMax())
	{
		for (_col = 0; _col < _stage->GetColMax(); _col++)
		{
			// Wallクラス含めた継承先を持つクラスState(仮)を作成
			// Stateクラスをメンバに持ちコンストラクタ引数にstageStateで応じたクラスを作成

			_state = _stage->GetStageState(_row, _col);
			if (_state == STATE_WALL)
			{
				Wall* _pWall = CreateGameObject<Wall>(this);
				_pWall->SetWallType(_state);
				_pWall->SetPosition(_col * ONEBLOCK_LENGTH, 0, 0);
				_pWall->SetLane(_col);
			}
			else if (_state == STATE_JUMP)
			{
				Wall* _pWall = CreateGameObject<JumpWall>(this);
				_pWall->SetWallType(_state);
				_pWall->SetPosition(_col * ONEBLOCK_LENGTH, 0, 0);
				_pWall->SetLane(_col);
			}
			else if (_state == STATE_SLIDING)
			{
				Wall* _pWall = CreateGameObject<SlidingWall>(this);
				_pWall->SetWallType(_state);
				_pWall->SetPosition(_col * ONEBLOCK_LENGTH, 0, 0);
				_pWall->SetLane(_col);
			}
			else if (_state == STATE_CHECKPOINT)
			{
				Wall* _pWall = CreateGameObject<CheckPoint>(this);
				_pWall->SetWallType(_state);
				_pWall->SetPosition(_col * ONEBLOCK_LENGTH, 0, 0);
				_pWall->SetLane(_col);
			}
		}
		_row++;
	}
	else
	{
		if (_isCreateGoal)
		{
			Wall* _pWall = CreateGameObject<Goal>(this);
			_pWall->SetPosition(1 * ONEBLOCK_LENGTH, 0, 0);		//ゴールを出すレーンは真ん中固定
			_pWall->SetLane(1);
			_isCreateGoal = false;
		}
	}
}

void Stage::ResetStage()
{
	_row = _reStartRow;
}

void Stage::SetRestartRow()
{
	_reStartRow = _row;
}

void Stage::SetRestartRow(int setRow)
{
	_reStartRow = setRow;
}

void Stage::SetCheckPointNum(int checkPointNum)
{
	_checkPointNum = checkPointNum;
}

void Stage::SetPlayingCheckPointNum(int playingCheckPointNum)
{
	_playingCheckPointNum = playingCheckPointNum;
}

int Stage::GetCheckPointNum()
{
	return _checkPointNum;
}

int Stage::GetPlayingCheckPointNum()
{
	return _playingCheckPointNum;
}

int Stage::GetBGM()
{
	return _stage->GetBGM();
}
