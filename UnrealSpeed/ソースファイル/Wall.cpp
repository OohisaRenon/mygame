#include "Wall.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/XactAudio.h"
#include "Engine/gameObject/Camera.h"

#include "UnrealSpeedProjectDefine.h"

#include "Alert.h"
#include "Runner.h"

#define WALL_COLLIDER_POSITION D3DXVECTOR3(HALFBLOCK_LENGTH, ONEBLOCK_LENGTH, HALFBLOCK_LENGTH)
#define WALL_COLLIDER_SIZE D3DXVECTOR3(ONEBLOCK_LENGTH, ONEBLOCK_LENGTH * 2, ONEBLOCK_LENGTH)

//コンストラクタ
Wall::Wall(IGameObject * parent)
	:IGameObject(parent, "Wall"), _hModel(-1), _soundFlg(true), _leanFlg(true), _lane(-1), _hRightPassSE(-1), _hLeftPassSE(-1)
{
}

//デストラクタ
Wall::~Wall()
{
}

//初期化
void Wall::Initialize()
{
	// overrideされる
	ModelLoad();

	BoxCollider* collision = new BoxCollider(WALL_COLLIDER_POSITION, WALL_COLLIDER_SIZE);
	AddCollider(collision);

	//TEST
	_position.z = ONEBLOCK_LENGTH * 30;	
	
	//アラート生成
	CreateAlert();
}

//更新
void Wall::Update()
{
	// プレイヤが死んでいるならnullptrが戻り値
	_pRunner = (Runner*)FindObject("Runner");
	if (_pRunner != nullptr)
	{
		D3DXVECTOR3 vecDiff = _position - D3DXVECTOR3(_pRunner->GetPosition().x + ONEBLOCK_LENGTH, _pRunner->GetPosition().y, _pRunner->GetPosition().z);

		float diffLength = D3DXVec3Length(&vecDiff);

		if ( _soundFlg && _lane == _pRunner->GetLane() + 1 && diffLength < ONEBLOCK_LENGTH * 30)
		{
			//右音声
			XactAudio::Play("RightPassSE");
			_soundFlg = false;
		}
		if (_soundFlg && _lane == _pRunner->GetLane() - 1 && diffLength < ONEBLOCK_LENGTH * 30)
		{
			//左音声
			XactAudio::Play("LeftPassSE");
			_soundFlg = false;
		}
		if (_soundFlg && _lane == _pRunner->GetLane() && diffLength < ONEBLOCK_LENGTH * 30)
		{
			//正面音声
			XactAudio::Play("FrontSE");
			_soundFlg = false;
		}

		// 名前検索、カメラが増えるとやばい
		Camera* pCam = (Camera*)FindObject("Camera");
		if (_lane == _pRunner->GetLane() + 1 && diffLength < ONEBLOCK_LENGTH)
		{
			// 右リーン
			pCam->SumDistinationLeanX(0.2f);
		}
		if (_lane == _pRunner->GetLane() - 1 && diffLength < ONEBLOCK_LENGTH * 3)
		{
			// 左リーン
			pCam->SumDistinationLeanX(-0.2f);
		}
	}

	_position.z -= ONEBLOCK_LENGTH * 0.8f;
	if (_position.z <= -ONEBLOCK_LENGTH * 5)
	{
		KillMe();
	}

	_position.y += 0.0001f;
}

//描画
void Wall::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Wall::Release()
{
}

//衝突
void Wall::OnCollision(IGameObject *pTarget)
{
	if (pTarget == FindObject("Runner"))
	{
		KillMe();
		((Runner*)pTarget)->Damage();
	}
}

void Wall::ModelLoad()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/Wall.fbx");
	assert(_hModel >= 0);
}

void Wall::CreateAlert()
{
	Alert* pAlert = CreateGameObject<Alert>(this);
	pAlert->SetWallType(_wallType);
}
