#include "Ear.h"

//コンストラクタ
Ear::Ear(IGameObject * parent)
	:IGameObject(parent, "Ear")
{
}

//デストラクタ
Ear::~Ear()
{
}

//初期化
void Ear::Initialize()
{
}

//更新
void Ear::Update()
{
}

//描画
void Ear::Draw()
{
}

//開放
void Ear::Release()
{
}