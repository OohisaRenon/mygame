#include "JumpWall.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"
#include "Engine/gameObject/Camera.h"

#include "UnrealSpeedProjectDefine.h"

#include "Wall.h"
#include "Sencer.h"
#include "Runner.h"

//コンストラクタ
JumpWall::JumpWall(IGameObject * parent)
	:Wall(parent), _pSencer(nullptr)
{
	_pSencer = CreateGameObject<Sencer>(this);
	_pSencer->SetPosition(_position);
	_pSencer->SetDetectKey(DIK_UPARROW);
}

//デストラクタ
JumpWall::~JumpWall()
{
}

void JumpWall::ModelLoad()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/JumpWall.fbx");
	assert(_hModel >= 0);
}

void JumpWall::OnCollision(IGameObject * pTarget)
{
	IGameObject* pRunner = FindObject("Runner");
	if (pTarget == pRunner)
	{
		// 入力がなかったら
		if (!_pSencer->IsInputted())
		{
			((Runner*)pTarget)->Damage();
		}
		KillMe();
	}
}