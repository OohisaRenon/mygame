#pragma once
#include "Engine/GameObject/GameObject.h"


//背景を管理するクラス
class Background : public IGameObject
{
	int _hModel;
	int _makeWindCnt;		//ウィンドを生成する間隔(1フレームごとにカウントアップ)
	int _makePoleCnt;		//ポールを生成する間隔(1フレームごとにカウントアップ)
	int _makeRoadCnt;		//地面を生成する間隔(1フレームごとにカウントアップ)

public:
	//コンストラクタ
	Background(IGameObject* parent);

	//デストラクタ
	~Background();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};