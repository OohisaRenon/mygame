#include "TitleScene.h"
#include "Engine/gameObject/Camera.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Image.h"	
#include "Engine/ResouceManager/XactAudio.h"

#include "UnrealSpeedProjectDefine.h"
#include "PassStageData.h"

#include "Stage.h"
#include "Runner.h"
#include "Background.h"


#define RUNNER_CAM_POSITION D3DXVECTOR3(HALFBLOCK_LENGTH, HALFBLOCK_LENGTH + 0.3, -1.5)
#define RUNNER_CAM_TARGET D3DXVECTOR3(HALFBLOCK_LENGTH + ONEBLOCK_LENGTH, HALFBLOCK_LENGTH, 0)

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
    : IGameObject(parent, "TitleScene"), _hModel(-1), _selectNum(0)
{
}

//初期化
void TitleScene::Initialize()
{
	std::string diffStr[] =
	{
		"Easy",
		"Normal",
		"Hard",
		"Endless",
	};

	//モデルデータのロード
	_hRunner = Model::Load("data/Models/Runner.fbx");
	assert(_hRunner >= 0);

	_pRunner = CreateGameObject<Runner>(this);

	_pRunner->SetPosition(ONEBLOCK_LENGTH, 0, HALFBLOCK_LENGTH);

	CreateGameObject<Background>(this);
	CreateGameObject<Stage>(this);

	_pCam = CreateGameObject<Camera>(this);
	_pCam->SetPosition(D3DXVECTOR3(2, 2, 4));
	_pCam->SetTarget(D3DXVECTOR3(4, 1, 5.5f));

	for (int i = 0; i < 2; i++)
	{
		std::string str = "Data/Picture/SampleLogo";
		str += diffStr[i];
		str += "TGS.png";
		_hDiff[i] = Image::Load(str);
	}

	//XactAudio::Play("TitleBGM");
}

//更新
void TitleScene::Update()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		PassStageData::SetPlayData((difficulty)4);
		if (Input::IsKey(DIK_LCONTROL) && Input::IsKey(DIK_LSHIFT))
		{
			PassStageData::SetPlayData((difficulty)4);
		}
		else
		{
			PassStageData::SetPlayData((difficulty)_selectNum);
		}
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

	if (Input::IsKeyDown(DIK_DOWNARROW))
	{
		if (_selectNum < 1)
		{
			//難易度カーソルを下に移動
			_selectNum += 1;
		}
	}
	else if (Input::IsKeyDown(DIK_UPARROW))
	{
		if (_selectNum > 0)
		{
			//難易度カーソルを上に移動
			_selectNum -= 1;
		}
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(_hDiff[_selectNum], _worldMatrix);
	Image::Draw(_hDiff[_selectNum]);

	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);

	// なんこれ
	Model::SetMatrix(_hRunner, _trans);
	Model::Draw(_hRunner);
}

//開放
void TitleScene::Release()
{
}