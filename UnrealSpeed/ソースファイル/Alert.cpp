#include "Alert.h"
#include "UnrealSpeedProjectDefine.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Alert::Alert(IGameObject * parent)
	:IGameObject(parent, "Alert")
{
}

//デストラクタ
Alert::~Alert()
{
}

//初期化
void Alert::Initialize()
{
	//DataDefineのステージデータとセットで
	//アラートのファイル名
	//※アラートの色変えたいときはfaileName読み込み順と、ヘッダーの順番を変える

	const std::string fileName[ALERT_COLOR_MAX] = {
		"data/Models/AlertYello.fbx",
		"data/Models/AlertRed.fbx",
		"data/Models/AlertBlue.fbx",
		"data/Models/AlertGreen.fbx",
		"data/Models/AlertWhite.fbx",
		"data/Models/AlertCyann.fbx",
		"data/Models/AlertMagenta.fbx",
	};
	
	//モデルロード
	for (int i = 0; i < ALERT_COLOR_MAX; i++)
	{
		_hModel[i] = Model::Load(fileName[i]);
		assert(_hModel[i] >= 0);
	}

}

//更新
void Alert::Update()
{
}

//描画
void Alert::Draw()
{
	//wallクラスからwallTypeを取ってくる
	//※[_wallType -1] = STATE_FLOOR = 0 なので、enumの数字を合わせるため[ -1 ]
	Model::SetMatrix(_hModel[_wallType -1], _worldMatrix);
	Model::Draw(_hModel[_wallType -1]);
}

//開放
void Alert::Release()
{
}