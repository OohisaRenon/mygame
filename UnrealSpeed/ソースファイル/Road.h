#pragma once
#include "Engine/GameObject/GameObject.h"

//地面(流れる白い線生成)を管理するクラス
class Road : public IGameObject
{
	int _hModel;    //モデル番号
public:
	//コンストラクタ
	Road(IGameObject* parent);

	//デストラクタ
	~Road();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};