#include "Ground.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Ground::Ground(IGameObject * parent)
	:IGameObject(parent, "Ground"), _hModel(-1)
{
}

//デストラクタ
Ground::~Ground()
{
}

//初期化
void Ground::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/models/ground.fbx");
	assert(_hModel >= 0);
}

//更新
void Ground::Update()
{
}

//描画
void Ground::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Ground::Release()
{
}