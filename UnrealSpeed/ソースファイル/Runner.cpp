#include "Runner.h"

#include "Alert.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/GameObject/Camera.h"

#include "Engine/ResouceManager/XactAudio.h"

#include "UnrealSpeedProjectDefine.h"
#include "PlayScene.h"

#define RUNNER_COLLIDER_POSITION D3DXVECTOR3(HALFBLOCK_LENGTH, 0.5, HALFBLOCK_LENGTH)
#define RUNNER_COLLIDER_SIZE D3DXVECTOR3(0.5, 1, 1)

#define RUNNER_CAM_POSITION D3DXVECTOR3(HALFBLOCK_LENGTH, HALFBLOCK_LENGTH, -0.5)
#define RUNNER_CAM_TARGET D3DXVECTOR3(HALFBLOCK_LENGTH, HALFBLOCK_LENGTH,0)

#define RUNNER_RIGHTEAR_POSITION D3DXVECTOR3(HALFBLOCK_LENGTH + ONEBLOCK_LENGTH, 0.5, HALFBLOCK_LENGTH)

#define RUNNER_LIFE 3

//コンストラクタ
Runner::Runner(IGameObject * parent)
	:IGameObject(parent, "Runner"), _hRunnerModel(-1), _lane(0), _life(RUNNER_LIFE), _blinkFlg(false), _blinkCnt(0), _slidingFlg(false), _jumpFlg(false), _slidingCnt(0), _jumpCnt(0), _isLifeDraw(true), _reStartRunFlg(false), _hRunnerPosMarkerModel(-1){
}

//デストラクタ
Runner::~Runner()
{
}

//初期化
void Runner::Initialize()
{
	//モデルデータのロード
	_hRunnerModel = Model::Load("data/Models/Runner.fbx");
	assert(_hRunnerModel >= 0);

	_hRunnerPosMarkerModel = Model::Load("data/Models/RunnerMarker.fbx");
	assert(_hRunnerPosMarkerModel >= 0);

	//コライダの作成
	BoxCollider * collision = new BoxCollider(RUNNER_COLLIDER_POSITION, RUNNER_COLLIDER_SIZE);
	AddCollider(collision);

	Model::SetAnimFrame(_hRunnerModel, 1, 150, 3.0f);

	const std::string fileName[LIFE_GAUGE_MAX] = {
		"data/Picture/LifeZero.png",
		"data/Picture/LifeRed.png",
		"data/Picture/LifeYellow.png",
		"data/Picture/LifeGreen.png",
	};

	//モデルロード
	for (int i = 0; i < LIFE_GAUGE_MAX; i++)
	{
		_hLifePict[i] = Image::Load(fileName[i]);
		assert(_hLifePict[i] >= 0);
	}
}

//更新
void Runner::Update()
{
	// カメラの探査
	Camera* pCam = (Camera*)FindObject("Camera");

	// Cameraが存在していてかつ、
	// スライディングとジャンプ中は入力をうけつけない
	if (pCam != nullptr && !_slidingFlg && !_jumpFlg)
	{
		if (GetParent()->GetObjectName() == "PlayScene")
		{
			InputKey(pCam);
		}
	}

	//1引数フレームになった時、2引数フレームから走るアニメーションを再開する
	RestartRunFrame(210, 75);
	RestartRunFrame(270, 1);
	RestartRunFrame(320, 75);
	//RestartRunFrame(370, 75);

	// スライディング中の処理
	if (Model::GetAnimFrame(_hRunnerModel) == 295)
	{
		Model::SetAnimFrame(_hRunnerModel, 294, 295, 1.0f);
		_slidingCnt++;
	}

	// ジャンプ中の処理
	/*if (Model::GetAnimFrame(_hRunnerModel) == 345)
	{
		Model::SetAnimFrame(_hRunnerModel, 344, 345, 1.0f);
		_jumpCnt++;
	}*/

	// スライディングの終了時の処理
	if (_slidingFlg && _slidingCnt > 5)
	{
		_slidingFlg = false;
		_slidingCnt = 0;
		Model::SetAnimFrame(_hRunnerModel, 296, 320, 1.0f);
	}

	// ジャンプの終了時の処理
	/*if (_jumpFlg && _jumpCnt > 5)
	{
		_jumpFlg = false;
		_jumpCnt = 0;
		Model::SetAnimFrame(_hRunnerModel, 346, 370, 1.0f);
	}*/

	// 走るフレーム補完終了時の処理
	if (_reStartRunFlg)
	{
		if (Model::GetAnimFrame(_hRunnerModel) >= 150)
		{
			Model::SetAnimFrame(_hRunnerModel, 1, 150, 3.0f);
			_reStartRunFlg = false;
		}
	}

	// 点滅時の処理
	if (_blinkFlg == true)
	{
		_blinkCnt++;
		if (_blinkCnt % 5 == 0)
		{
			if (IsVisibled())
			{
				Invisible();
			}
			else if (!IsVisibled())
			{
				Visible();
			}
		}
		else if (_blinkCnt > 45)
		{
			Visible();
			_blinkCnt = 0;
			_blinkFlg = false;
		}
	}

	if (_life <= 0)
	{
		XactAudio::Play("BreakSE");
		KillMe();
	}
}

void Runner::InputKey(Camera * pCam)
{
	//移動
	//右方向キー
	if (Input::IsKeyDown(DIK_RIGHTARROW) && _position.x + ONEBLOCK_LENGTH <= ONEBLOCK_LENGTH * 2)
	{
		// 走るアニメーションへの滑らかな移行をキャンセル
		_reStartRunFlg = false;
		// そのレーンへ移動
		_position.x += ONEBLOCK_LENGTH;					// 実際の位置を移動
		_lane++;										// レーン番号を移動
		pCam->SumDistinationPosX(ONEBLOCK_LENGTH);		// カメラの目標値を移動
		// 各方向キーに応じたアクション
		Model::SetAnimFrame(_hRunnerModel, 211, 270, 1.0f);
	}
	//左方向キー
	if (Input::IsKeyDown(DIK_LEFTARROW) && _position.x - ONEBLOCK_LENGTH >= 0)
	{
		_reStartRunFlg = false;
		_position.x -= ONEBLOCK_LENGTH;
		pCam->SumDistinationPosX(-ONEBLOCK_LENGTH);
		_lane--;
		Model::SetAnimFrame(_hRunnerModel, 151, 210, 1.0f);
	}

	//スペースキー
	if (Input::IsKeyDown(DIK_SPACE))
	{
		_reStartRunFlg = false;
		Model::SetAnimFrame(_hRunnerModel, 271, 295, 1.0f);
		_slidingFlg = true;

	}
	//上方向キー
	/*if (Input::IsKeyDown(DIK_UPARROW))
	{
		_reStartRunFlg = false;
		Model::SetAnimFrame(_hRunnerModel, 321, 345, 1.0f);
		_jumpFlg = true;
	}*/
}

//描画
void Runner::Draw()
{
	Model::SetMatrix(_hRunnerModel, _worldMatrix);
	Model::Draw(_hRunnerModel);

	Model::SetMatrix(_hRunnerPosMarkerModel, _worldMatrix);
	Model::Draw(_hRunnerPosMarkerModel);
	//BGMPlayされた時から描画スタート、ゴールするまで描画
	/*_isLifeDraw = ((PlayScene*)GetParent())->GetIsPlay();*/
	if (!_isGoal && _isLifeDraw)
	{
		// Test
		Image::SetMatrix(_hLifePict[_life], _worldMatrix);
		Image::Draw(_hLifePict[_life]);
	}
}

//開放
void Runner::Release()
{
}

//衝突
void Runner::OnCollision(IGameObject * pTarget)
{
	if (pTarget == FindObject("Goal"))
	{
		_isGoal = true;
	}
}

//ダメージを受けた処理
void Runner::Damage()
{
	if (!_blinkFlg)
	{
		_life -= 1;
		_blinkFlg = true;
	}
}

//1引数フレームになった時、2引数フレームから走るアニメーションを再開する
void Runner::RestartRunFrame(int endFrame ,int startFrame)
{
	if (Model::GetAnimFrame(_hRunnerModel) == endFrame)
	{
		Model::SetAnimFrame(_hRunnerModel, startFrame, 150, 3);
		_reStartRunFlg = true;
	}
}