#pragma once
#include "Engine/GameObject/GameObject.h"
#include "DataDefine.h"

class Runner;
class Alert;

//Wallを管理するクラス
class Wall : public IGameObject
{
protected:
	int _hModel;
	int _lane;
	bool _soundFlg;
	bool _leanFlg;
	Runner* _pRunner;

	int _hRightPassSE;
	int _hLeftPassSE;
		
	stageState _wallType;		//どんな壁なのか

public:
	//コンストラクタ
	Wall(IGameObject* parent);

	//仮想デストラクタ
	virtual ~Wall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//衝突
	virtual void OnCollision(IGameObject *pTarget) override;

	void SetLane(int lane)
	{
		_lane = lane;
	}
	int GetLane()
	{
		return _lane;
	}

	virtual void ModelLoad();

	//wallTypeセッター(どんな壁か設定)
	void SetWallType(stageState wallType)
	{
		_wallType = wallType;
	}

	//wallTypeゲッター
	stageState GetWallType()
	{
		return _wallType;
	}

	//アラートを生成を行う
	virtual void CreateAlert();

};