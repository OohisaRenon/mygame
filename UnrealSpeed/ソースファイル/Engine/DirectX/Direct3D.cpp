#include<assert.h>
#include "Direct3D.h"



namespace Direct3D
{
	LPDIRECT3D9			_pD3d = nullptr;	//Direct3Dオブジェクト
	LPDIRECT3DDEVICE9	_pDevice = nullptr;	//Direct3Dデバイスオブジェクト
	float				_aspect = 1.0f;		//スクリーンのアスペクト比	
	bool		_isDrawCollision = false;	//コリジョンを表示するか
	bool		_isLighting = false;			//ライティングするか

	void Initialize(HWND hWnd, int screenWidth, int screenHeight)
	{
		HRESULT hr;

		//Direct3Dオブジェクトの作成
		_pD3d = Direct3DCreate9(D3D_SDK_VERSION);
		assert(_pD3d >= 0);


		//DIRECT3Dデバイスオブジェクトの作成
		D3DPRESENT_PARAMETERS d3dpp;
		ZeroMemory(&d3dpp, sizeof(d3dpp));
		d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
		d3dpp.BackBufferCount = 1;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.Windowed = GetPrivateProfileInt("SCREEN", "FullScreen", 0, ".\\Data\\setup.ini") != 1;
		d3dpp.EnableAutoDepthStencil = TRUE;
		d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
		d3dpp.BackBufferWidth = screenWidth;
		d3dpp.BackBufferHeight = screenHeight;
		d3dpp.hDeviceWindow = hWnd;
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

		// アンチエイリアシング
		if (GetPrivateProfileInt("RENDER", "AntiAliasing", 0, ".\\Data\\setup.ini") != 0)
		{
			DWORD QualityBackBuffer = 0;
			_pD3d->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, d3dpp.BackBufferFormat,
				d3dpp.Windowed, D3DMULTISAMPLE_4_SAMPLES, &QualityBackBuffer);
			d3dpp.MultiSampleType = D3DMULTISAMPLE_4_SAMPLES;
			d3dpp.MultiSampleQuality = QualityBackBuffer - 1;
		}

		hr = _pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &_pDevice);
		assert(_pDevice > 0);


		//アスペクト比
		_aspect = (float)screenWidth / (float)screenHeight;

		//アルファブレンド
		_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

		//シェーディング
		_pDevice->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);

		//アンビエントライト
		//_pDevice->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);

		//_pDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_ARGB(255, 100, 100, 100));

		//ライティング
		_isLighting = GetPrivateProfileInt("RENDER", "Lighting", 0, ".\\Data\\setup.ini") != 0;
		if (_isLighting)
		{
			_pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
			D3DLIGHT9 lightState;
			ZeroMemory(&lightState, sizeof(lightState));

			lightState.Type = D3DLIGHT_DIRECTIONAL;

			lightState.Direction = D3DXVECTOR3(1, -1, 1);

			lightState.Diffuse.r = 1.0f;
			lightState.Diffuse.g = 1.0f;
			lightState.Diffuse.b = 1.0f;

			lightState.Ambient.r = 1.0f;
			lightState.Ambient.g = 1.0f;
			lightState.Ambient.b = 1.0f;


//			//CHANGE
////			lightState.Type = D3DLIGHT_DIRECTIONAL;
//			lightState.Type = D3DLIGHT_POINT;
//
////			lightState.Direction = D3DXVECTOR3(1, -1, 1);
//			//点光源の配置場所
//			lightState.Position = D3DXVECTOR3(4.5, 20, 80);
//			/*lightState.Range = 0;
//			lightState.Attenuation0 = 0;
//			lightState.Attenuation1 = 0;
//			lightState.Attenuation2 = 0;*/
//			lightState.Range = 50;
//			lightState.Attenuation0 = 0.0f;
//			lightState.Attenuation1 = 0.0000002f;
//			lightState.Attenuation2 = 0.05f;
//
//			lightState.Diffuse.r = 1.0f;
//			lightState.Diffuse.g = 1.0f;
//			lightState.Diffuse.b = 1.0f;
//
//			lightState.Ambient.r = 1.0f;
//			lightState.Ambient.g = 1.0f;
//			lightState.Ambient.b = 1.0f;
//			lightState.Ambient.a = 1.0f;

			_pDevice->SetLight(0, &lightState);

			_pDevice->LightEnable(0, TRUE);
		}
		else
		{
			_pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
		}

		////カメラ
		//D3DXMATRIX view, proj;
		//D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
		//D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)screenWidth / screenHeight, 0.5f, 1000.0f);
		//_pDevice->SetTransform(D3DTS_VIEW, &view);
		//_pDevice->SetTransform(D3DTS_PROJECTION, &proj);

		//コリジョン表示するか
		_isDrawCollision = GetPrivateProfileInt("DEBUG", "ViewCollider", 0, ".\\Data\\setup.ini") != 0;


	}

	//描画開始
	void BeginDraw()
	{
		//画面をクリア
		_pDevice->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

		//描画開始
		_pDevice->BeginScene();
	}

	//描画終了
	void EndDraw()
	{
		//描画終了
		_pDevice->EndScene();

		//スワップ
		_pDevice->Present(nullptr, nullptr, nullptr, nullptr);
	}

	//開放処理
	void Release()
	{
		_pDevice->Release();
		_pD3d->Release();
	}

}