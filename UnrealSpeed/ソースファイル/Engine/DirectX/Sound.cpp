#include<assert.h>
#include "Sound.h"
#include <string>

//コンストラクタ
Sound::Sound():
	_pDSound(nullptr),
	_pPrimary(nullptr)
{
}


//デストラクタ
Sound::~Sound()
{
	_pPrimary->Release();
	_pDSound->Release();
}

//初期化
void Sound::Initialize(HWND hWnd)
{
	HRESULT ret;

	// COMの初期化
	CoInitialize(NULL);

	// DirectSound8を作成
	//第一引数:使用するサウンドデバイス※NULLだとデフォルトのサウンドデバイス
	//第二引数:生成出来たらLPDIRECTSOUND8のポインタが返る
	//第三引数:常にNULL
	ret = DirectSoundCreate8(NULL, &_pDSound, NULL);
	assert(SUCCEEDED(ret));


	// 強調モード
	//第一引数:ウィンドウハンドル
	//第二引数:フラグの論理演算
	//DSSCL_EXCLUSIVE:フォーカスが移ったら鳴らさない
	//DSSCL_PRIORITY:優先レベル(プライマリーバッファのフォーマット指定)
	ret = _pDSound->SetCooperativeLevel(hWnd, DSSCL_EXCLUSIVE | DSSCL_PRIORITY);
	assert(SUCCEEDED(ret));

	//プライマリサウンドバッファの作成
	CreatePrimaryBuffer();
}



//プライマリサウンドバッファの作成
void Sound::CreatePrimaryBuffer()
{
	HRESULT ret;
	WAVEFORMATEX wf;

	// プライマリサウンドバッファの作成
	DSBUFFERDESC dsdesc;
	ZeroMemory(&dsdesc, sizeof(DSBUFFERDESC));
	dsdesc.dwSize = sizeof(DSBUFFERDESC);
	dsdesc.dwFlags = DSBCAPS_PRIMARYBUFFER;		//プライマリバッファの指定
	dsdesc.dwBufferBytes = 0;			//バッファのサイズ(プライマリバッファは絶対に「0」を入れる)
	dsdesc.lpwfxFormat = NULL;
	ret = _pDSound->CreateSoundBuffer(&dsdesc, &_pPrimary, NULL);	//プライマリバッファ生成
	assert(SUCCEEDED(ret));

	// プライマリバッファのステータスを決定
	wf.cbSize = sizeof(WAVEFORMATEX);
	wf.wFormatTag = WAVE_FORMAT_PCM;	//音源のフォーマット(ステレオとかモノラルとか？)
	wf.nChannels = 2;		
	wf.nSamplesPerSec = 44100;		//44k
	wf.wBitsPerSample = 16;			//16ビット
	wf.nBlockAlign = wf.nChannels * wf.wBitsPerSample / 8;
	wf.nAvgBytesPerSec = wf.nSamplesPerSec * wf.nBlockAlign;
	ret = _pPrimary->SetFormat(&wf);
	assert(SUCCEEDED(ret));
}


//ファイルのロード(引数で指定したファイルwaveファイルを読み込む)	※ファイル名はwinmainで指定
HRESULT Sound::Load(LPDIRECTSOUNDBUFFER * dsb, std::string file)
{
	HRESULT ret;
	MMCKINFO mSrcWaveFile;
	MMCKINFO mSrcWaveFmt;
	MMCKINFO mSrcWaveData;
	LPWAVEFORMATEX wf;

	// WAVファイルをロード
	HMMIO hSrc;
	hSrc = mmioOpenA((LPSTR)file.c_str(), NULL, MMIO_ALLOCBUF | MMIO_READ | MMIO_COMPAT);
	assert(hSrc);

	//※waveファイルはチャンクというまとまりで管理されているらしい
	// 'WAVE'チャンクチェック
	ZeroMemory(&mSrcWaveFile, sizeof(mSrcWaveFile));
	ret = mmioDescend(hSrc, &mSrcWaveFile, NULL, MMIO_FINDRIFF);
	assert(mSrcWaveFile.fccType == mmioFOURCC('W', 'A', 'V', 'E'));

	// 'fmt 'チャンクチェック	(そのファイルのフォーマット)	※ステレオとかモノラルとか
	ZeroMemory(&mSrcWaveFmt, sizeof(mSrcWaveFmt));
	ret = mmioDescend(hSrc, &mSrcWaveFmt, &mSrcWaveFile, MMIO_FINDCHUNK);
	assert(mSrcWaveFmt.ckid == mmioFOURCC('f', 'm', 't', ' '));

	// ヘッダサイズの計算
	int iSrcHeaderSize = mSrcWaveFmt.cksize;
	if (iSrcHeaderSize<sizeof(WAVEFORMATEX))
		iSrcHeaderSize = sizeof(WAVEFORMATEX);

	// ヘッダメモリ確保
	wf = (LPWAVEFORMATEX)malloc(iSrcHeaderSize);
	assert(wf);
	ZeroMemory(wf, iSrcHeaderSize);

	// WAVEフォーマットのロード
	ret = mmioRead(hSrc, (char*)wf, mSrcWaveFmt.cksize);
	assert(SUCCEEDED(ret));

	// fmtチャンクに戻る
	mmioAscend(hSrc, &mSrcWaveFmt, 0);

	// dataチャンクを探す
	while (1) {
		// 検索
		ret = mmioDescend(hSrc, &mSrcWaveData, &mSrcWaveFile, 0);
		assert(SUCCEEDED(ret));

		if (mSrcWaveData.ckid == mmioStringToFOURCCA("data", 0))
			break;
		// 次のチャンクへ
		ret = mmioAscend(hSrc, &mSrcWaveData, 0);
	}

	/////////////////////////////////////////////
	//    サウンドバッファはループバッファ    //
	////////////////////////////////////////////

	// サウンドバッファの作成
	DSBUFFERDESC dsdesc;
	ZeroMemory(&dsdesc, sizeof(DSBUFFERDESC));
	dsdesc.dwSize = sizeof(DSBUFFERDESC);
	//DSBCAPS_GETCURRENTPOSITION2:このバッファの現在の再生ポイントをもっと詳しく調べられる
	//DSBCAPS_STATIC:メモリ上にすべてをロードし確保する
	//DSBCAPS_LOCDEFER:バッファをハードウェアかメインメモリにとるか自動で管理させる
	dsdesc.dwFlags = DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_STATIC | DSBCAPS_LOCDEFER;			//使用したい機能に合わせてフラグを設定
	dsdesc.dwBufferBytes = mSrcWaveData.cksize;		//セカンダリーバッファ作成時はサイズを指定
	dsdesc.lpwfxFormat = wf;
	dsdesc.guid3DAlgorithm = DS3DALG_DEFAULT;
	ret = _pDSound->CreateSoundBuffer(&dsdesc, dsb, NULL);
	assert(SUCCEEDED(ret));

	//サウンドバッファはうかつに書き換えられないようにロック->書き込み->アンロックを行う
	// ロック開始
	LPVOID pMem1, pMem2;
	DWORD dwSize1, dwSize2;
	ret = (*dsb)->Lock(0, mSrcWaveData.cksize, &pMem1, &dwSize1, &pMem2, &dwSize2, 0);
	assert(SUCCEEDED(ret));

	// データ書き込み
	mmioRead(hSrc, (char*)pMem1, dwSize1);		//サウンドバッファのサイズとこのバッファのポインタが返る
	mmioRead(hSrc, (char*)pMem2, dwSize2);		//バッファのロックした位置から最後までで1つ、最初からロックした位置の直前までで2つ必要
	//※バッファの最初から最後までロックしたからと言って一つしか必要ないとも限らないので注意(必ず2つ必要)

	// ロック解除
	(*dsb)->Unlock(pMem1, dwSize1, pMem2, dwSize2);

	// ヘッダ用メモリを開放
	free(wf);

	// WAVを閉じる
	mmioClose(hSrc, 0);

	return S_OK;

}

//SE再生
void Sound::PlaySE(LPDIRECTSOUNDBUFFER* dsb)
{
	(*dsb)->SetCurrentPosition(0);
	(*dsb)->Play(0, 0, 0);
}

//BGM再生
void Sound::PlayBGM(LPDIRECTSOUNDBUFFER * dsb)
{
	(*dsb)->SetCurrentPosition(0);
	(*dsb)->Play(0, 0, DSBPLAY_LOOPING);
}
	
//停止
void Sound::Stop(LPDIRECTSOUNDBUFFER* dsb)
{
	(*dsb)->Stop();
}

//開放
void Sound::Release()
{
	_pPrimary->Release();
	_pDSound->Release();

	// COMの終了
	CoUninitialize();
}

