#pragma once

#include "gameObject/gameObject.h"
#include "sceneManager/sceneManager.h"
#include "DirectX\Direct3D.h"
#include "DirectX\Input.h"


//安全にメモリを開放するためのマクロ
#define SAFE_DELETE(p) {if ((p)!=nullptr) { delete (p); (p)=nullptr;}}
#define SAFE_DELETE_ARRAY(p) {if ((p)!=nullptr) { delete[] (p); (p)=nullptr;}}
#define SAFE_DESTROY(p) {if ((p)!=nullptr) { p->Destroy(); (p)=nullptr;}}

//違うcppで実装しても値を保持するための構造体
struct Global
{
	//ウィンドウの背景サイズ
	int screenWidth;
	int screenHeight;
};
extern Global g;
