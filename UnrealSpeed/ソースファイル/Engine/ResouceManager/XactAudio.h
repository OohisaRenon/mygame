#pragma once
#include <string>
#include "xact3.h"


namespace XactAudio
{

	//xactエンジン作成
	void Initialize();

	//WaveBank読み込み
	//引数:読み込みたいWaveBank名を呼び出し側で指定
	void WaveBankLoad(std::string cueName);

	//SoundBank読み込み
	//引数:読み込みたいSoundBank名を呼び出し側で指定
	void SoundBankLoad(std::string cueName);

	//再生
	//引数:読み込みたいSoundBank名、WaveBank名を呼び出し側で指定
	void Play(std::string cueName);

	//停止
	//引数:読み込みたいSoundBank名、WaveBank名を呼び出し側で指定
	void Stop(std::string cueName);

	//解放
	void Release();
};

