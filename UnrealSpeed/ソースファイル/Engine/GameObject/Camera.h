#pragma once
#include "../gameObject/gameObject.h"
// 常にdestinationの位置に動こうとする
// 瞬間的に移動させたいならSetPositionする

//カメラクラス
class Camera : public IGameObject
{
private:
	D3DXVECTOR3 _position;	//カメラの位置
	D3DXVECTOR3 _target;	//カメラの焦点
	D3DXVECTOR3 _camHead;	//カメラの上

	D3DXVECTOR3 _destinationPos;	//目的位置
	bool _isArriving;				//目的位置に到着しているか
	int _waitMoveCnt;				//目的位置に動き始めるまでの時間

	D3DXVECTOR3 _destinationTarget;	//目的焦点
	D3DXVECTOR3 _destinationCamHead;//目的カメラ上位置

public:
	Camera(IGameObject* parent);
	~Camera();

	//初期化（プロジェクション行列作成）
	void Initialize() override;

	//更新（ビュー行列作成）
	void Update() override;

	//描画（必要ないけどオーバーライドしないといけない）
	void Draw() override {};

	//開放（必要ないけどオーバーライドしないといけない）
	void Release() override {};

	//焦点を設定
	void SetTarget(D3DXVECTOR3 target) { _target = target; }

	//カメラの頭の位置の加算(頭の位置が変わると傾く)
	void SetCamLeanX(float sumLean);

	void SumDistinationLeanX(float setLean);

	void SumDistinationPos(D3DXVECTOR3 distinationPos);
	void SumDistinationPosX(float distinationX);
	void SumDistinationPosY(float distinationY);
	void SumDistinationPosZ(float distinationZ);

	bool IsArriving();

	void SumWaitMoveCnt(int sumCnt);

	void SetPosition(D3DXVECTOR3 position) override;
	void SetPosition(float x, float y, float z) override;
};