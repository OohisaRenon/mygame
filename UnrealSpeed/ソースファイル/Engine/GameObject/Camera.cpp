#include "camera.h"
#include "../DirectX/Direct3D.h"
#include "../GameObject/GameObjectManager.h"

#define CAM_FOV 120
#define LEAN_MAX 0.3

//コンストラクタ
Camera::Camera(IGameObject* parent)
	: IGameObject(parent, "Camera"), _target(D3DXVECTOR3(0, 0, 0)), _camHead(D3DXVECTOR3(0, 1, 0)), _destinationPos(D3DXVECTOR3(0, 0, 0)), _isArriving(true), _waitMoveCnt(0), _destinationCamHead(D3DXVECTOR3(0, 1, 0))
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化（プロジェクション行列作成）
void Camera::Initialize()
{
	//プロジェクション行列
	D3DXMATRIX proj;
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(CAM_FOV), Direct3D::_aspect, 0.5f, 100.0f);
	Direct3D::_pDevice->SetTransform(D3DTS_PROJECTION, &proj);

	_destinationPos = _position;
}

//更新（ビュー行列作成）
void Camera::Update()
{
	Transform();

	D3DXVECTOR3 worldPos;
	D3DXVec3TransformCoord(&worldPos, &_position, &_worldMatrix);

	D3DXVECTOR3 worldTarget;
	D3DXVec3TransformCoord(&worldTarget, &_target, &_worldMatrix);

	//ビュー行列
	D3DXMATRIX view;
	D3DXMatrixLookAtLH(&view, &worldPos, &worldTarget, &_camHead);
	Direct3D::_pDevice->SetTransform(D3DTS_VIEW, &view);

	//移動待機でなければ
	if (_waitMoveCnt == 0)
	{
		// 現在位置と目的位置が違う
		if (_destinationPos != _position)
		{
			// 到着していない
			_isArriving = false;
			// 目的位置への距離
			D3DXVECTOR3 disVector = _destinationPos - _position;
			/*D3DXVECTOR3 disVector = _position - _destinationPos;*/
			float distance = D3DXVec3Length(&disVector);
			// 距離がかなり縮まったなら
			if (distance < 0.01f)
			{
				_position = _destinationPos;
			}
			else
			{
				/*disVector *= -1;*/
				disVector *= 0.1;
				_position += disVector;
			}
		}
		else
		{
			_isArriving = true;
		}
	}
	else
	{
		// 移動待機ならカウントを減らす
		_waitMoveCnt--;
	}

	// 現在カメラ上位置と目的カメラ上位置が違う
	if (_destinationCamHead != _camHead)
	{
		float distanceX = _destinationCamHead.x - _camHead.x;
		//近似値で目的位置に現在位置を合わせる
		if (distanceX < 0.1f && distanceX > -0.1f)
		{
			_destinationCamHead.x = _camHead.x;
			return;
		}
		// distanceXが正
		if (distanceX > 0)
		{
			_camHead.x += 0.1f;
		}
		// distanceXが負
		else if (distanceX < 0)
		{
			_camHead.x -= 0.1f;
		}
	}
	else
	{
		// くそきもい、関数化するなりもっといい方法がある
		if (_camHead.x < 0.08f && _camHead.x > -0.08f)
		{
			//現在カメラ上x位置が0に近ければ0にする
			if (_camHead.x < 0.005f && _camHead.x > -0.005f)
			{
				_camHead.x = 0;
			}
			else
			{
				// distanceXが正
				if (_camHead.x > 0)
				{
					_camHead.x -= 0.005f;
				}
				// distanceXが負
				else if (_camHead.x < 0)
				{
					_camHead.x += 0.005f;
				}
			}
		}
		else
		{
			// distanceXが正
			if (_camHead.x > 0)
			{
				_camHead.x -= 0.01f;
			}
			// distanceXが負
			else if (_camHead.x < 0)
			{
				_camHead.x += 0.01f;
			}
		}
	}
}

void Camera::SetCamLeanX(float setLean)
{
	//addLeanが+で加算後傾きすぎなら
	if (setLean > LEAN_MAX)
	{
		_camHead.x = LEAN_MAX;
		return;
	}
	//addLeanが-で加算後傾きすぎなら
	else if(setLean < -LEAN_MAX)
	{
		_camHead.x = -LEAN_MAX;
		return;
	}
	_camHead.x = setLean;
}

void Camera::SumDistinationLeanX(float setLean)
{
	//addLeanが+で加算後傾きすぎなら
	if (_destinationCamHead.x + setLean > LEAN_MAX)
	{
		_destinationCamHead.x = LEAN_MAX;
		return;
	}
	//addLeanが-で加算後傾きすぎなら
	else if (_destinationCamHead.x + setLean < -LEAN_MAX)
	{
		_destinationCamHead.x = -LEAN_MAX;
		return;
	}
	_destinationCamHead.x += setLean;
}

void Camera::SumDistinationPos(D3DXVECTOR3 distinationPos)
{
	_destinationPos += distinationPos;
}

void Camera::SumDistinationPosX(float distinationX)
{
	_destinationPos.x += distinationX;
}

void Camera::SumDistinationPosY(float distinationY)
{
	_destinationPos.y += distinationY;
}

void Camera::SumDistinationPosZ(float distinationZ)
{
	_destinationPos.z += distinationZ;
}

bool Camera::IsArriving()
{
	return _isArriving;
}

void Camera::SumWaitMoveCnt(int sumCnt)
{
	_waitMoveCnt += sumCnt;
}

void Camera::SetPosition(D3DXVECTOR3 position)
{
	_position = position;
	_destinationPos = _position;
}

void Camera::SetPosition(float x, float y, float z)
{
	_position = D3DXVECTOR3(x, y, z);
	_destinationPos = _position;
}