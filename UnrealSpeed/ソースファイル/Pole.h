#pragma once
#include "Engine/GameObject/GameObject.h"


//ステージの横の棒を管理するクラス
class Pole : public IGameObject
{
	int _hModel;    //モデル番号

public:
	//コンストラクタ
	Pole(IGameObject* parent);

	//デストラクタ
	~Pole();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	int GethModel()
	{
		return _hModel;
	}
};