#include "Road.h"

#include "UnrealSpeedProjectDefine.h"

#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Road::Road(IGameObject * parent)
	:IGameObject(parent, "Road"), _hModel(-1)
{
}

//デストラクタ
Road::~Road()
{
}

//初期化
void Road::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/Road.fbx");
	assert(_hModel >= 0);

	_position.z = ONEBLOCK_LENGTH * 30;
}

//更新
void Road::Update()
{
	_position.z -= ONEBLOCK_LENGTH * 0.3f;
	if (_position.z <= -ONEBLOCK_LENGTH * 5)
	{
		KillMe();
	}
}

//描画
void Road::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Road::Release()
{
}