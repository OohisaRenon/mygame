#include "Engine/ResouceManager/Model.h"

#include "UnrealSpeedProjectDefine.h"

#include "Wind.h"

//コンストラクタ
Wind::Wind(IGameObject * parent)
	:IGameObject(parent, "Wind"), _hModel(-1)
{
}

//デストラクタ
Wind::~Wind()
{
}

//初期化
void Wind::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/Wind.fbx");
	assert(_hModel >= 0);
	_position.z = ONEBLOCK_LENGTH * 50;		//生成位置はWallと同じ

	_rotate.y = 180;
}

//更新
void Wind::Update()
{
	_position.z -= ONEBLOCK_LENGTH * 2.5;		//移動速度はWallより速めに設定
	if (_position.z < ONEBLOCK_LENGTH)
	{
		KillMe();
	}
}

//描画
void Wind::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Wind::Release()
{
}