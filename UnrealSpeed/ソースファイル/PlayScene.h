#pragma once
#pragma once

#include "Engine/global.h"
#include "Engine/DirectX/Text.h"

class Runner;
class Camera;
class Stage;

enum
{
	GAMEOVER_UP,
	GAMEOVER_DOWN,
	GOAL_LOGO_UP,
	GOAL_LOGO_CENTER,
	GOAL_LOGO_DOWN,
	GOAL_LOGO_BACK,	//test
	PICTURE_MAX,
};

class PlayScene : public IGameObject 
{
	Runner* _pRunner;
	Camera* _pCam;
	Stage* _pStage;
	bool _deadRunner;
	bool _deadPictFlag;
	bool _isGoal;				//ゴールしているか
	bool _isLoading;			//ロード中か
	bool _isPlay;				//音楽再生しているか
	bool _isMoveLeft;			//左に動いていいか
	bool _isMoveRight;			//右に動いていいか
	int _hPict[PICTURE_MAX];	//画像の番号
	int _loadCount;				//ロードが終わるまでのおおよそのカウント

	//ゲームオーバー画像を表示するX座標の位置
	int _gameoverUpPosX;
	int _gameoverDownPosX;
	int _logoLeftSpeed;			//ロゴが左に動く速度
	int _logoRightSpeed;		//ロゴが右に動く速度
	bool _restartFlg;			//リスタートするかどうか
	bool _titleFlg;				//タイトルへ戻るかどうか
	int _stopPos;				//ロゴを止めたい位置

	//まとめてロードするために
	//邪魔なので代案求
	enum AlertColor
	{
		ALERT_YELLO,
		ALERT_RED,
		ALERT_BLUE,
		ALERT_GREEN,
		ALERT_WHITE,
		ALERT_CYAAN,
		ALERT_MAGENT,
		ALERT_COLOR_MAX
	};

	int _hInitRunnerModel;
	int _hInitWallModel;
	int _hInitStageModel;
	int _hInitSlidingWallModel;
	int _hInitJumpWallModel;
	int _hInitCheckPointModel;
	int _hInitBackgroundModel;
	int _hInitWindModel;
	int _hInitRoadModel;
	int _hInitPoleModel;
	int _hInitGroundModel;
	int _hInitAlertModel[ALERT_COLOR_MAX];

public:
	PlayScene(IGameObject* parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
	bool IsDeadRunner() { return _deadRunner; }
	bool GetIsPlay()	{ return _isPlay; }
	void Restart();
	void Title();
	void LogoMoveRight();
	void LogoMoveLeft();
};