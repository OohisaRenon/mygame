#include "SlidingWall.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"
#include "Engine/gameObject/Camera.h"

#include "UnrealSpeedProjectDefine.h"

#include "Wall.h"
#include "Sencer.h"
#include "Runner.h"

//コンストラクタ
SlidingWall::SlidingWall(IGameObject * parent)
	:Wall(parent), _pSencer(nullptr)
{
	_pSencer = CreateGameObject<Sencer>(this);
	_pSencer->SetPosition(_position);
	_pSencer->SetDetectKey(DIK_SPACE);
}

//デストラクタ
SlidingWall::~SlidingWall()
{
}

void SlidingWall::ModelLoad()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/SlidingWall.fbx");
	assert(_hModel >= 0);
}

void SlidingWall::OnCollision(IGameObject * pTarget)
{
	IGameObject* pRunner = FindObject("Runner");
	if (pTarget == pRunner)
	{
		// 入力がなかったら
		if (!_pSencer->IsInputted())
		{
			((Runner*)pTarget)->Damage();
		}
		KillMe();
	}
}