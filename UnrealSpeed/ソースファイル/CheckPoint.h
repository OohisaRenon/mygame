#pragma once
#include "Engine/GameObject/GameObject.h"

#include "Wall.h"

class Stage;

//チェックポイントを管理するクラス
class CheckPoint : public Wall
{
protected:
	Stage * _pStage;
	bool _collided;

public:
	//コンストラクタ
	CheckPoint(IGameObject* parent);

	//デストラクタ
	~CheckPoint();

	void ModelLoad() override;

	void OnCollision(IGameObject *);

	//アラートの生成を行う
	void CreateAlert() override {};
};