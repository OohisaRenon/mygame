#pragma once
#include "Engine/GameObject/GameObject.h"

#include "Wall.h"

class Sencer;

//ジャンプ壁を管理するクラス
class SlidingWall : public Wall
{
	Sencer* _pSencer;
public:
	//コンストラクタ
	SlidingWall(IGameObject* parent);

	//デストラクタ
	~SlidingWall();

	void ModelLoad() override;

	void OnCollision(IGameObject *);
};