#pragma once
#include "Engine/GameObject/GameObject.h"

#include "CheckPoint.h"

//チェックポイント(両端)を管理するクラス
class CheckPointSub : public CheckPoint
{
public:
	//コンストラクタ
	CheckPointSub(IGameObject* parent);

	//デストラクタ
	~CheckPointSub();

	//初期化
	void ModelLoad() override;

	//何かに当たったら
	void OnCollision(IGameObject * pTarget) override;
};