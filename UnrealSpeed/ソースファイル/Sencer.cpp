#include "Sencer.h"
#include "Engine/DirectX/Input.h"
#include "UnrealSpeedProjectDefine.h"

#include "Wall.h"
#include "Runner.h"

//#define SENCER_WALL_COLLIDER_POSITION D3DXVECTOR3(HALFBLOCK_LENGTH, ONEBLOCK_LENGTH - HALFBLOCK_LENGTH, HALFBLOCK_LENGTH)
#define SENCER_WALL_COLLIDER_SIZE D3DXVECTOR3(ONEBLOCK_LENGTH, ONEBLOCK_LENGTH, ONEBLOCK_LENGTH * 8)

//コンストラクタ
Sencer::Sencer(IGameObject * parent)
	:IGameObject(parent, "Sencer"), _sencerFlg(false), _detectKey(0)
{
}

//デストラクタ
Sencer::~Sencer()
{
}

//初期化
void Sencer::Initialize()
{
	BoxCollider* collision = new BoxCollider(_position, _position);
	AddCollider(collision);
}

//更新
void Sencer::Update()
{
	Runner* pRunner = (Runner*)FindObject("Runner");

	if (pRunner != nullptr)
	{
		D3DXVECTOR3 vecDiff = GetParent()->GetPosition() - D3DXVECTOR3(pRunner->GetPosition());
		float diffLength = D3DXVec3Length(&vecDiff);

		if (!_sencerFlg && ((Wall*)GetParent())->GetLane() == pRunner->GetLane() && diffLength < ONEBLOCK_LENGTH * 20 && Input::IsKeyDown(_detectKey))
		{
 			_sencerFlg = true;
		}
	}
}

//描画
void Sencer::Draw()
{
}

//開放
void Sencer::Release()
{
}

void Sencer::SetDetectKey(int detectKey)
{
	_detectKey = detectKey;
}
