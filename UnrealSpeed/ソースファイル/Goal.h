#pragma once
#include "Engine/GameObject/GameObject.h"

#include "Wall.h"

//ゴール(中央)を管理するクラス
class Goal : public Wall
{
	bool _isGoal;

public:
	//コンストラクタ
	Goal(IGameObject* parent);

	//デストラクタ
	~Goal();

	//初期化
	void ModelLoad() override;

	//何かに当たった時
	void OnCollision(IGameObject *pTarget) override;

	//アラートの生成を行う
	void CreateAlert() override {};
};