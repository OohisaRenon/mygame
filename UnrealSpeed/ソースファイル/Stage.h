#pragma once
#include "Engine/GameObject/GameObject.h"
#include <vector>
#include "StageData.h"
#include "DataDefine.h"

//ステージを管理するクラス
class Stage : public IGameObject
{
	int _hSound;
	int _hModel;
	int _waitCnt;
	int _row;
	int _col;
	StageData* _stage;

	bool _isCreateGoal;
	bool _createAlertFlg;
	int _playingCheckPointNum;
	int _checkPointNum;
	int _reStartRow;
	stageState _state;		//どんな壁なのか

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//マップデータ一列分の生成
	void OneRowDataCreate();

	//引数int地点からコースを再生成
	void ResetStage();
	void SetRestartRow();
	void SetRestartRow(int);
	void SetCheckPointNum(int);
	void SetPlayingCheckPointNum(int);

	int GetCheckPointNum();

	int  GetPlayingCheckPointNum();

	//セッター
	void SetIsCreateGoal(bool isGoal)
	{
		_isCreateGoal = isGoal;
	}

	int GetBGM();
};