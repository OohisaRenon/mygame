#pragma once
#include "Engine/GameObject/GameObject.h"

class Camera;

//◆◆◆を管理するクラス
class Runner : public IGameObject
{
	enum lifeGauge
	{
		LIFE_GAUGE_ZERO,
		LIFE_GAUGE_RED,
		LIFE_GAUGE_YELLOW,
		LIFE_GAUGE_GREEN,
		LIFE_GAUGE_MAX
	};

	int  _hRunnerModel;		//ランナーのモデルハンドル、アニメーション込み
	int  _hRunnerPosMarkerModel;		//走っているラインをわかりやすくするリングのハンドル

	int  _lane;				//現在走っているレーン
	int  _life;				//現在のHP

	bool _blinkFlg;			//点滅しているフラグ(trueでしている最中)
	int  _blinkCnt;			//点滅しているフレーム数
	bool _slidingFlg;		//スライディングしているフラグ(trueでしている最中)
	int  _slidingCnt;		//スライディングしているフレーム数
	bool _jumpFlg;			//スライディングしているフラグ(trueでしている最中)
	int  _jumpCnt;			//スライディングしているフレーム数
	bool _reStartRunFlg;	//走るフレームの補完をしているフラグ(trueでしている最中)

	bool _isGoal;			//ゴールしているかのフラグ(trueでゴールした)
	bool _isLifeDraw;		/*・・・・・・・・・けすかも！！！！！！・・・・・・・・・・・*/
	int  _hLifePict[LIFE_GAUGE_MAX];	//LifeのUI画像のハンドル配列


public:
	//コンストラクタ
	Runner(IGameObject* parent);

	//デストラクタ
	~Runner();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//キー入力の処理
	void InputKey(Camera * pCam);

	//衝突
	void OnCollision(IGameObject *pTarget) override;

	int GetLane()
	{
		return _lane;
	}

	//ゴールフラグのセッター
	void SetIsGoal(bool goalFlag)
	{
		_isGoal = goalFlag;
	}

	//ゴールフラグのゲッター
	bool GetIsGoal()
	{
		return _isGoal;
	}

	//ダメージを受けた処理
	void Damage();

	//1引数フレームになった時、2引数フレームから走るアニメーションを再開する
	void RestartRunFrame(int endFrame, int startFrame);
};