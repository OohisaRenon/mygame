#pragma once
#include "Engine/GameObject/GameObject.h"

enum earType
{
	EAR_RIGHT,
	EAR_LEFT,
	EAR_MAX
};

//◆◆◆を管理するクラス
class Ear : public IGameObject
{
	earType _type;
public:
	//コンストラクタ
	Ear(IGameObject* parent);

	//デストラクタ
	~Ear();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void SetEarType(earType type)
	{
		_type = type;
	}
};