#pragma once
#include "Engine/GameObject/GameObject.h"
#include "DataDefine.h"

//壁の危険表示を管理するクラス
class Alert : public IGameObject
{
	//アラートの色
	enum AlertColor
	{
		//壁を出さ
		ALERT_YELLO = 1,		//普通の壁
		ALERT_RED,				//ジャンプ
		ALERT_BLUE,				//スライディング
		ALERT_GREEN,
		ALERT_WHITE,
		ALERT_CYAAN,
		ALERT_MAGENT,
		ALERT_COLOR_MAX
	};

	int _hModel[ALERT_COLOR_MAX];
	stageState _wallType;

public:
	//コンストラクタ
	Alert(IGameObject* parent);

	//デストラクタ
	~Alert();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//wallTypeのセッター
	void SetWallType(stageState wallType)
	{
		_wallType = wallType;
	}
};