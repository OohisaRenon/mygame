#include "PlayScene.h"
#include "Engine/global.h"
#include "Engine/gameObject/Camera.h"
#include "Engine/ResouceManager/XactAudio.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Model.h"

#include "UnrealSpeedProjectDefine.h"

#include "Stage.h"
#include "Runner.h"
#include "Background.h"

#define RUNNER_CAM_POSITION D3DXVECTOR3(HALFBLOCK_LENGTH, HALFBLOCK_LENGTH + 0.3, -1.5)
#define RUNNER_CAM_TARGET D3DXVECTOR3(HALFBLOCK_LENGTH + ONEBLOCK_LENGTH, HALFBLOCK_LENGTH, 0)
#define DIFFICULTY_MAX 4
#define CHECKPOINT_MAX 9

PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), _pRunner(nullptr), _pCam(nullptr), _deadRunner(false), _deadPictFlag(false), _isGoal(false), _loadCount(0),
	_isLoading(true), _isPlay(false), _gameoverUpPosX(-2000), _gameoverDownPosX(2000), _isMoveLeft(true), _isMoveRight(true), _logoLeftSpeed(300), _logoRightSpeed(300),
	_restartFlg(false), _titleFlg(false), _stopPos(0)
{
}

std::string BGMCuenameAry[DIFFICULTY_MAX][CHECKPOINT_MAX] =
{
	{"EasyBGM", "EasyCheckPoint1", "EasyCheckPoint2", "EasyCheckPoint3", "EasyCheckPoint4", "", "", "", ""},
	{"NormalBGM", "NormalCheckPoint1", "NormalCheckPoint2", "NormalCheckPoint3", "NormalCheckPoint4", "NormalCheckPoint5", "NormalCheckPoint6", "NormalCheckPoint7", "NormalCheckPoint8"},
	{"HardBGM", "", "", "", "", "", "", "", ""},
	{"", "", "", "", "", "", "", "", ""},
};

void PlayScene::Initialize()
{
	////////////////////////////////////////////////////////////////////////////

	//まとめてロード

	//バンク2のテストで書き換え
	XactAudio::WaveBankLoad("Data/Sound/WaveBank.xwb");
	XactAudio::SoundBankLoad("Data/Sound/SoundBank.xsb");

	//ランナー
	_hInitRunnerModel = Model::Load("data/Models/Runner.fbx");
	assert(_hInitRunnerModel >= 0);

	//壁
	_hInitWallModel = Model::Load("data/Models/Wall.fbx");
	assert(_hInitWallModel >= 0);

	//スライディング壁
	_hInitSlidingWallModel = Model::Load("data/Models/SlidingWall.fbx");
	assert(_hInitSlidingWallModel >= 0);

	//ジャンプ壁
	_hInitJumpWallModel = Model::Load("data/Models/JumpWall.fbx");
	assert(_hInitJumpWallModel >= 0);

	//チェックポイント
	_hInitCheckPointModel = Model::Load("data/Models/CheckPoint.fbx");
	assert(_hInitCheckPointModel >= 0);

	//ステージ
	_hInitStageModel = Model::Load("data/Models/Stage.fbx");
	assert(_hInitStageModel >= 0);

	//背景の球
	_hInitBackgroundModel = Model::Load("data/Models/Background.fbx");
	assert(_hInitBackgroundModel >= 0);

	//風
	_hInitWindModel = Model::Load("data/Models/Wind.fbx");
	assert(_hInitWindModel >= 0);

	//レーン上の線
	_hInitRoadModel = Model::Load("data/Models/Road.fbx");
	assert(_hInitRoadModel >= 0);

	//背景の棒
	_hInitPoleModel = Model::Load("data/Models/Pole.fbx");
	assert(_hInitPoleModel >= 0);

	//背景の地面
	_hInitGroundModel = Model::Load("data/models/ground.fbx");
	assert(_hInitGroundModel >= 0);

	//危険表示
	const std::string fileName[ALERT_COLOR_MAX] = {
		"data/Models/AlertYello.fbx",
		"data/Models/AlertRed.fbx",
		"data/Models/AlertBlue.fbx",
		"data/Models/AlertGreen.fbx",
		"data/Models/AlertWhite.fbx",
		"data/Models/AlertCyann.fbx",
		"data/Models/AlertMagenta.fbx",
	};

	//モデルロード
	for (int i = 0; i < ALERT_COLOR_MAX; i++)
	{
		_hInitAlertModel[i] = Model::Load(fileName[i]);
		assert(_hInitAlertModel[i] >= 0);
	}

	///////////////////////////////////////////////////////////////////////////

	CreateGameObject<Background>(this);
	_pStage = CreateGameObject<Stage>(this);
	_pRunner = CreateGameObject<Runner>(this);

	_pCam = CreateGameObject<Camera>(this);
	_pCam->SetPosition(RUNNER_CAM_POSITION);
	_pCam->SetTarget(D3DXVECTOR3(_pRunner->GetPosition().x + HALFBLOCK_LENGTH, _pRunner->GetPosition().y, 6));

	//画像読み込み
	//ゲームオーバー画面のUI
	_hPict[GAMEOVER_UP] = Image::Load("Data/Picture/GameoverUp.png");
	assert(_hPict[GAMEOVER_UP] >= 0);
	_hPict[GAMEOVER_DOWN] = Image::Load("Data/Picture/GameoverDown.png");
	assert(_hPict[GAMEOVER_DOWN] >= 0);

	//ゴール画面のUI
	_hPict[GOAL_LOGO_UP] = Image::Load("data/Picture/GoalLogoUp.png");
	assert(_hPict[GOAL_LOGO_UP] >= 0);
	_hPict[GOAL_LOGO_CENTER] = Image::Load("data/Picture/GoalLogoCenter.png");
	assert(_hPict[GOAL_LOGO_CENTER] >= 0);
	_hPict[GOAL_LOGO_DOWN] = Image::Load("data/Picture/GoalLogoDown.png");
	assert(_hPict[GOAL_LOGO_DOWN] >= 0);
	_hPict[GOAL_LOGO_BACK] = Image::Load("data/Picture/Back.jpg");
	assert(_hPict[GOAL_LOGO_BACK] >= 0);

}

void PlayScene::Update()
{

	//ロード終わるまでカウント
	if (_loadCount < 1)
	{
		_loadCount++;
	}
	else
	{
		if (!_isPlay)
		{
			_isLoading = false;
		}
		else
		{
			_isLoading = true;
		}
	}

	//ロード終わったら再生
	if (!_isLoading)
	{
		// 再生するBGMのナンバーを保存
		_pStage->SetPlayingCheckPointNum(_pStage->GetCheckPointNum());
		// BGMの再生
		XactAudio::Play(BGMCuenameAry[_pStage->GetBGM()][_pStage->GetCheckPointNum()]);
		_isPlay = true;
	}

	//ランナーがゴールしているなら
	if (!_isGoal)
	{
		if (!_deadRunner && (FindObject("Runner") != nullptr))
		{
			_isGoal = _pRunner->GetIsGoal();
		}
	}

	if (_deadRunner)
	{
		_deadPictFlag = true;
	}

	// ランナーが死んだらカメラのターゲットを更新しない
	if (!_deadRunner)
	{
		_deadRunner = (bool)_pRunner->GetStateFlgDead();
		if (!_deadRunner)
		{
			_pCam->SetTarget(D3DXVECTOR3(_pRunner->GetPosition().x + HALFBLOCK_LENGTH, _pRunner->GetPosition().y, 6));
		}
	}

	
	// ランナーが死んでいて、Wallが無い。または、ゴールしている
	if (_deadRunner && (FindObject("Runner") == nullptr) && (FindObject("Wall") == nullptr) || _isGoal)
	{

		//BGM停止
		XactAudio::Stop(BGMCuenameAry[_pStage->GetBGM()][_pStage->GetPlayingCheckPointNum()]);

		//ロゴｼｭｯ!ってやる(右)
		LogoMoveRight();

		//ロゴｼｭｯ!ってやる(左)
		LogoMoveLeft();
	}
}

void PlayScene::Draw()
{

	//ランナー死んだら表示
	if (_deadPictFlag)
	{
		//両サイドからｼｭｯ!ってやる

		//上半分
		D3DXMATRIX matRightMove;
		//移動行列合成
		D3DXMatrixTranslation(&matRightMove, _gameoverUpPosX, 0, 0);
		Image::SetMatrix(_hPict[GAMEOVER_UP], matRightMove * _worldMatrix);
		Image::Draw(_hPict[GAMEOVER_UP]);

		//下半分
		D3DXMATRIX matLeftMove;
		//移動行列合成
		D3DXMatrixTranslation(&matLeftMove, _gameoverDownPosX, 0, 0);
		Image::SetMatrix(_hPict[GAMEOVER_DOWN], matLeftMove * _worldMatrix);
		Image::Draw(_hPict[GAMEOVER_DOWN]);
	}

	if (_isGoal)
	{
		//ゴール時の背景
		Image::SetMatrix(_hPict[GOAL_LOGO_BACK], _worldMatrix);
		Image::Draw(_hPict[GOAL_LOGO_BACK]);

		//右からｼｭｯってやる
		D3DXMATRIX matRightMove;
		D3DXMatrixTranslation(&matRightMove, _gameoverUpPosX, 0, 0);
		Image::SetMatrix(_hPict[GOAL_LOGO_UP], matRightMove * _worldMatrix);
		Image::Draw(_hPict[GOAL_LOGO_UP]);

		//左からｼｭｯってやる
		D3DXMATRIX matLeftMove;
		D3DXMatrixTranslation(&matLeftMove, _gameoverDownPosX, 0, 0);
		Image::SetMatrix(_hPict[GOAL_LOGO_CENTER], matLeftMove * _worldMatrix);
		Image::Draw(_hPict[GOAL_LOGO_CENTER]);

		//右からｼｭｯってやる
		Image::SetMatrix(_hPict[GOAL_LOGO_DOWN], matRightMove * _worldMatrix);
		Image::Draw(_hPict[GOAL_LOGO_DOWN]);

	}
}

void PlayScene::Release()
{
}

void PlayScene::Restart()
{
	_pRunner->KillMe();		//ゴールしてリスタートするとランナーが増えるので
	_pRunner = CreateGameObject<Runner>(this);
	_pCam->SetPosition(RUNNER_CAM_POSITION);
	_pCam->SetTarget(D3DXVECTOR3(_pRunner->GetPosition().x + HALFBLOCK_LENGTH, _pRunner->GetPosition().y, 6));
	_pStage->ResetStage();
	_deadRunner = false;
	_deadPictFlag = false;
	_isGoal = false;
	_pStage->SetIsCreateGoal(!_isGoal);		//ゴールしてないならゴールを生成する( (_isGoal == false) => (SetIsCreateGoal = true) )
	_restartFlg = false;

	//ロゴ関係の値初期化
	_isMoveRight = true;	//フラグ初期化
	_isMoveLeft = true;		//フラグ初期化
	_gameoverUpPosX = -_stopPos ;	//ロゴのポジション初期化
	_gameoverDownPosX = _stopPos ;	//ロゴのポジション初期化
	_stopPos = 0;			//初期化止める位置初期化
	_logoRightSpeed = 300;		//スピードもとに戻す

	// 再生するBGMのナンバーを保存
	_pStage->SetPlayingCheckPointNum(_pStage->GetCheckPointNum());
	// BGMの再生
	XactAudio::Play(BGMCuenameAry[_pStage->GetBGM()][_pStage->GetCheckPointNum()]);
}

void PlayScene::Title()
{
	SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
	pSceneManager->ChangeScene(SCENE_ID_TITLE);
}

void PlayScene::LogoMoveRight()
{
	//入ってくる
	if (_isMoveRight)
	{
		if (_gameoverUpPosX < -200)
		{
			_gameoverUpPosX += _logoRightSpeed;
		}
		else if (_gameoverUpPosX >= -200)
		{
			_logoRightSpeed = 5;
			_gameoverUpPosX += _logoRightSpeed;
		}

		//止まる
		if (_gameoverUpPosX >= _stopPos)
		{
			_gameoverUpPosX = _stopPos;
			_isMoveRight = false;
		}
	}

	//止まったとこから画面外へ
	if (!_isMoveRight)
	{
		_stopPos = 2000;	//止めてほしい位置

		//リスタート
		if (Input::IsKeyDown(DIK_R))
		{
			_logoRightSpeed = 300;
			_restartFlg = true;
		}
		if (_restartFlg)
		{
			_gameoverUpPosX += _logoRightSpeed;
			//下のロゴを動かす方でリスタート処理
		}

		//タイトルへ
		if (Input::IsKeyDown(DIK_ESCAPE))
		{
			_logoRightSpeed = 300;
			_titleFlg = true;
		}
		if (_titleFlg)
		{
			_gameoverUpPosX += _logoRightSpeed;
			//下のロゴを動かす方でタイトルへ戻す処理
		}
	}
}

void PlayScene::LogoMoveLeft()
{
	//入って
	if (_isMoveLeft)
	{

		if (_gameoverDownPosX > 200)
		{
			_gameoverDownPosX -= _logoLeftSpeed;
		}
		else if (_gameoverDownPosX <= 200)
		{
			_logoLeftSpeed = 5;
			_gameoverDownPosX -= _logoLeftSpeed;
		}
		//止まる
		if (_gameoverDownPosX <= 0)
		{
			_gameoverDownPosX = 0;
			_isMoveLeft = false;
		}
	}

	//止まったとこから画面外へ
	if (!_isMoveLeft)
	{
		//リスタート
		if (Input::IsKeyDown(DIK_R))
		{
			_logoLeftSpeed = 300;
			_restartFlg = true;
		}
		if (_restartFlg)
		{
			//中央から画面外まで動かす
			_gameoverDownPosX -= _logoLeftSpeed;
			if (_gameoverDownPosX < _stopPos)
			{
				if (_gameoverDownPosX < -2000)
				{
					//リスタート
					Restart();
				}
			}
		}

		//タイトルへ
		if (Input::IsKeyDown(DIK_ESCAPE))
		{
			_logoLeftSpeed = 300;
			_titleFlg = true;
		}
		if (_titleFlg)
		{
			//停止位置から画面外まで動かす
			_gameoverDownPosX -= _logoLeftSpeed;
			if (_gameoverDownPosX < -_stopPos)
			{
				if (_gameoverDownPosX < -2000)
				{
					//タイトルへ戻す
					Title();
				}
			}
		}
	}
}