#include "Pole.h"
#include "Engine/ResouceManager/Model.h"

#include "UnrealSpeedProjectDefine.h"

#include "Background.h"

//コンストラクタ
Pole::Pole(IGameObject * parent)
	:IGameObject(parent, "Pole"), _hModel(-1)
{
}

//デストラクタ
Pole::~Pole()
{
}

//初期化
void Pole::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/Pole.fbx");
	assert(_hModel >= 0);
	_scale.z *= 0.5f;		//大きさ調整
	_position.z = ONEBLOCK_LENGTH * 120;		//生成位置はWallと同じ
}

//更新
void Pole::Update()
{
	_position.z -= ONEBLOCK_LENGTH * 0.5;		//移動速度はWallより速めに設定
	//ポールが消える位置
	if (_position.z < -2)
	{
		KillMe();
	}
}

//描画
void Pole::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Pole::Release()
{
}