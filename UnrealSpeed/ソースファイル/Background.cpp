#include "Background.h"
#include "Engine/ResouceManager/Model.h"

#include "UnrealSpeedProjectDefine.h"

#include "Wind.h"
#include "Pole.h"
#include "Ground.h"
#include "Road.h"
#include "Runner.h"

//コンストラクタ
Background::Background(IGameObject * parent)
	:IGameObject(parent, "Background"), _hModel(-1), _makeWindCnt(0), _makePoleCnt(0), _makeRoadCnt(0)
{
}

//デストラクタ
Background::~Background()
{
}

//初期化
void Background::Initialize()
{
	srand(time(NULL));

	_position.x = ONEBLOCK_LENGTH;

	//モデルデータのロード
	_hModel = Model::Load("data/Models/Background.fbx");
	assert(_hModel >= 0);

	CreateGameObject<Ground>(this);

	Pole* _pRightPole = CreateGameObject<Pole>(this);
	_pRightPole->SetPosition(D3DXVECTOR3(-HALFBLOCK_LENGTH, 0, 0));
	Pole* _pLeftPole = CreateGameObject<Pole>(this);
	_pLeftPole->SetPosition(D3DXVECTOR3(ONEBLOCK_LENGTH * 6, 0, 0));

}

//更新
void Background::Update()
{
	//ポール生成
	if (_makePoleCnt > 8)
	{
		Pole* _pRightPole = CreateGameObject<Pole>(this);
		_pRightPole->SetPosition(D3DXVECTOR3(-HALFBLOCK_LENGTH, 0, 0));
		Pole* _pLeftPole = CreateGameObject<Pole>(this);
		_pLeftPole->SetPosition(D3DXVECTOR3(ONEBLOCK_LENGTH * 6, 0, 0));
		_makePoleCnt = 0;
	}
	_makePoleCnt++;

	//風生成(左、右、上で3方向で分けて生成)
	if (_makeWindCnt > 5)
	{
		Wind* pLeftWind = CreateGameObject<Wind>(FindObject("Player"));
		pLeftWind->SetPosition(D3DXVECTOR3(rand() % 100 / 10 - 10, rand() % 7 + 3, 0));
		Wind* pRightWind = CreateGameObject<Wind>(FindObject("Player"));
		pRightWind->SetPosition(D3DXVECTOR3(rand() % 100 / 10 + 10, rand() % 7 + 3, 0));
		Wind* pUpWind = CreateGameObject<Wind>(FindObject("Player"));
		pUpWind->SetPosition(D3DXVECTOR3(rand() % 60 - 30 / 10 , rand() % 10 + 7, 0));
		_makeWindCnt = 0;
	}

	_makeWindCnt++;

	//流れる白い線生成
	if (_makeRoadCnt > 30)
	{
		Road* pLeftRoad = CreateGameObject<Road>(this);
		pLeftRoad->SetPosition(D3DXVECTOR3(-ONEBLOCK_LENGTH,0,0));
		Road* pRightRoad = CreateGameObject<Road>(this);
		pRightRoad->SetPosition(D3DXVECTOR3(ONEBLOCK_LENGTH, 0, 0));
		Road* pCenterRoad = CreateGameObject<Road>(this);
		_makeRoadCnt = 0;
	}
	_makeRoadCnt++;
}

//描画
void Background::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Background::Release()
{
}