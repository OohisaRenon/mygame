#include <iostream>
#include <fstream>
#include <assert.h>
#include "StageData.h"
#include "PassStageData.h"

StageData::StageData(int stageNum) : _data(nullptr), _stageNum(stageNum), _stageBGM(-1), _stageSpeed(-1), _stageCol(-1), _stageRow(-1)
{
}

StageData::~StageData()
{
	for (int row = 0; row < _stageRow; row++)
	{
		delete(_data[row]);
	}
	delete(_data);
}

void StageData::LoadData()
{
	std::string diffStr[] =
	{
		//"Test",
		"Easy",
		"Normal",
		"Hard",
		"Endless",
		"UraNormal"
	};
	difficulty diff = PassStageData::GetPlayData();
	std::string str = "Data/stage/stage";
	str += diffStr[diff];
	str += ".txt";
	std::ifstream inputFile(str, std::ios::in);
	if (!inputFile)		//エラーチェック
	{
		assert(false);
	}

	std::string inputStr = "";

	_stageRow = 0;

	while (!inputFile.eof())
	{
		std::string lineBuff;
		std::getline(inputFile, lineBuff);
		inputStr += lineBuff;
		inputStr += ',';
		_stageRow++;
	}
	_stageRow -= DATAFILE_STAGE_OTHER_ROWLEN;
	assert(_stageRow > DATAFILE_STAGE_OTHER_ROWLEN);

	_stageCol = StrPopFrontInt(&inputStr);
	assert(_stageCol >= 0);

	_stageBGM = StrPopFrontInt(&inputStr);
	assert(_stageBGM >= 0);

	_stageSpeed = StrPopFrontInt(&inputStr);
	assert(_stageSpeed >= 0);

	_data = new stageState*[_stageRow];
	for (int row = 0; row < _stageRow; row++)
	{
		_data[row] = new stageState[_stageCol];

		for (int col = 0; col < _stageCol; col++)
		{
			_data[row][col] = (stageState)StrPopFrontInt(&inputStr);
		}
	}

	std::string test = "";
	for (int row = 0; row < _stageRow; row++)
	{
		for (int col = 0; col < _stageCol; col++)
		{
			test += _data[row][col];
		}
	}
}

int StageData::StrPopFrontInt(std::string * str)
{
	// 先頭から改行までの文字列を数値化
	int iNum = stoi(*str);

	// 読んだ場所を削除
	int i = str->find(',');
	*str = str->substr(++i);

	return iNum;
}