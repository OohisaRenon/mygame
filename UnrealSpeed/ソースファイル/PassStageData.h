#pragma once
#include <string>

enum difficulty
{
	DIFF_EASY,
	DIFF_NORMAL,
	DIFF_HARD,
	DIFF_ENDLESS,
};

namespace PassStageData
{
	void SetPlayData(difficulty dataDifficulty);
	difficulty GetPlayData();
}