#pragma once
#include "Engine/GameObject/GameObject.h"

#include "Wall.h"

class Sencer;

//ジャンプ壁を管理するクラス
class JumpWall : public Wall
{
	Sencer* _pSencer;
public:
	//コンストラクタ
	JumpWall(IGameObject* parent);

	//デストラクタ
	~JumpWall();

	void ModelLoad() override;

	void OnCollision(IGameObject *);
};