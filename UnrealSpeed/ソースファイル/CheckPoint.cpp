#include "CheckPoint.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"
#include "Engine/gameObject/Camera.h"

#include "UnrealSpeedProjectDefine.h"

#include "Wall.h"
#include "Sencer.h"
#include "Runner.h"
#include "Stage.h"
#include "CheckPointSub.h"

//コンストラクタ
CheckPoint::CheckPoint(IGameObject * parent)
	:Wall(parent), _pStage(nullptr), _collided(false)
{
}

//デストラクタ
CheckPoint::~CheckPoint()
{
}

void CheckPoint::ModelLoad()
{
	CheckPointSub* pCheckPointSubLeft = CreateGameObject<CheckPointSub>(this);
	pCheckPointSubLeft->_position.x = 0 * ONEBLOCK_LENGTH;

	CheckPointSub* pCheckPointSubRight = CreateGameObject<CheckPointSub>(this);
	pCheckPointSubRight->_position.x = 2 * ONEBLOCK_LENGTH;

	//モデルデータのロード
	_hModel = Model::Load("data/Models/CheckPoint.fbx");
	assert(_hModel >= 0);

	Model::SetAnimFrame(_hModel, 1, 60, 1.0f);

	_scale *= 4;

	_pStage = (Stage*)FindObject("Stage");
}

void CheckPoint::OnCollision(IGameObject * pTarget)
{
	if (!_collided)
	{
		_pStage->SetRestartRow();
		_pStage->SetCheckPointNum(_pStage->GetCheckPointNum() + 1);
		_collided = true;
	}
}