#pragma once
#include <string>
#include "DataDefine.h"

class StageData
{
	stageState** _data;
	int _stageBGM;
	int _stageSpeed;	// 0に近いほど早い(0は禁止)
	int _stageNum;
	int _stageCol;
	int _stageRow;

public:
	StageData(int stageNum);
	~StageData();
	void LoadData();
	int StrPopFrontInt(std::string*);
	
	//Getter
	stageState GetStageState(int row, int col)
	{
		return _data[row][col];
	}
	int GetBGM()
	{
		return _stageBGM;
	}
	int GetSpeed()
	{
		return _stageSpeed;
	}
	int GetStageNum()
	{
		return _stageNum;
	}
	int GetColMax()
	{
		return _stageCol;
	}
	int GetRowMax()
	{
		return _stageRow;
	}
};