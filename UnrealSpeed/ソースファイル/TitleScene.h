#pragma once

#include "StageData.h"
#include "Engine/global.h"

#include <vector>
#include <string>

class Camera;
class Runner;

struct DiffLevel
{
	int level;
	std::string fileName;
};

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{

	int _hModel;   //モデル番号
	int _hRunner;  //モデル番号
	int _selectNum;

	int _hDiff[4];

	D3DXMATRIX _trans;

	Runner* _pRunner;
	Camera* _pCam;


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
