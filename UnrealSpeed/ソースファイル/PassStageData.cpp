#include "PassStageData.h"

namespace PassStageData
{
	difficulty diff;

	void SetPlayData(difficulty dataDifficulty)
	{
		diff = dataDifficulty;
	}

	difficulty GetPlayData()
	{
		return diff;
	}
}