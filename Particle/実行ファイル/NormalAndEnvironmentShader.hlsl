//グローバル変数はHLSL内では変更できない
//実質定数

float4x4 WVP;	//ローカル行列, ワールド行列＊ビュー行列＊プロジェクション行列
float4x4 RS;	//回転行列＊拡大縮小行列を受け取る
float4x4 W;		//ワールド行列を受け取る

float4	LIGHT_DIR;	//光の方向ベクトル(受け取った時点では光->物体のベクトル)
float4	DIFFUSE_COLOR;	//アプリ側から物体の色を受け取る
float4	AMBIENT_COLOR;	//アプリ側から環境光を受け取る
float4	SPECULER_COLOR;	//アプリ側から鏡面反射光の色を受け取る
float4  CAMERA_POS;		//視点(カメラの位置)
float	SPECULER_POWER;	//アプリ側から鏡面反射光の強さを受け取る
bool	IS_SET_TEXTURE;		//テクスチャ貼ったか、貼ってないか
texture TEXTURE;		//テクスチャの情報(色、UV座標)
texture NORMAL_MAP;		//ノーマルマッピング用の画像データ
texture ROUGH_MAP;		//ラフネス用の画像
textureCUBE TEX_CUBE;	//立方体の環境マッピング用の画像

//サンプラー(テクスチャを貼るための設定)
sampler texSampler = sampler_state
{
	//どの値をテクスチャとして設定するのか(今回はグローバル変数のTEXTURE)
	Texture = <TEXTURE>;

	//アンチエリアスをかけるかどうか：LINEAR
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;

	//※テクスチャを繰り返して何枚も貼って使う場合
	//AddressU：UVの「U」
	//AddressV：UVの「V」

	//AddressU = Clamp;		//テクスチャの切れ目以降はテクスチャの左右端の色をずっと表示
	//AddressV = Clamp;		//テクスチャの切れ目以降はテクスチャの上下端の色をずっと表示
	//AddressU = Mirror;	//テクスチャの左右を反転して表示
	//AddressV = Mirror;	//テクスチャの上下を反転して表示
	AddressU = Wrap;		//テクスチャの左右方向に繰り返して何枚も表示
	AddressV = Wrap;		//テクスチャの上下に繰り返して何枚も表示

};

//サンプラー(テクスチャを貼るための設定)
sampler normalSampler = sampler_state
{
	//どの値をテクスチャとして設定するのか(今回はグローバル変数のTEXTURE)
	Texture = <NORMAL_MAP>;

	//アンチエリアスをかけるかどうか：LINEAR
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;

	AddressU = Wrap;		//テクスチャの左右方向に繰り返して何枚も表示
	AddressV = Wrap;		//テクスチャの上下に繰り返して何枚も表示

};

//ラフネス用のサンプラー
sampler roughSampler = sampler_state
{
	//どの値をテクスチャとして設定するのか(今回はグローバル変数のTEXTURE)
	Texture = <ROUGH_MAP>;

	//アンチエリアスをかけるかどうか：LINEAR
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;

	AddressU = Wrap;		//テクスチャの左右方向に繰り返して何枚も表示
	AddressV = Wrap;		//テクスチャの上下に繰り返して何枚も表示

};

//環境マッピング用キューブテクスチャのサンプラー
samplerCUBE cubeSampler = sampler_state
{
	//使用するテクスチャを指定
	Texture = <TEX_CUBE>;

	//アンチエリアスをかけるかどうか：LINEAR
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;

};

//色と頂点の位置を戻り値として使うための構造体
//頂点シェーダーの出力＆ピクセルシェーダーの入力
//※頂点シェーダーからピクセルシェーダーに渡す情報
struct VS_OUT
{
	//セマンティクスで何のデータとして使うのか先に指定する
	//決められたセマンティクスが存在しないときは(TEXCOORD1って書いとけばいい)
	//セマンティクスは絶対に書かなきゃいけない
	float4 pos   : SV_POSITION;	//位置
	float3 eye	 : TEXCOORD1;	//視線ベクトル
	float2 uv	 : TEXCOORD0;	//uv座標
	float3 light : TEXCOORD2;	//ライトの向き(頂点の向きに修正)

	//色が関係するときはfloat4にしないとアルファ値が計算できない
};


////////////////////////////////////////////////////////////////////////
//頂点シェーダーの戻り値をそのままピクセルシェーダーに渡すので
//頂点シェーダーの戻り値とピクセルシェーダーの引数は同じじゃなきゃだめ
/////////////////////////////////////////////////////////////////////////


//頂点シェーダー(バーテクスシェーダー)
//各頂点がどの位置に表示されるのか計算する
//引数：pos (ローカル座標の位置情報)
//引数：normal(頂点の法線情報)
//引数：tangent(接線)
//戻り値: 3次元座標を2次元座標に変換した
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0, float3 tangent : TANGENT)
{
	//出力データ
	VS_OUT outData;		 //↓掛け算の関数(ローカル行列, ワールド行列＊ビュー行列＊プロジェクション行列)
	outData.pos = mul(pos, WVP);	//mulはベクトルと数値と行列の掛け算をしてくれる関数

	//※接線と法線の外積を計算して従法線を求める
	float3 binormal = (float3)cross(tangent, (float3)normal);

	//法線ベクトルに回転、拡大縮小済みの行列をかける
	//どんなシェーディングするときでもやる

	normal = mul(normal, RS);	//物が回った時(拡大縮小した時)に影(法線)も一緒に回る(拡大縮小する)のを防ぐ
	tangent = (float3)mul(tangent, RS);	//物が回った時(拡大縮小した時)に影(接線)も一緒に回る(拡大縮小する)のを防ぐ
	binormal = (float3)mul(binormal, RS);	//物が回った時(拡大縮小した時)に影(従法線)も一緒に回る(拡大縮小する)のを防ぐ

	//方向のみ知りたいベクトルは正規化(長さを1に)しておく
	normal = normalize(normal);		//法線情報を正規化
	tangent = normalize(tangent);	//法線情報を正規化
	binormal = normalize(binormal);	//法線情報を正規化

	////////////////////////
	//視線ベクトルを求める//
	////////////////////////

	//ローカル座標(pos)にワールド座標(W)をかける
	float4 worldPos = mul(pos, W);
	float3 eye = normalize((float3)CAMERA_POS - (float3)worldPos);	//カメラの位置からワールド座標を引いてベクトルを求める

	//視線ベクトルを頂点に合わせて回転
	//軸を変えたいときは内積を使う！
	//変更したい軸と自分との内積
	outData.eye.x = dot(eye, tangent);
	outData.eye.y = dot(eye, binormal);
	outData.eye.z = dot(eye, (float3)normal);

	//ライトの向きを頂点に合わせて回転
	outData.light.x = dot((float3)LIGHT_DIR, tangent);
	outData.light.y = dot((float3)LIGHT_DIR, binormal);
	outData.light.z = dot(LIGHT_DIR, normal);

	//アプリ側から受け取ったUV座標を戻り値用の構造体に登録
	outData.uv = uv;

	//頂点シェーダーの出力
	return outData;
}

//ピクセルシェーダー
//引数：pos (3次元座標を2次元座標に直した頂点の位置情報)
//戻り値：ピクセル色
float4 PS(VS_OUT inData) : COLOR
{

	//法線ベクトルを正規化
	//inData.normal = normalize(inData.normal);

	//法線はノーマルマップの画像データをもとに割り当て
	float3 normal = (float3)tex2D(normalSampler, inData.uv) * 2 - 1;

	//視線ベクトルも正規化
	inData.eye = normalize(inData.eye);

	//ライトの方向を指すベクトル(方向だけ知りたい)
	float3 lightDir = inData.light;		//光の降り注ぐ方向
	lightDir = normalize(lightDir);		//方向だけ知りたいときは正規化(ベクトルの長さを1にする)

	//色の明るさを計算
	//float4 diffuse = clamp( dot(normal, -lightDir), 0, 1);	//法線とライトのベクトルの内積
	float4 diffuse = saturate(dot(normal, -lightDir));	//法線とライトのベクトルの内積
	diffuse.a = 1;	//透明になると困るのでaだけは1で固定しておく


	//テクスチャ貼ってたら
	if (IS_SET_TEXTURE)
	{
		//tex2D：テクスチャを貼る
		//引数：texSampler  サンプラーの名前
		//引数：inDataUV  uv座標
		diffuse *= tex2D(texSampler, inData.uv);
	}

	else
	{
		diffuse *= DIFFUSE_COLOR;	//アプリ側から受け取った色を設定
	}


	//視線の反射ベクトルを求める
	float3 refEye = reflect(inData.eye, normal);

	//立方体環境マッピングをする時にシェーダー側でddsを張り付ける
	//※diffuse * x -> xの値が高いほどざらざら -> (反射しないので)テクスチャの色が出てくる
	//※texCUBE() * y -> yの値はxと足して1.0になればいいので->(1.0 - tex2D(roughSampler, inData.uv))
	//※0.2, 0.8については足して1.0になるようにする
	//diffuse = diffuse * 0.8 + texCUBE(cubeSampler, refEye) * 0.2;
	diffuse = diffuse * tex2D(roughSampler, inData.uv).r + texCUBE(cubeSampler, refEye) * (1.0 - tex2D(roughSampler, inData.uv).r);

	//アプリ側から受け取った環境光を設定
	float4 ambient = AMBIENT_COLOR;

	//鏡面反射光
	//反射ベクトルを求める(使用する関数：reflect)
	//引数:lightDir  光の降り注ぐ方向ベクトル
	//引数:normal  法線ベクトル
	float3 R = reflect(lightDir, normal);

	//鏡面反射光の計算		視線ベクトルと鏡面反射の内積	 //↓ハイライトの濃さは固定で2としておく
	float4 speculer = pow(saturate(dot(R, inData.eye)), SPECULER_POWER) * 2 * SPECULER_COLOR;
	
	//鏡面反射もつるつるの面だけをするようにしたいのでつるつる情報を与える
	speculer *= (1.0 - tex2D(roughSampler, inData.uv).r);

	//頂点シェーダーで計算した色のデータをそのまま返す
	//グローシェーディング
	return ambient + diffuse + speculer;
}

//関数をピクセルシェーダー、頂点シェーダーとして登録
//エントリーポイントみたいなもの
technique
{
	//※車のバックミラーを表示するには、前の景色と後ろの景色を描画するため「pass」は2つ必要
	//0番目のパス
	pass
	{
		//実行するたびにコンパイルするので、実行ファイル+シェーダーのソースを入れないとだめ
		//頂点シェーダーのバージョン3.0でコンパイルしてね！
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}