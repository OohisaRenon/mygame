float4x4 WorldViewProj;		//ワールド座標*ビュー座標*プロジェクション座標
texture BASE;
texture OVERLAY;
texture PRETICLE_RENDER : RENDERCOLORTARGET;	//RENDERCOLORTARGET : レンダリングターゲットに指定可能なサーフェス(動的なテクスチャ？)

//texture Base <
//	string UIName = "Base Texture";
//	string ResourceType = "2D";
//> ;

//2Dのサンプラーはsampler2Dで定義する
//たぶんパーティクルの丸いほう
sampler2D BaseTexture = sampler_state
{
	Texture = <Base>;
	AddressU = Wrap;	//テクスチャの左右方向に繰り返して何枚も表示
	AddressV = Wrap;	//テクスチャの上下に繰り返して何枚も表示
};

//texture Overlay <
//	string UIName = "Overlay Texture";
//	string ResourceType = "2D";
//> ;

//たぶんスプライトに出す色ついてる方
sampler2D OverlayTexture = sampler_state
{
	Texture = <Overlay>;
	AddressU = Wrap;
	AddressV = Wrap;
};

//texture PreRender : RENDERCOLORTARGET
//<
//	string Format = "X8R8G8B8";	//たぶん表示の形式RGBAと変わらないっぽい？
//> ;

//画像合成後の画像データのサンプラー
sampler2D PreRenderSampler = sampler_state
{
	Texture = <PreRender>;
};

//頂点シェーダーで読み込むデータをまとめた構造体
struct VS_INPUT
{
	float4 Position : POSITION0;
	float2 Tex      : TEXCOORD0;
};

//頂点シェーダーで出力するデータをまとめた構造体
struct VS_OUTPUT
{
	float4 Position : POSITION0;
	float2 Tex      : TEXCOORD0;
};

//particle.pngの頂点シェーダー
//※一応エントリーポイントに設定
VS_OUTPUT cap_mainVS(VS_INPUT Input)
{
	VS_OUTPUT Output;

	Output.Position = mul(Input.Position, WorldViewProj);
	Output.Tex = Input.Tex;

	return(Output);
}

//particle.pngのピクセルシェーダー
float4 cap_mainPS(float2 tex: TEXCOORD0) : COLOR
{
	return tex2D(BaseTexture, tex);
}

///////////////////////////////////////////////////////

//頂点シェーダーで読み込むデータをまとめた構造体
struct Overlay_VS_INPUT
{
	float4 Position : POSITION0;
	float2 Texture1 : TEXCOORD0;
};

//頂点シェーダーで出力するデータをまとめた構造体
struct Overlay_VS_OUTPUT
{
	float4 Position : POSITION0;
	float2 Texture1 : TEXCOORD0;
	float2 Texture2 : TEXCOORD1;

};

//overlay.pngとparticle.pngの色を合成
vector blend(vector bottom, vector top)
{
	//Overlay
	/*float r = (top.r + bottom.r <1)? (0) : (top.r + bottom.r - 1);
	float g = (top.g + bottom.g <1)? (0) : (top.g + bottom.g - 1);
	float b = (top.b + bottom.b <1)? (0) : (top.b + bottom.b - 1);

	return  vector(r,g,b,bottom.a);*/

	//Classic color burn
	//return vector(bottom.rgb + top.rgb - 1, bottom.a);

	//Color burn
	//return vector( 1 - (1-bottom.rgb)/top.rgb, bottom.a);

	//Linear light
	
	float r = (top.r < 0.5) ? (bottom.r + 2 * top.r - 1) : (bottom.r + top.r);
	float g = (top.g < 0.5) ? (bottom.g + 2 * top.g - 1) : (bottom.g + top.g);
	float b = (top.b < 0.5) ? (bottom.b + 2 * top.b - 1) : (bottom.b + top.b);

	return  vector(r, g, b, bottom.a);
}

//overray.pngの頂点シェーダー
Overlay_VS_OUTPUT over_mainVS(Overlay_VS_INPUT Input)
{
	Overlay_VS_OUTPUT Output;

	Output.Position = mul(Input.Position, WorldViewProj);
	Output.Texture1 = Input.Texture1;
	Output.Texture2 = Output.Position.xy*float2(0.5, 0.5) + float2(0.5, 0.5);

	return(Output);
}

//overray.pngのピクセルシェーダー
float4 over_mainPS(float2 tex :TEXCOORD0, float2 pos : TEXCOORD1) : COLOR
{
	vector tmp = blend(tex2D(OverlayTexture, pos), tex2D(PreRenderSampler, tex));
	return tmp;
}

//particle用とoverray用でテクニックは2つ用意
technique technique0
{
	pass p0
	{
		CullMode = None;
		VertexShader = compile vs_2_0 cap_mainVS();
		PixelShader = compile ps_2_0 cap_mainPS();
	}

	pass p0
	{
		CullMode = None;
		VertexShader = compile vs_2_0 over_mainVS();
		PixelShader = compile ps_2_0 over_mainPS();
	}
}
