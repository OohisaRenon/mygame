#include <string>
#include <vector>
#include <stack>
#include "Particle.h"
#include "Engine/Model.h"
#include "Engine/Direct3D.h"
#include "Engine/Camera.h"
#include "PlayScene.h"
#include "resource.h"
#include <commctrl.h>

//ソートで使うスワップ処理のマクロ
#define swap(type, pivot, y)    do { type t = pivot; pivot = y; y = t; } while (0)

//////////////////////////////////
//円周率の定数
#define _USE_MATH_DEFINES
#include <math.h>

//組み込み関数
#include "immintrin.h"
//////////////////////////////////

//コンストラクタ
Particle::Particle(IGameObject * parent)
	:IGameObject(parent, "Particle"), pEffect_(nullptr), particleList_(nullptr), pParticleTexture_(nullptr),
	pVertexBuffer_(nullptr), dialogData_(DialogData()),	meetingPoint_(D3DXVECTOR3(0,0,0)),
	moveType_(MOVE_TYPE_ROOP), aliveTime_(300), spiralRadius(1.0f), spiralHeight(0.0f),
	spiralAngleY(1.0f), spiralAngleZ(1.0f)
{
}

//デストラクタ
Particle::~Particle()
{
}

//初期化
void Particle::Initialize()
{
	//画面の更新時間を変更
	Direct3D::SetClearTime(dialogData_.clearTime);

	//エフェクトの初期化
	InitEffect();

	//パーティクルの初期化
	InitParticle();

	//頂点情報を初期化
	InitVartex();

	//テクスチャのデータロード
	LoadTextureData();

}

void Particle::InitEffect()
{
	////シェーダー作成時に、作成失敗したときそのメッセージが入る
	//LPD3DXBUFFER err = 0;

	////HLSLで書いたシェーダーをロード
	////第一引数：デバイスオブジェクト
	////第二引数：使いたいシェーダーのファイル名(パス)
	////第七引数：格納先のポインタ
	//if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice,
	//	"CommonShader.hlsl", NULL, NULL,
	//	D3DXSHADER_DEBUG, NULL, &pEffect_, &err
	//)))
	//{
	//	//シェーダー作成失敗したときにテキストボックス表示
	//	//第一引数：デバイスオブジェクト
	//	//第二引数：エラーメッセージの内容
	//	//第三引数：ステータスバーに表示する内容
	//	//第四引数：なんのボタンを出すか
	//	MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダー作成エラー", MB_OK);
	//}
}

//テクスチャのデータを読み込んでセット
void Particle::LoadTextureData()
{
	//テクスチャ貼りたい
	pParticleTexture_ = new LPDIRECT3DTEXTURE9[TEXTURE_MAX];	//配列の要素数はテクスチャの枚数分
	ZeroMemory(pParticleTexture_, sizeof(LPDIRECT3DTEXTURE9) * TEXTURE_MAX);

	for (int i = 0; i < TEXTURE_MAX; i++)
	{
		//枠線以外すべて透過させた.pngをテクスチャとして貼るとポイントプリミティブの形が変わる
		//※遊び(空白部分)が大きいとその分だけ小さくなっちゃうので、アイコンみたいにギリギリのサイズで作るべき
		D3DXCreateTextureFromFileEx(Direct3D::pDevice, (DATA_PATH + fileName[i] + DATA_TYPE[TYPE_PNG]).c_str(), 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
			D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &pParticleTexture_[i]);
		assert(pParticleTexture_[i] != nullptr);
	}
}

//パーティクルの初期位置や初期速度を設定する
void Particle::InitParticle()
{

	particleList_ = new ParticleData[PARTICLE_COUNT];

	//パーティクル初期化
	for (DWORD i = 0; i < PARTICLE_COUNT; i++)
	{
		//配置
		particleList_[i].position.x = 0;
		particleList_[i].position.y = 0;
		particleList_[i].position.z = 0;

		//色設定
		particleList_[i].color = 
			D3DCOLOR_ARGB((DWORD)dialogData_.color.a,
						  (DWORD)dialogData_.color.r,
						  (DWORD)dialogData_.color.g,
						  (DWORD)dialogData_.color.b);

	}

	//型をセット(なんの情報として使うか, 何の情報を持たせるか)
	//※今回は位置と点のサイズと色
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_PSIZE | D3DFVF_DIFFUSE);

	//ピクセル1つ1つに色をつけてくれるかどうか
	//true : UV座標をパーティクル(頂点)ごとに設定する, false : すべてのパーティクル(頂点)に同じ座標を与える
	Direct3D::pDevice->SetRenderState(D3DRS_POINTSPRITEENABLE, TRUE);

	//これでパーティクルのサイズが決まる
	//※書かないとパーティクルのサイズが1になる
	Direct3D::pDevice->SetRenderState(D3DRS_POINTSIZE, *((DWORD*)&particleList_->size));

	//ポイントスプライト(パーティクル)もカメラを引いたら小さく、寄せたら大きく見えてほしいので
	Direct3D::pDevice->SetRenderState(D3DRS_POINTSCALEENABLE, TRUE);
	Direct3D::pDevice->SetRenderState(D3DRS_POINTSCALE_A, *((DWORD*)&particleList_->size));
	Direct3D::pDevice->SetRenderState(D3DRS_POINTSCALE_B, *((DWORD*)&particleList_->size));
	Direct3D::pDevice->SetRenderState(D3DRS_POINTSCALE_C, *((DWORD*)&particleList_->size));

}

//パーティクルをソートする
//※パーティクル同士が重なった時正常にアルファブレンドの計算がされない場合があるので
void Particle::CameraDistSort()
{

	int left = 0;		//ソートする配列の最初の添え字	※基本0
	int right = PARTICLE_COUNT - 1;		//ソートする配列の最初の添え字	※今回は PARTICLE_COUNT - 1 で固定

	//1. カメラの視線ベクトルをとってきて正規化する
	//2. カメラの位置から各パーティクルまでのベクトルを計算(particlePos - camPos)
	//3. 1,2の内積をとってカメラの視線ベクトルにカメラからのパーティクルまでの距離を投影する
	//4. 各パーティクルの3を比較して降順にソート

	//カメラからパーティクルまでの距離を測ってX軸との内積をとる
	Camera* pCam = nullptr;
	pCam = ((PlayScene*)pParent_)->GetCamObject();	//カメラの位置取得
	D3DXVECTOR3* particleVecAry = new D3DXVECTOR3[PARTICLE_COUNT];		//カメラからパーティクルまでのベクトル
	D3DXVECTOR3 camPos = pCam->GetPosition();	//カメラの位置
	D3DXVECTOR3 camTarget = pCam->GetTarget();	//カメラの焦点(見てる位置)
	D3DXVECTOR3 camEyeVec = camTarget - camPos;	//カメラの視線ベクトル
	//float* distAry = new float[PARTICLE_COUNT];		
	float distAry[PARTICLE_COUNT] = { 0 };		// カメラからパーティクルまでの距離

	//値の設定
	for (DWORD i = 0; i < PARTICLE_COUNT; i++)
	{
		particleVecAry[i] = particleList_[i].position - camPos;	//カメラからパーティクルまでの距離
		distAry[i] = D3DXVec3Dot(&particleVecAry[i], &camEyeVec);		//カメラの視線ベクトルに投影したベクトルの長さ
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//再起しないクイックソート
	//初期値とか設定
	int index = 0;			// 配列の添え字
	int* leftStack = new int[PARTICLE_COUNT];	// 分割すべき先頭要素の添字のスタック
	int* rightStack = new int[PARTICLE_COUNT];	// 分割すべき末尾要素の添字のスタック

	leftStack[index] = left;		//ソートする範囲を選択(ソートする配列の先頭)
	rightStack[index] = drawCount_;		//ソートする範囲を選択(ソートする配列の末尾)
	index++;	//配列をソート(探索)するときは左側の添え字を[1]からスタートさせる


	//スタックの添え字が0(先頭)になるまでループ
	while (index-- > 0)
	{
		left = leftStack[index];		// 左カーソル(スタートは配列の左端)
		right = rightStack[index];		// 右カーソル(スタートは配列の右端)
		int pl = left;
		int pr = right;
		float pivot = distAry[(left + right) / 2];		// 配列の中央の要素を基準値(pivot)とする
		
		do
		{
	
			//基準値より大きい値が出てくるまで右端から始めたインデックスを進める
			//※ループを抜けた時の pr = 基準値より大きい値が入っている配列の要素番号
			//while (particleList_[pl].distAry > pivot.distAry)
			while (distAry[pl] > pivot)
			{
				//インデックスを進める
				pl++;

			}

			//基準値より小さい値が出てくるまで左端から始めたインデックスを進める
			//※ループを抜けた時の pl = 基準値より小さい値が入っている配列の要素番号
			//while (particleList_[pr].distAry < pivot.distAry)
			while (distAry[pr] < pivot)
			{
				//インデックスを進める
				pr--;
			}

			//※↑のwhile()2つを抜けてきた時点で
			// pl = 基準値より大きい値の入った要素番号
			// pr = 基準値より小さい値の入った要素番号
			if (pl <= pr)
			{
				//左に基準値より「小さい値」
				//右に基準値より「大きい値」が入るようにスワップ
				//スワップ後はインデックスを進める
				swap(ParticleData, particleList_[pl], particleList_[pr]);
				pl++;
				pr--;
			}

		// pl++, pr--を続けてplとprが交差する or 重なる(同じ要素を刺す)までループする
		} while (pl <= pr);

		//左側の方が右側より要素数が少ない
		if (left < pr)
		{
			leftStack[index] = left;	// 先頭側のグループ
			rightStack[index] = pr;		// の範囲（添字）を
			index++;					// プッシュする
		}

		//右側の方が左側より要素数が少ない
		if (pl < right)
		{
			leftStack[index] = pl;		// 末尾側のグループ
			rightStack[index] = right;	// の範囲（添字）を
			index++;					// プッシュする
		}
	}

	//生成と逆順に開放していく
	SAFE_DELETE_ARRAY(rightStack);
	SAFE_DELETE_ARRAY(leftStack)
	//SAFE_DELETE_ARRAY(distAry);
	SAFE_DELETE_ARRAY(particleVecAry);
}


//パーティクルを動かすための頂点情報を初期化
void Particle::InitVartex()
{
	//パーティクルが入る頂点バッファ用の領域確保
	//引数 : sizeof(ParticleData) * MAX_PARTICLES 格納する頂点一個当たりのサイズ * 個数
	//   0 : 使用方法
	//D3DFVF〜 : 頂点に何の情報を持たせるか(D3DFVFを書くときは宣言された(数字の低い)順に書く)
	//D3DPOOL_MANAGED : メモリの指定
	//&pVertexBuffer : 頂点バッファの格納先
	//   0 : nullptrとか0で固定
	Direct3D::pDevice->CreateVertexBuffer(sizeof(ParticleData) * PARTICLE_COUNT, 0, D3DFVF_XYZ | D3DFVF_PSIZE | D3DFVF_DIFFUSE,
		D3DPOOL_MANAGED, &pVertexBuffer_, 0);


	//※頂点バッファはVRAM(RAM = Random Access Memory)に格納されてて、いじるのが難しいので頂点バッファをロックしてコピーする
	ParticleData *vertexBuff;
	pVertexBuffer_->Lock(0, 0, (void**)&vertexBuff, 0);
	memcpy(vertexBuff, particleList_, sizeof(ParticleData) * PARTICLE_COUNT);	//メモリの中身をそのままコピーする
	pVertexBuffer_->Unlock();		//解放処理(RAMの値そのまま書き換えるのはよくないので、コピーして解放)
	assert(pVertexBuffer_ != nullptr);

}

//更新
void Particle::Update()
{
	//描画するパーティクルの個数が予期せぬ値になっていないか確認
	CheckDrawCount();

	//※頂点バッファはVRAM(RAM = Random Access Memory)に格納されてて、いじるのが難しいので頂点バッファをロックしてコピーする
	ParticleData *vertexBuff;
	pVertexBuffer_->Lock(0, 0, (void**)&vertexBuff, 0);
	memcpy(vertexBuff, particleList_, sizeof(ParticleData) * drawCount_);	//メモリの中身をそのままコピーする
	pVertexBuffer_->Unlock();		//解放処理(RAMの値そのまま書き換えるのはよくないので、コピーして解放)

	//パーティクル移動
	for (int i = 0; i < (int)drawCount_; i++)
	{
		//ダイアログでいじった項目を更新
		particleList_[i].size = (float)dialogData_.size;
		particleList_[i].speed = (float)dialogData_.speed;
		
		Direct3D::SetBackColor(dialogData_.backColor);	//背景の色
		Direct3D::SetClearTime(dialogData_.clearTime);	//画面のクリア時間

		MoveParticles(i);	//パーティクルを1つずつ動かす

		if ( (particleList_[i].position == dialogData_.meetingPoint && dialogData_.isMoveDiffusion) ||
			 (particleList_[i].position != dialogData_.meetingPoint && dialogData_.isMoveDiffusion == false) )
		{
			//パーティクルの位置が原点の時は進行方向ベクトルを設定する
			InitMoveVec(i);
			
			//色を変える処理
			ColorChange(i);
		}

		//1フレームごとに時間を計測
		particleList_[i].elapsedTime++;
	}

}

void Particle::CheckDrawCount()
{
	//ダイアログの値より描画する数が小さいなら毎フレーム加算
	//※1フレームあたりに生成する個数が増える
	if (dialogData_.count > drawCount_)
	{
		drawCount_ += dialogData_.countPerFrame;

		//加算結果が描画個数の上限を超えた時調整する
		if (drawCount_ > PARTICLE_COUNT)
		{
			drawCount_ = PARTICLE_COUNT;
		}
	}

	//減らす場合はダイアログの値をそのまま反映させる
	else
	{
		drawCount_ = dialogData_.count;
	}
}

//色を変える処理
//引数 : index 色を変えたいパーティクルの番号を指定
//※↑基本的にループのカウントが入る
void Particle::ColorChange(int index)
{
	//入力値
	switch (dialogData_.colorSelectType)
	{

	//完全ランダム
	case COLOR_TYPE_RANDOM:
		particleList_[index].color = D3DCOLOR_ARGB(COLOR_MAX,
			rand() % COLOR_MAX,
			rand() % COLOR_MAX,
			rand() % COLOR_MAX);
		break;

	//ランダムの範囲を指定
	//※ダイアログの値 〜 255	最大値は255で固定
	case COLOR_TYPE_SELECT_RANDOM:
		particleList_[index].color = D3DCOLOR_ARGB(
			rand() % (DWORD)(dialogData_.randomColorMin.a),
			rand() % (DWORD)(dialogData_.randomColorMin.r),
			rand() % (DWORD)(dialogData_.randomColorMin.g),
			rand() % (DWORD)(dialogData_.randomColorMin.b));
		break;

	//入力値そのまま
	case COLOR_TYPE_MANUAL:
	default:
		particleList_[index].color =
			D3DCOLOR_ARGB((DWORD)dialogData_.color.a,
				(DWORD)dialogData_.color.r,
				(DWORD)dialogData_.color.g,
				(DWORD)dialogData_.color.b);
		break;
	}
}

//パーティクルを動かす
//引数 : index ループで使っている配列の添え字
void Particle::MoveParticles(int index)
{

	//パーティクルが集合地点を超えた時のベクトルの動きを制御する
	//※この数値が大きいほど集合地点を通り過ぎた後でも大きく動く
	//※↑数値が大きいほど移動ベクトルの慣性を引き継ぐイメージ
	float resistance = (float)dialogData_.resistance;

	switch(moveType_)
	{
		case MOVE_TYPE_DEFAULT:
			//膨張と収縮を繰り返す(時間が経つにつれて規模が小さくなる)

			//中央に向かって集まる
			//各パーティクル進行方向ベクトル = パーティクルの集合地点 - 各パーティクルの位置
			moveVec_ = meetingPoint_ - particleList_[index].position;
			D3DXVec3Normalize(&moveVec_, &moveVec_);	//方向だけ必要なので正規化
			moveVec_ /= resistance;		//方向ベクトルを減衰

			//パーティクル全体の進行方向のベクトルを各パーティクルの進行方向ベクトル分加算
			//※最初は0だが、2フレーム目以降は進行方向ベクトル分加算されていく
			//※集合地点が動かない場合は方向が変わることはないので、実質スカラー倍
			moveVec_ += particleList_[index].moveDir;
			D3DXVec3Normalize(&moveVec_, &moveVec_);

			//各パーティクルの進行方向ベクトルもパーティクル全体の進行方向ベクトルを入れとく
			particleList_[index].moveDir = moveVec_;

			//ベクトルを確定させたところで進行方向ベクトル * 速度で移動距離を求めて
			//各パーティクルを動かす
			particleList_[index].position += particleList_[index].moveDir * particleList_[index].speed;
			break;

		//ループ用エフェクト
		case MOVE_TYPE_ROOP:
		default:

			// 1  . 任意の軸(1)を一本傾ける
			// 2  . 任意の軸(2)を基準に360度回転させる
			// 3-1. 軸(1)と軸(2)を正規化する
			// 3-2. 正規化した軸(1),軸(2)の内積を算出
			// 3-3. acos()で↑で求めた内積を使って拡散させたい角度を算出
			// 4  . 軸(1),軸(2)の外積を求める
			// 5  . 3で取った角度と、4で取ったベクトル(外積)を使って
			//	    RotationAxis()で「外積のベクトルを軸に」「3の角度」分回転させる


			//回転させたい螺旋状に
			if (dialogData_.isRotate)
			{
				//三次元極座標系に変換するための行列
				//↓公式のようなものらしい	数式を理解する必要なし
				D3DXMATRIX moveMat;
				D3DXMatrixTranslation( &moveMat,
									   spiralRadius * cosf(spiralAngleY) * cosf(spiralAngleZ),	/* X軸 */
									   spiralHeight * sinf(spiralAngleZ),						/* Y軸 */
									   spiralRadius * sinf(spiralAngleY) * cosf(spiralAngleZ) );/* Z軸 */

				//ダイアログの回転軸で回転させる
				D3DXMATRIX matRotate;
				D3DXVec3Normalize(&dialogData_.rotationAxis, &dialogData_.rotationAxis);
				D3DXMatrixRotationAxis(&matRotate, &dialogData_.rotationAxis, D3DXToRadian(spiralAngleZ));

				//行列合成
				moveMat = moveMat * matRotate;

				//↑の行列を使ってベクトルを変換
				D3DXVec3TransformCoord(&particleList_[index].moveDir, &particleList_[index].moveDir, &matRotate);

			}

			//位置を動かす
			particleList_[index].position += particleList_[index].moveDir * particleList_[index].speed / (float)dialogData_.resistance;

			//軌道 : 拡散
			if (dialogData_.isMoveDiffusion)
			{
				//動いた距離 or 時間が が一定に達したら中心付近に戻す	
				//if (D3DXVec3Length(&(particleList_[index].position - meetingPoint_)) > RESET_RANGE_MAX)
				if(particleList_[index].elapsedTime > aliveTime_)
				{
					//particleList_[index].position = D3DXVECTOR3(0, 0, 0);
					particleList_[index].position = dialogData_.meetingPoint;
					particleList_[index].elapsedTime = 0;
				}
			}

			//軌道 : 収縮
			else
			{
				//目的地付近まで来たら
				//if (D3DXVec3Length(&(meetingPoint_ - particleList_[index].position)) <= RESET_POINT)
				if (particleList_[index].elapsedTime > aliveTime_)
				{
					//ランダムもう一度ばらまく
					particleList_[index].position.x = (float)(rand() % RESET_RANGE_MAX) - (RESET_RANGE_MAX / 2);
					particleList_[index].position.y = (float)(rand() % RESET_RANGE_MAX) - (RESET_RANGE_MAX / 2);
					particleList_[index].position.z = (float)(rand() % RESET_RANGE_MAX) - (RESET_RANGE_MAX / 2);

					//進行方向ベクトルに完成した行列を適用
					D3DXVec3TransformCoord(&particleList_[index].moveDir, &particleList_[index].moveDir, &moveDirMat_);
					D3DXVec3Normalize(&particleList_[index].moveDir, &particleList_[index].moveDir);
					particleList_[index].elapsedTime = 0;
				}
			}

			break;
	}
}

//行列生成して進行方向ベクトルを初期化
//引数 : index パーティクルリストの添え字
void Particle::InitMoveVec(int index)
{

	//拡散度(パーティクルをばらまく範囲)を設定
	// *10 /10 をすることで少数第一位までのランダム値を生成
	//※rand() で生成できる値の最大値は「32767」らしいので値をもっと細かくしたい時は別の方法を取る必要がある
	float angle = (float)(rand() % (ANGLE_MAX * 10)) / 10;
	float diffusion = (float)(rand() % (dialogData_.diffusion * 10)) / 10;

	////拡散度(パーティクルをばらまく範囲)を設定
	//D3DXMatrixRotationY(&diffMatRY_, D3DXToRadian(rand() % ANGLE_MAX));
	//D3DXMatrixRotationZ(&diffMatRZ_, D3DXToRadian((rand() % dialogData_.diffusion)));

	D3DXMatrixRotationY(&diffMatRY_, D3DXToRadian(angle));
	D3DXMatrixRotationZ(&diffMatRZ_, D3DXToRadian(diffusion));

	//↑の行列を合成
	diffMat_ = diffMatRZ_ * diffMatRY_;

	//進行方向ベクトルを初期化
	if (dialogData_.isMoveDiffusion)
	{
		//拡散
		moveVec_ = D3DXVECTOR3(0, 1, 0);

		//パーティクルの進む方向を決める(拡散させる)
		//初期位置に戻った時のみ
		D3DXVec3TransformCoord(&moveVec_, &moveVec_, &diffMat_);
		D3DXVec3Normalize(&moveVec_, &moveVec_);

		//生成方向ベクトルを傾ける
		float dot = D3DXVec3Dot(&moveVec_, &dialogData_.createDir);	//回転軸とパーティクルを出したい方向との内積を取る
		float angle = acos(dot);	//拡散させる範囲(円錐の鋭角になってる部分の角度を求める)

		//任意の方向に出すために回転軸とパーティクルを出したい方向との外積を取る	※引数の順番で向きが変わる
		//生成範囲を任意の方向に動かすときに使う
		D3DXVECTOR3 cross;
		D3DXVec3Cross(&cross, &moveVec_, &dialogData_.createDir);

		//任意の軸で放射状に放出する行列完成!!
		D3DXMatrixRotationAxis(&moveDirMat_, &cross, angle);

		//進行方向ベクトルに完成した行列を適用
		D3DXVec3TransformCoord(&moveVec_, &moveVec_, &moveDirMat_);
		D3DXVec3Normalize(&moveVec_, &moveVec_);

		//生成方向がX,Y,Z軸と完全に被ると拡散されないのでもう一度傾ける
		D3DXVec3TransformCoord(&moveVec_, &moveVec_, &diffMatRZ_);

	}

	else
	{
		//収縮
		moveVec_ = meetingPoint_ - particleList_[index].position;

		//回転させるなら
		if (dialogData_.isRotate)
		{
			//ダイアログの回転軸に合わせて回転
			//回転角度は拡散度の項目を変更
			D3DXMATRIX mat;
			D3DXMatrixRotationAxis(&mat, &dialogData_.rotationAxis, (float)dialogData_.diffusion);
			D3DXVec3TransformCoord(&moveVec_, &moveVec_, &mat);
		}

	}

	//ベクトル正規化
	D3DXVec3Normalize(&moveVec_, &moveVec_);

	//各パーティクルの進行方向ベクトルを設定
	particleList_[index].moveDir = moveVec_;

}

//描画
void Particle::Draw()
{
	//アルファブレンド
	//パーティクル同士 or パーティクルと他の物体が重なった時の処理
	switch (dialogData_.alphaBlend)
	{
		//乗算
	case ALPHA_BLEND_MUL:
		Direct3D::pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		Direct3D::pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		Direct3D::pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		break;

		//加算
		//※デフォルトは加算
	case ALPHA_BLEND_ADD:
	default:																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																				
		Direct3D::pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		Direct3D::pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		Direct3D::pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

		break;
	}

	//前後関係を意識する必要のないもの(煙や光などの形が不安定なもの)は
	//ソートを切ってこれで動かした方が負荷がかからない
	//※形がはっきりしたものを表現したいときはNG
	//↑建物とかの普通のオブジェクト
	//※これを適用したものを一番最後に描画しないと前後関係がおかしくなるので注意

	//書き込む (ソートする)
	if (dialogData_.isZWrite)
	{
		//Z軸で見てカメラから遠い順に描画するときれいに描画されるらしいので
		//カメラからのパーティクルの距離でソート
		//※ここパーティクルの描画する個数を動的に変えるならここを変更
		Direct3D::pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
		CameraDistSort();
	}

	//Zバッファへ深度を書き込まない
	else
	{
		Direct3D::pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	}

	//テクスチャステージ(テクスチャの色と透明度をブレンドする順番)の設定
	//SetTextureStageState(stage, type, value) : テクスチャの状態を設定
	//stage : テクスチャを合成する順番0が最初に貼られる、1以降は0が貼られたポリゴンの上に1のテクスチャが貼られる
	//type  : 設定したい項目を決める (列挙型から選択)   ※色なのか、透明度なのか
	//value : typeの項目の設定    ※typeで設定した項目をどのようにいじるのか)
	//テクスチャの透明度をいじる
	//※↓の2行セットで透明度いじれる
	//Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);		//対象 : 使用するアルファ値を設定、テクスチャからアルファ値を読み込んでくる
	//Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);	//演算子 : D3DTOP_SELECTARG1 = ↑のテクスチャのアルファ値そのまま使うよ
	//↑テクスチャのアルファ値そのまま使う = ブレンドの必要なし...第三引数(3行目のSetTextureStageState)は書かなくてよし！
	Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);	//数値1
	Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);	//演算子
	Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);	//数値2

	//テクスチャをセット
	//第一引数はGetSurfaceLevelの第一引数と同じ
	Direct3D::pDevice->SetTexture(0, pParticleTexture_[dialogData_.textureID]);

	//ポイントプリミティブはライティング切らないと色つかないらしい
	//※テクスチャ貼ってても黒くなっちゃう
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	//ストリームに頂点情報を登録
	//頂点バッファとデバイスのオブジェクトをつなげてる
	//引数 : 0 ストリーム番号 たぶん基本0でいい
	//引数 : pVertexBuffer 描画したい物体の頂点データを指定
	//引数 : 0 ↑で指定した頂点データの配列から、描画を開始したいデータが入っている場所を指定する
	//※↑pVertexBuffer_の要素番号 * pVertexBufferのコピー元のデータ型
	//※ここパーティクルの描画する個数を動的に変えるならここを変更
	//引数 : sizeof(ParticleData)
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(ParticleData));

	//どんな形のポリゴンを何個分描画するか
	//D3DPT_POINTLIST : 描画するポリゴンの形
	// 0 : 何番目の頂点データから読み込むか  ※基本最初から読み込みたいから「0」
	//MAX_PARTICLES : 描画したいポリゴンの数
	//※カメラからの距離が遠いやつを最初に描画するといいらしい
	Direct3D::pDevice->DrawPrimitive(D3DPT_POINTLIST, 0, drawCount_);


	////////////////////////////////////////////////////
	//他の物体の描画に影響が出ないように
	//SetRenderState, SetTextureStageState の値を
	//元に戻す
	////////////////////////////////////////////////////


	//ライティング
	//元に戻す
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	//テクスチャとマテリアルのアルファブレンドの設定を元に戻す
	Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);		//対象 : 使用するアルファ値を設定、テクスチャからアルファ値を読み込んでくる
	Direct3D::pDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);	//演算子 : D3DTOP_SELECTARG1 = ↑のテクスチャのアルファ値そのまま使うよ

	//パーティクル同士 or パーティクルとほかの物体 の
	//アルファブレンドの設定を戻す
	Direct3D::pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	Direct3D::pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//Zバッファの深度を書き込むかどうか
	Direct3D::pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	//////////////////////////////////////////////////////////////////////////////////////

}

//開放
void Particle::Release()
{
	SAFE_RELEASE(pVertexBuffer_);
	for (int i = 0; i < TEXTURE_MAX; i++)
	{
		SAFE_RELEASE(pParticleTexture_[i]);	//テクスチャデータはCreate〜関数で作ってるのでRELEASEで解放
	}
	SAFE_DELETE_ARRAY(pParticleTexture_);	//配列そのものはnewしてるのでDELETE
	SAFE_DELETE_ARRAY(particleList_);
	//SAFE_RELEASE(pEffect_);
}

//ダイアログの操作を受けたリソースのIDを受け取って処理を分ける
// hDlg : 対象とするダイアログプロシージャ
//  wp  : 操作されたリソースのID
bool Particle::CommandProcess(HWND hDlg, WPARAM wp)
{
	int err = 0;

	//ダイアログに何かあったらtrue
	switch (LOWORD(wp))
	{

	//アルファブレンドの計算方法
	//今後増やすかもしれないので
	case IDC_RADIO_ALPHA_ADD:
		dialogData_.alphaBlend = ALPHA_BLEND_ADD;
		return TRUE;
	case IDC_RADIO_ALPHA_MUL:
		dialogData_.alphaBlend = ALPHA_BLEND_MUL;
		return TRUE;

	//Zバッファの書き込み ON or OFF
	case IDC_RADIO_ZBUFFER_TRUE:
		dialogData_.isZWrite = true;
		return TRUE;
	case IDC_RADIO_ZBUFFER_FALSE:
		dialogData_.isZWrite = false;
		return TRUE;

	//回転させるかどうか
	case IDC_RADIO_ROTATE_TRUE:
		dialogData_.isRotate = true;
		return TRUE;
	case IDC_RADIO_ROTATE_FALSE:
		dialogData_.isRotate = false;
		return TRUE;

	//パティクルの動き	拡散
	case IDC_RADIO_MOVE_DIFFUSION:
		dialogData_.isMoveDiffusion = true;
		return TRUE;

	//収縮
	case IDC_RADIO_CONTRACTION:
		dialogData_.isMoveDiffusion = false;
		return TRUE;


	//スピンコントロール(エディットボックス)の値を取得
	//サイズ
	case IDC_EDIT_SIZE:
		dialogData_.size = SendMessage(GetDlgItem(hDlg, IDC_SPIN_SIZE), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);

		//オーバーフローした値が入らないように
		//※現時点ではエディットボックスにはオーバーフローした値が入る
		CheckDialogData(dialogData_, CHECK_TYPE_SIZE);

	//移動速度
	case IDC_EDIT_SPEED:
		dialogData_.speed = SendMessage(GetDlgItem(hDlg, IDC_SPIN_SPEED), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_SPEED);

	//抵抗
	//※使わない場合もある
	case IDC_EDIT_RESISTANCE:
		dialogData_.resistance = SendMessage(GetDlgItem(hDlg, IDC_SPIN_RESISTANCE), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_RESISTANCE);

	//個数
	case IDC_EDIT_COUNT:
		dialogData_.count = SendMessage(GetDlgItem(hDlg, IDC_SPIN_COUNT), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_COUNT);
		return TRUE;

	//1フレームあたりに描画する個数
	case IDC_EDIT_COUNT_PER_FRAME:
		dialogData_.countPerFrame = SendMessage(GetDlgItem(hDlg, IDC_SPIN_COUNT_PER_FRAME), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_COUNT_PER_FRAME);
		return TRUE;


	//クリア時間(フレーム単位)
	case IDC_EDIT_CLEAR_TIME:
		dialogData_.clearTime = SendMessage(GetDlgItem(hDlg, IDC_SPIN_CLEAR_TIME), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_CLEAR_TIME);
		return TRUE;

	//パーティクルの色
	//色をランダムで決める
	case IDC_RADIO_COLOR_RANDOM:
		dialogData_.colorSelectType = COLOR_TYPE_RANDOM;
		return TRUE;

	//ランダムの最小値を決める
	case IDC_RADIO_COLOR_SELECT_RANDOM:
	case IDC_EDIT_COLOR_RANDOM_R:	/* 赤 */
	case IDC_EDIT_COLOR_RANDOM_G:	/* 緑 */
	case IDC_EDIT_COLOR_RANDOM_B:	/* 青 */
	case IDC_EDIT_COLOR_RANDOM_ALPHA:	/* 透明度 */
		dialogData_.colorSelectType = COLOR_TYPE_SELECT_RANDOM;
		dialogData_.randomColorMin.r = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_COLOR_RANDOM_R), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.randomColorMin.g = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_COLOR_RANDOM_G), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.randomColorMin.b = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_COLOR_RANDOM_B), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.randomColorMin.a = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_COLOR_RANDOM_ALPHA), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_RANDOM_COLOR);
		return TRUE;

	//色を入力して決める
	case IDC_RADIO_COLOR_MANUAL:
	case IDC_EDIT_COLOR_R:	/* 赤 */
	case IDC_EDIT_COLOR_G:	/* 緑 */
	case IDC_EDIT_COLOR_B:	/* 青 */
	case IDC_EDIT_COLOR_ALPHA:	/* 透明度 */
		dialogData_.colorSelectType = COLOR_TYPE_MANUAL;
		dialogData_.color.r = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_COLOR_R), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.color.g = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_COLOR_G), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.color.b = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_COLOR_B), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.color.a = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_COLOR_ALPHA), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_COLOR);
		return TRUE;

	//背景の色(赤)
	case IDC_EDIT_BACK_COLOR_R:		/* 赤 */
	case IDC_EDIT_BACK_COLOR_G:		/* 緑 */
	case IDC_EDIT_BACK_COLOR_B:		/* 青 */
		dialogData_.backColor.r = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_BACK_COLOR_R), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.backColor.g = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_BACK_COLOR_G), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.backColor.b = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_BACK_COLOR_B), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_BACK_COLOR);
		return TRUE;

	//目的地(正の値)
	//※パーティクルが集合する位置
	case IDC_EDIT_MEETING_X:	/* X座標 */	
	case IDC_EDIT_MEETING_Y:	/* Y座標 */
	case IDC_EDIT_MEETING_Z:	/* Z座標 */
		dialogData_.meetingPoint.x = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_MEETING_X), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.meetingPoint.y = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_MEETING_Y), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.meetingPoint.z = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_MEETING_Z), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_MEETING_POINT);
		return TRUE;

	//目的地(負の値)
	case IDC_EDIT_MEETING_NEGATIVE_X:	/* X座標 */
	case IDC_EDIT_MEETING_NEGATIVE_Y:	/* Y座標 */
	case IDC_EDIT_MEETING_NEGATIVE_Z:	/* Z座標 */
		dialogData_.meetingPoint.x = -(float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_MEETING_NEGATIVE_X), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.meetingPoint.y = -(float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_MEETING_NEGATIVE_Y), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.meetingPoint.z = -(float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_MEETING_NEGATIVE_Z), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_MEETING_POINT);
		return TRUE;
	
	//回転軸
	case IDC_EDIT_ROTATION_X:	/* X */
	case IDC_EDIT_ROTATION_Y:	/* Y */
	case IDC_EDIT_ROTATION_Z:	/* Z */
		dialogData_.rotationAxis.x = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_ROTATION_X), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.rotationAxis.y = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_ROTATION_Y), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.rotationAxis.z = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_ROTATION_Z), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_ROTATION_AXIS);
		return TRUE;

	//生成方向ベクトル	(正の数)
	case IDC_EDIT_CREATE_DIR_X:	/* X */
	case IDC_EDIT_CREATE_DIR_Y:	/* Y */
	case IDC_EDIT_CREATE_DIR_Z:	/* Z */
		dialogData_.createDir.x = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_CREATE_DIR_X), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.createDir.y = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_CREATE_DIR_Y), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.createDir.z = (float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_CREATE_DIR_Z), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_CREATE_DIR);
		return TRUE;

	//生成方向ベクトル	(負の数)
	case IDC_EDIT_CREATE_DIR_NEGATIVE_X:	/* X */
	case IDC_EDIT_CREATE_DIR_NEGATIVE_Y:	/* Y */
	case IDC_EDIT_CREATE_DIR_NEGATIVE_Z:	/* Z */
		dialogData_.createDir.x = -(float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_CREATE_DIR_NEGATIVE_X), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.createDir.y = -(float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_CREATE_DIR_NEGATIVE_Y), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		dialogData_.createDir.z = -(float)SendMessage(GetDlgItem(hDlg, IDC_SPIN_CREATE_DIR_NEGATIVE_Z), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_CREATE_DIR);
		return TRUE;

	//拡散度
	case IDC_EDIT_DIFFUSION:
		dialogData_.diffusion = (unsigned int)SendMessage(GetDlgItem(hDlg, IDC_SPIN_DIFFUSION), UDM_GETPOS32, (WPARAM)0, (LPARAM)0);
		CheckDialogData(dialogData_, CHECK_TYPE_DIFFUSION);
		return TRUE;

	//コンボボックス
	case IDC_COMBO_TEXTURE_TYPE:
		dialogData_.textureID = (textureFile)SendMessage(GetDlgItem(hDlg, IDC_COMBO_TEXTURE_TYPE), CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
	}

	//何もなければfalse
	return FALSE;
}

//ダイアログから受け取った値が上限 or 下限を超えていないかチェックする
//超えていた場合は上限 or 下限の値を入れなおす
//引数 : dialog 確認したいデータの入った構造体	※構造体のサイズが大きいので参照で渡す
//引数 : type 確認したい項目
void Particle::CheckDialogData(DialogData &dialog, checkDataType type)
{

	switch (type)
	{
		//パーティクルの色
		case CHECK_TYPE_COLOR:
		if (dialogData_.color.a > COLOR_MAX)
		{
			dialogData_.color.a = COLOR_MAX;
		}

		if (dialogData_.color.r > COLOR_MAX)
		{
			dialogData_.color.r = COLOR_MAX;
		}

		if (dialogData_.color.g > COLOR_MAX)
		{
			dialogData_.color.g = COLOR_MAX;
		}

		if (dialogData_.color.b > COLOR_MAX)
		{
			dialogData_.color.b = COLOR_MAX;
		}
		break;

		//色のランダムの範囲
		case CHECK_TYPE_RANDOM_COLOR:
		if (dialogData_.randomColorMin.a > COLOR_MAX || dialogData_.randomColorMin.a < 1)
		{
			dialogData_.randomColorMin.a = COLOR_MAX;
		}

		if (dialogData_.randomColorMin.r > COLOR_MAX || dialogData_.randomColorMin.r < 1)
		{
			dialogData_.randomColorMin.r = COLOR_MAX;
		}

		if (dialogData_.randomColorMin.g > COLOR_MAX || dialogData_.randomColorMin.g < 1)
		{
			dialogData_.randomColorMin.g = COLOR_MAX;
		}

		if (dialogData_.randomColorMin.b > COLOR_MAX || dialogData_.randomColorMin.b < 1)
		{
			dialogData_.randomColorMin.b = COLOR_MAX;
		}
		break;

		//背景の色
		case CHECK_TYPE_BACK_COLOR:
		if (dialogData_.backColor.r > COLOR_MAX)
		{
			dialogData_.backColor.r = COLOR_MAX;
		}

		if (dialogData_.backColor.g > COLOR_MAX)
		{
			dialogData_.backColor.g = COLOR_MAX;
		}

		if (dialogData_.backColor.b > COLOR_MAX)
		{
			dialogData_.backColor.b = COLOR_MAX;
		}
		break;

		//集合地点
		case CHECK_TYPE_MEETING_POINT:
		if (dialogData_.meetingPoint.x > MEETING_POS_MAX)
		{
			dialogData_.meetingPoint.x = MEETING_POS_MAX;
		}

		if (dialogData_.meetingPoint.y > MEETING_POS_MAX)
		{
			dialogData_.meetingPoint.y = MEETING_POS_MAX;
		}

		if (dialogData_.meetingPoint.z > MEETING_POS_MAX)
		{
			dialogData_.meetingPoint.z = MEETING_POS_MAX;
		}
		break;
		
		//回転軸
		case CHECK_TYPE_ROTATION_AXIS:
		if (dialogData_.rotationAxis.x > ANGLE_MAX)
		{
			dialogData_.rotationAxis.x = ANGLE_MAX;
		}

		if (dialogData_.rotationAxis.y > ANGLE_MAX)
		{
			dialogData_.rotationAxis.y = ANGLE_MAX;
		}

		if (dialogData_.rotationAxis.z > ANGLE_MAX)
		{
			dialogData_.rotationAxis.z = ANGLE_MAX;
		}

		break;

		//パーティクルの生成方向の角度
		case CHECK_TYPE_CREATE_DIR:
		if (dialogData_.createDir.x > ANGLE_MAX)
		{
			dialogData_.createDir.x = ANGLE_MAX;
		}

		if (dialogData_.createDir.y > ANGLE_MAX)
		{
			dialogData_.createDir.y = ANGLE_MAX;
		}

		if (dialogData_.createDir.z > ANGLE_MAX)
		{
			dialogData_.createDir.z = ANGLE_MAX;
		}			
		break;

		//拡散度
		case CHECK_TYPE_DIFFUSION:
		if (dialogData_.diffusion > ANGLE_MAX || dialogData_.diffusion == NULL)
		{
			dialogData_.diffusion = ANGLE_MAX;
		}
		break;

		//サイズ
		case CHECK_TYPE_SIZE:
		if (dialogData_.size > PARTICLE_SIZE_MAX)
		{
			dialogData_.size = PARTICLE_SIZE_MAX;
		}
		break;

		//速度
		case CHECK_TYPE_SPEED:
		if (dialogData_.speed > SPEED_MAX)
		{
			dialogData_.speed = SPEED_MAX;
		}
		break;

		//画面上に描画する個数
		case CHECK_TYPE_COUNT:
		if (dialogData_.count > PARTICLE_COUNT)
		{
			dialogData_.count = PARTICLE_COUNT;
		}
		break;

		//1フレームあたりに生成する個数
		case CHECK_TYPE_COUNT_PER_FRAME:
		if (dialogData_.countPerFrame > PARTICLE_COUNT)
		{
			dialogData_.countPerFrame = PARTICLE_COUNT - 1;
		}
		break;


		//画面のクリア時間
		case CHECK_TYPE_CLEAR_TIME:
		if (dialogData_.clearTime > CLEAR_TIME_MAX)
		{
			dialogData_.clearTime = CLEAR_TIME_MAX;
		}
		break;

		//抵抗
		case CHECK_TYPE_RESISTANCE:
		if (dialogData_.resistance > RESISTANCE_MAX)
		{
			//上限
			dialogData_.resistance = RESISTANCE_MAX;
		}
		if (dialogData_.resistance < 1)
		{
			//下限
			dialogData_.resistance = 1;
		}
		break;

		default:
		break;
	}

}
