#pragma once
#include <string>
#include "Sprite.h"

namespace Image
{
	struct ImageData
	{
		std::string fileName_;
		Sprite* pSprite_;
		D3DXMATRIX matrix_;

		//コンストラクタ
		ImageData() : fileName_(""), pSprite_(nullptr)
		{
			D3DXMatrixIdentity(&matrix_);
		};
	};

	//スプライトとテクスチャの作成
	//引数 : 読み込むファイルのデータ(winmainの中でファイル名指定)
	int Load(std::string fileName);

	//2D画像の描画
	//引数 : handle 画像番号
	//引数 : color 色を変えたいときは指定する  指定しない時はデフォルト(1,1,1,1)で画像データの色のまま表示
	void Draw(int handle, D3DXCOLOR color = D3DXCOLOR(1,1,1,1));

	//移動、回転、拡大率をいじった後どう動くか確定させる
	//引数 : handle 画像番号
	//引数 : matrix 移動、回転、拡大縮小用の行列
	void SetMatrix(int handle, D3DXMATRIX &matrix);

	//解放
	//引数 : handle 画像番号
	void Release(int handle);

	//ゲーム終了時子供をすべて消す
	void AllRelease();

	//画像データの詳細情報のゲッター
	//引数 : handle 画像番号
	//引数 : data 書き込み用変数
	void GetPictData(int handle, D3DXIMAGE_INFO &data);
};
