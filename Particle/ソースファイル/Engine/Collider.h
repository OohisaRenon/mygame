#pragma once

#include <d3dx9.h>


class IGameObject;

//あたり判定のタイプ
enum ColliderType
{
	COLLIDER_BOX,		//箱型
	COLLIDER_SPHERE,	//球体
	COLLIDER_MAX,
};


//あたり判定を管理するクラス
class Collider
{
	friend class BoxCollider;
	friend class SphereCollider;

protected:
	IGameObject*	pGameObject_;	//この判定をつけたゲームオブジェクト
	ColliderType	type_;			//種類
	D3DXVECTOR3		center_;		//コライダーの中心位置
	LPD3DXMESH		pMesh_;			//テスト表示用の枠
	D3DXVECTOR3		rotate_;		//コライダーの回転した角度
	D3DXVECTOR3		scale_;			//コライダーの拡大率
	D3DXMATRIX		colMat_;		//コライダーをつけたオブジェクトがどの程度回転、拡大縮小したか
	int	hCol_;		//コライダー番号(一つ目のコライダーは番号「0」)

public:
	//コンストラクタ
	Collider();

	//デストラクタ
	virtual ~Collider();

	//接触判定（継承先のSphereColliderかBoxColliderでオーバーライド）
	//引数：target	相手の当たり判定
	//戻値：接触してればtrue
	virtual bool IsHit(Collider* target) = 0;

	//コライダーの持ち主のゲッター
	//戻り値：持ち主のインスタンス
	virtual IGameObject* GetGameObject();

	//コライダーをつけたいゲームオブジェクトのセッター
	//引数：gameObject  コライダーをつけたいオブジェクト(持ち主)
	void SetGameObject(IGameObject* gameObject);

	//コライダーの中心のゲッター
	//戻り値：コライダーの中心
	D3DXVECTOR3 GetCenter();

	//コライダーのハンドルのゲッター
	//戻り値：コライダー番号
	int GetColHandle();

	//ハンドルのセッター
	//引数：handle  設定したいハンドル(コライダー番号)
	void SetColHandle(int handle);

	//箱型同士の衝突判定
	//引数：boxA	１つ目の箱型判定
	//引数：boxB	２つ目の箱型判定
	//戻値：接触していればtrue
	bool IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB);

	//自分のOBBコライダーの面の位置から、衝突判定を行う相手の位置までの距離を返す
	//引数：box  判定するOBBコライダー
	//引数：targetPos   判定する相手のワールド座標位置での位置(コライダーの中心位置)
	//戻り値：OBBコライダーの面の位置から、衝突判定を行う相手の位置までの距離を返す
	float LenOBBToPoint(BoxCollider * boxB, D3DXVECTOR3& targetPos);

	//箱型と球体の衝突判定
	//引数：box	箱型判定
	//引数：sphere	２つ目の箱型判定
	//戻値：接触していればtrue
	bool IsHitBoxVsCircle(BoxCollider* box, SphereCollider* sphere);

	//bool IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB);

	//球体同士の衝突判定
	//引数 circleA： １つ目の球体判定
	//引数 circleB： ２つ目の球体判定
	//引数 colPos： 衝突時の接点の位置 (書き込み用)
	//引数 colCenterA： 衝突時の球体Aの中心座標 (書き込み用)
	//引数 colCenterB： 衝突時の球体Bの中心座標 (書き込み用)
	//戻値：接触していればtrue
	bool IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB,
		D3DXVECTOR3 &colPos = D3DXVECTOR3(0, 0, 0), D3DXVECTOR3 &colCenterA = D3DXVECTOR3(0, 0, 0), D3DXVECTOR3 &colCenterB = D3DXVECTOR3(0, 0, 0));

	//テスト表示用の枠を描画
	//引数：position	位置
	//引数：rotate  回転した角度
	//引数：scale   拡大率
	void Draw(D3DXVECTOR3 position, D3DXVECTOR3 rotate, D3DXVECTOR3 scale);

	//コライダーを回転拡大縮小
	void ColliderTransform(BoxCollider* collider);

	//分離軸上に投影されたboxAとboxBの中心から面までの線分の長さを計算
	//引数：spalationAxis  　分離軸とする単位ベクトルこのベクトルを視点として2次元で当たり判定を行う
	//引数：targetAxis1  	x,y,z1本も分離軸として使用していない方のコライダーの軸(一本目)(長さを含む)
	//引数：targetAxis2 	x,y,z1本も分離軸として使用していない方のコライダーの軸(二本目)(長さを含む)
	//引数：targetAxis3 	x,y,z1本も分離軸として使用していない方のコライダーの軸(三本目)(長さを含む)
	//戻り値：投影線分の長さの合計値(boxAとboxBの中心から面までの線分の長さの合計値)
	float CalcProjectionLine(D3DXVECTOR3* sepalationAxis, D3DXVECTOR3* targetAxis1, D3DXVECTOR3* targetAxis2, D3DXVECTOR3* targetAxis3 = nullptr);

	//衝突した面の法線を算出する
	//引数 targetCollider：衝突した「相手の」コライダー(onCollisionのtargetの方)
	//引数 collisionAxis：衝突軸書き込み用の変数
	//引数 colliderNormal：算出した法線ベクトルを書き込む用の変数
	//引数 isCorner：角に当たったかどうか
	//※角に当たった時のcollisionNormalは(0,0,0)
	void CalcNormalVec(BoxCollider* targetCollider, D3DXVECTOR3 &collisionAxis, D3DXVECTOR3 &collisionNormal, bool &isCorner);

	//めり込んだコライダーの位置を元に戻す
	//引数 targetNormal：衝突した面の法線
	//引数 targetCol：衝突相手のコライダー
	//引数 len：実際に戻す距離
	//引数 moveVec： コライダーをつけたオブジェクトの移動時の速度ベクトル
	void AdjustColliderPosition(D3DXVECTOR3 &targetNormal, BoxCollider *targetCol, float &len, D3DXVECTOR3 &moveVec);

};

