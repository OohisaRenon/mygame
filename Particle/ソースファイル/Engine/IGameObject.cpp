#include "IGameObject.h"
#include "Global.h"
#include "Collider.h"
#include "Direct3D.h"


IGameObject::IGameObject() : IGameObject(nullptr, "")
{
	colliderList_.clear();
}



IGameObject::IGameObject(IGameObject * parent) : 
	IGameObject(parent, "")
{
}

//引数が親のポインタだけだったり、名前だけのコンストラクタが呼ばれたときもこれを呼ぶ
IGameObject::IGameObject(IGameObject * parent, const std::string & name) :		//pParentの初期化は引数で受け取った親を指定
		pParent_(parent), name_(name), dead_(false), isVisible_(true),		//名前の初期化は引数で受け取った名前を指定 
		position_(D3DXVECTOR3(0,0,0)), rotate_(D3DXVECTOR3(0, 0, 0)),
		scale_(D3DXVECTOR3(1, 1, 1)), colPos_(D3DXVECTOR3(1, 1, 1)),
		moveVec_(D3DXVECTOR3(0, 0, 0)), updateTime_(0), moveStart_(0), moveEnd_(0)
{
	D3DXMatrixIdentity(&localMatrix_);
	D3DXMatrixIdentity(&worldMatrix_);
	colliderList_.clear();
}

IGameObject::~IGameObject()
{
	for (auto it = colliderList_.begin(); it != colliderList_.end(); it++)
	{
		SAFE_DELETE(*it);
	}
	colliderList_.clear();
}

//CreateしたオブジェクトをchildListに追加
void IGameObject::PushBackChild(IGameObject* pObj)
{
	childList_.push_back(pObj);
}

//オブジェクトの名前のゲッター
const std::string& IGameObject::GetObjectName()
{
	return name_;
}

// Drawを拒否
void IGameObject::Invisible()
{
	isVisible_ = false;
}

// Drawを許可
void IGameObject::Visible()
{
	isVisible_ = true;
}

void IGameObject::UpdateSub()
{
	//自分のUpdateを呼ぶ
	Update();
	
	Transform();	//オブジェクトの移動、回転、拡大縮小後の情報を登録
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	//衝突時の処理を行う場合と、行わない場合で計測時間が変わってくるので
	//衝突判定の前と後で、計測時間を分けて足すことで衝突時の処理の影響を受けないようにする
	//////////////////////////////////////////////////////////////////////////////////////////////

	if (!colliderList_.empty())
	{
		//現在のシーンと当たり判定をして、再起でその子供すべてと当たり判定をする
		Collision(SceneManager::GetCurrentScene());
	}

	//一番上の親なので、Updateが呼ばれたときにすべての子供のUpdateを呼ぶ
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		//子供がKillMe()を呼んでたら...
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			//消えるときに消える要素の次のイテレータを返す
			it = childList_.erase(it);		//消した瞬間次の要素に移っているのでit++はfor文内に書かない
		}

		else
		{
			//要素を消さなかった時だけイテレータをインクリメント
			it++;
		}
	}
}

void IGameObject::DrawSub()
{
	//まず自分の描画関数を呼ぶ
	//Draw許可されてるなら
	if (isVisible_)
	{
		Draw();


//リリース時は削除
#ifdef _DEBUG
			//コリジョンの描画
		if (Direct3D::isDrawCollision)
		{
			CollisionDraw();
		}
#endif

		//その子供の描画関数も呼ぶ
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}
	}
}

//まとめて解放処理
//上の(親になっている)クラスから順番に解放処理
void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

void IGameObject::KillMe()
{
	//子は親が殺さなきゃいけない(実際殺すのは親クラス)
	//ここですべきなのは自分が死にたいかどうかを聞くだけ
	dead_ = true;
}

//コライダー（衝突判定）を追加する
void IGameObject::AddCollider(Collider* collider)
{
	collider->SetGameObject(this);
	colliderList_.push_back(collider);

	//当たり判定を追加したときにコライダー番号を設定
	//※コライダーリストのサイズが「1」の時 -> コライダー1つのみ -> 最初のコライダーは「0番」にしたい
	//1つ目を0番にしたいのでsize() - 1
	int size = colliderList_.size() - 1;
	collider->SetColHandle(size);
}

//指定した子オブジェクト取得
//引数 : ID 子オブジェクトのID(生成順の早いものから0,1,2...)
IGameObject* IGameObject::GetChild(int ID)
{
	auto it = childList_.begin();
	for (int i = 0; i < ID; i++)
	{
		it++;
	}
	return (*it);
}


//衝突判定
void IGameObject::Collision(IGameObject * pTarget)
{
	//自分同士の当たり判定はしない
	if (this == pTarget)
	{
		return;
	}
	
	//colliderList_のサイズが2以上なら
	//コライダーリストから自分に付いているコライダーすべてと、当たった相手に付いているコライダー全て判定(総当たり)

	//1つのオブジェクトが複数のコリジョン情報を持ってる場合もあるので二重ループ
	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		for (auto j = pTarget->colliderList_.begin(); j != pTarget->colliderList_.end(); j++)
		{
			if ((*i)->IsHit(*j))
			{
				//当たった
				this->OnCollision((*j));
			}
		}
	}

	//子供がいないなら終わり
	if (pTarget->childList_.empty())
		return;

	//子供も当たり判定
	for (auto i = pTarget->childList_.begin(); i != pTarget->childList_.end(); i++)
	{
		Collision(*i);
	}

}


//テスト用の衝突判定枠を表示
void IGameObject::CollisionDraw()
{
	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);	//ワイヤーフレーム(線だけのやつ)
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);				//ライティングOFF
	Direct3D::pDevice->SetTexture(0, nullptr);								//テクスチャなし

	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		(*i)->Draw(position_, rotate_, scale_);
	}

	//固定パイプラインの設定を元に戻す
	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);	//面のあるポリゴン
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);	//シーン全体のライティング
}

void IGameObject::Transform()
{
	//	D3DXMatrixIdentity(&m);		正規化

	//移動行列
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;
	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);

	//回転行列
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));

	//拡大縮小行列
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);

	//行列の合成
	//行列を合成する順番を変えると挙動も変わる(基本は拡大率,回転,移動の順)
	//自分視点の行列
	localMatrix_ = matS * matRX * matRY * matRZ * matT;

	//世界の中心から見た行列
	//worldMatrix = 親クラスがのlocalMatrixと自分のlocalMatrixの合成したもの
	//※worldMatrixは既に親クラスのlocalMatrixが合成されている
	if (pParent_ == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}

	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
}

//位置情報のセッター
void IGameObject::SetPosition(D3DXVECTOR3 position)
{
	position_ = position;
}

//角度のセッター
void IGameObject::SetRotate(D3DXVECTOR3 rotate)
{
	rotate_ = rotate;
}

//拡大縮小のセッター
void IGameObject::SetScale(D3DXVECTOR3 scale)
{
	scale_ = scale;
}

//移動ベクトルのセッター
void IGameObject::SetMoveVec(D3DXVECTOR3 & moveVec)
{
	moveVec_ = moveVec;
}

//衝突地点のセッター
void IGameObject::SetColPos(D3DXVECTOR3 & colPos)
{
	colPos_ = colPos;
}

//更新時間のセッター
double IGameObject::SetUpdateTime(double time)
{
	return updateTime_ = time;
}

//衝突時間のセッター
double IGameObject::SetColTime(double time)
{
	return colTime_ = time;
}

//位置情報のゲッター
D3DXVECTOR3 IGameObject::GetPosition()
{
	return position_;
}

//角度のゲッター
D3DXVECTOR3 IGameObject::GetRotate()
{
	return rotate_;
}

//拡大縮小のゲッター
D3DXVECTOR3 IGameObject::GetScale()
{
	return scale_;
}

//移動ベクトルのゲッター
D3DXVECTOR3 IGameObject::GetMoveVec()
{
	return moveVec_;
}

//衝突地点のゲッター
D3DXVECTOR3 IGameObject::GetColPos()
{
	return colPos_;
}

//更新時間のゲッター
double IGameObject::GetUpdateTime()
{
	return updateTime_;
}

//衝突時間のゲッター
double IGameObject::GetColTime()
{
	return colTime_;
}

//親のゲッター
IGameObject * IGameObject::GetParent()
{
	return pParent_;
}
