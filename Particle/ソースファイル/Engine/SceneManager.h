#pragma once
#include "IGameObject.h"

//各シーンの番号
enum SCENE_ID
{
	SCENE_ID_PLAY,
};

//シーンの切り替えを管理するクラス
class SceneManager : public IGameObject
{
	static SCENE_ID currentSceneID_;	//今表示しているシーンのIDは何か
	static SCENE_ID nextSceneID_;		//次に表示したいシーンの何かIDは何か
	static IGameObject* pCurrentScene_;	//実際に今表示しているシーンは何か

public:

	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//シーンを切り替える
	static void ChangeScene(SCENE_ID next);

	//今どのシーンを表示しているか
	//戻り値：現在表示中のシーン
	static IGameObject* GetCurrentScene();

};