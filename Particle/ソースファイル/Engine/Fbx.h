#pragma once
#include "Global.h"
#include <fbxsdk.h>

#pragma comment(lib,"libfbxsdk-mt.lib")

struct RayCastData
{
	D3DXVECTOR3 origin;	//レイの発射位置(レイの原点)
	D3DXVECTOR3 direction;	//レイの発射方向
	float distance;		//当たった点までの距離
	bool hit;			//レイが当たったかどうかの判定  (当たったらtrue)
};

class Fbx
{

	//HLTLで書いた処理がここに入る
	LPD3DXEFFECT pEffect_;

	//頂点情報の構造体
	struct Vertex
	{
		D3DXVECTOR3 pos;		//位置
		D3DXVECTOR3 normal;		//法線(面の表がどっち側かの情報)
		D3DXVECTOR2 uv;			//貼り付けるテクスチャの座標(二次元座標)
	};

	//Fbxで使うやつ
	FbxManager*  pManager_;
	FbxImporter* pImporter_;
	FbxScene*    pScene_;

	LPDIRECT3DVERTEXBUFFER9  pVertexBuffer_;	//頂点バッファ
	LPDIRECT3DINDEXBUFFER9*  ppIndexBuffer_;	//インデックスバッファ

	LPDIRECT3DTEXTURE9* pTexture_;		//テクスチャ
	D3DMATERIAL9*      pMaterial_;		//マテリアル

	void CheckNode(FbxNode* pNode);
	void CheckMesh(FbxMesh* pMesh);

	int vertexCount_;				//頂点数
	int polygonCount_;				//ポリゴン(三角形)の枚数
	int indexCount_;				//頂点数
	int materialCount_;				//マテリアルの数
	int* polygonCountOfMaterial_;	//マテリアルごとの色が入る配列


public:
	Fbx(LPD3DXEFFECT pEffect);
	~Fbx();

	//モデルデータをFBXファイルからロード
	//引数 : fileName 読み込みたいファイルの名前(パス)
	void Load(const char* fileName);

	//描画
	//引数 : matrix ワールド行列
	void Draw(const D3DXMATRIX &matrix);
	
	//レイキャスト
	//引数 : data レイキャストに必要なデータ
	//引数 : matrix ワールド座標
	void RayCast(RayCastData &data, const D3DXMATRIX &matrix);
	
	//シェーダーアクセス用ポインタのセッター
	//引数 : effect シェーダーアクセス用ポインタ
	void SetEffect(LPD3DXEFFECT effect);
};

