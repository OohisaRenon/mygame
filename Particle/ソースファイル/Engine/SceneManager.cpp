#include "SceneManager.h"
#include "../PlayScene.h"



////////////////////////////////////////////////
//シーンマネージャーはすべてのシーンをinclude//
///////////////////////////////////////////////

//////////////////////////////////////////
//シーンマネージャーの子供は常に一人だけ//
/////////////////////////////////////////


//※staticのついた変数は関数の外側で初期化(コンストラクタもNG)
SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_PLAY;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_PLAY;
IGameObject* SceneManager::pCurrentScene_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneMnager")
{
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	pCurrentScene_ = CreateGameObject<PlayScene>(this);
}

//更新
void SceneManager::Update()
{
	//今のフレームと描画しているシーンと次のフレームで描画したいシーンが違うなら
	if (currentSceneID_ != nextSceneID_)
	{
		//シーンの削除
		auto scene = childList_.begin();
		(*scene)->ReleaseSub();		//シーンの子オブジェクト消去
		SAFE_DELETE(*scene);		//シーンそのものの消去
		childList_.clear();			//シーンを消したときにchildListをきれいにしてから次のシーンに渡す

		switch (nextSceneID_)
		{
		//シーンの型以外は同じなのでSAFE_DELETEのようにマクロにして引数でシーンを渡してもいい
		case SCENE_ID_PLAY: pCurrentScene_ = CreateGameObject<PlayScene>(this);	break;
		}

		//シーンを切り替える
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//次に表示したいシーンをメンバのnextSceneID_にセット
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}

//今どのシーンを表示しているか
//戻り値：現在表示中のシーン
IGameObject * SceneManager::GetCurrentScene()
{
	return pCurrentScene_;
}
