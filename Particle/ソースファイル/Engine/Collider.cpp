#include "BoxCollider.h"
#include "SphereCollider.h"
#include "IGameObject.h"
#include "Direct3D.h"

//コンストラクタ
Collider::Collider():
	pGameObject_(nullptr),pMesh_(nullptr), hCol_(-1)
{
}

//デストラクタ
Collider::~Collider()
{
	if (pMesh_ != nullptr)
	{
		pMesh_->Release();
	}
}

//コライダーの持ち主のゲッター
IGameObject * Collider::GetGameObject()
{
	return pGameObject_;
}

//コライダーをつけたいゲームオブジェクトのセッター
//引数：gameObject  コライダーをつけたいオブジェクト(持ち主)
void Collider::SetGameObject(IGameObject * gameObject)
{
	pGameObject_ = gameObject; 
}

//コライダーの中心位置のゲッター
D3DXVECTOR3 Collider::GetCenter()
{
	return center_;
}

//コライダーのハンドルのゲッター
int Collider::GetColHandle()
{
	return hCol_;
}

//ハンドルのセッター
//引数：handle  設定したいハンドル(コライダー番号)
void Collider::SetColHandle(int handle)
{
	hCol_ = handle;
}

//箱型同士の衝突判定
//引数：boxA	１つ目の箱型判定
//引数：boxB	２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB)
{
	//※boxBが自分のOBB
	//boxAが衝突相手のOBB

	//boxAとboxBの各面同士で当たり判定
	//AとBの上下左右前後の+,-各パターン計6パターンで検証

	ColliderTransform(boxA);
	ColliderTransform(boxB);

	D3DXVECTOR3 boxPosA = boxA->pGameObject_->GetPosition() + boxA->center_;	//衝突相手の移動後のコライダーの中心位置
	D3DXVECTOR3 boxPosB = boxB->pGameObject_->GetPosition() + boxB->center_;	//自分の移動後のコライダーの中心位置

	//コライダーの中心から伸びるx,y,z軸(axisVec[i])の方向ベクトル(これを分離軸として使う)
	//正規化済みなのですべて単位ベクトル(長さ1)
	D3DXVECTOR3 directionAxisAX = boxA->GetAxisVec(0);	//boxAのX軸の方向ベクトル
	D3DXVECTOR3 directionAxisAY = boxA->GetAxisVec(1);	//boxAのY軸の方向ベクトル
	D3DXVECTOR3 directionAxisAZ = boxA->GetAxisVec(2);	//boxAのZ軸の方向ベクトル
	D3DXVECTOR3 directionAxisBX = boxB->GetAxisVec(0);	//boxBのX軸の方向ベクトル
	D3DXVECTOR3 directionAxisBY = boxB->GetAxisVec(1);	//boxBのY軸の方向ベクトル
	D3DXVECTOR3 directionAxisBZ = boxB->GetAxisVec(2);	//boxBのZ軸の方向ベクトル

	//投影線分として使う
	//x,y,z各方向へのコライダーの半径 = 単位ベクトル * その軸の長さ
	//※単位ベクトルで方向を指定、その方向に対して長さをかける
	//↑の例) vec(1,0,0) * len(3) = (3,0,0)  右方向に「3」の長さのベクトル
	D3DXVECTOR3 AxisAX = directionAxisAX * boxA->GetAxisLen(0);	//boxAのX軸視点
	D3DXVECTOR3 AxisAY = directionAxisAY * boxA->GetAxisLen(1);	//boxAのY軸視点
	D3DXVECTOR3 AxisAZ = directionAxisAZ * boxA->GetAxisLen(2);	//boxAのZ軸視点
	D3DXVECTOR3 AxisBX = directionAxisBX * boxB->GetAxisLen(0);	//boxBのX軸視点
	D3DXVECTOR3 AxisBY = directionAxisBY * boxB->GetAxisLen(1);	//boxBのY軸視点
	D3DXVECTOR3 AxisBZ = directionAxisBZ * boxB->GetAxisLen(2);	//boxBのZ軸視点

	//衝突相手の中心から、自分の中心までのベクトル
	//これと15方向から見た自分の中心から相手の重心方向に向かって伸びるAxis〇×の合計を比較する
	D3DXVECTOR3 interval = boxPosA - boxPosB;

	//boxB(自分)とboxA(target)の投影線分の長さを算出
	//※3次元のOBBをx,y,zなど特定の方向から見た時の2次元のものとして衝突判定を行う
	//特定(分離軸)の方向から見た場合の衝突判定を15回繰り返す
	//boxAの方向ベクトル(3) + boxBの方向ベクトル(3) + boxBの各方向ベクトルとboxAの各方向ベクトルとの外積(9)
	//↑のboxBとboxAの外積はAの方向ベクトルとBの方向ベクトルとで総当たり↓
	//(boxAX, boxBX or BY or BZ), (boxAY, boxBX or BY or BZ), (boxAZ, boxBX or BY or BZ)

	////////////////////////////////////////////////////
	//分離軸：AxisBXを視点として二次元に変換して距離を計算
	////////////////////////////////////////////////////

	//分離軸として使用したベクトルの実際の長さ(分離軸方向へのコライダーの半径)
	float projectionLineB = D3DXVec3Length(&AxisBX);

	//分離軸に投影した衝突判定相手のコライダーの半径
	float projectionLineA = CalcProjectionLine(&directionAxisBX, &AxisAX, &AxisAY, &AxisAZ);

	//分離軸と重心間の距離の内積をとることで重心間の距離を分離軸方向から見た二次元座標に変換する
	float len = fabs(D3DXVec3Dot(&interval, &directionAxisBX));

	//重心間の距離がboxAとboxBのコライダーの半径の合計より小さいなら
	if (len > projectionLineA + projectionLineB)
	{
		//衝突していない
		return false;
	}

	//分離軸：AxisBYを視点として同様に計算
	projectionLineB = D3DXVec3Length(&AxisBY);
	projectionLineA = CalcProjectionLine(&directionAxisBY, &AxisAX, &AxisAY, &AxisAZ);
	len = fabs(D3DXVec3Dot(&interval, &directionAxisBY));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：AxisBZを視点として同様に計算
	projectionLineB = D3DXVec3Length(&AxisBZ);
	projectionLineA = CalcProjectionLine(&directionAxisBZ, &AxisAX, &AxisAY, &AxisAZ);
	len = fabs(D3DXVec3Dot(&interval, &directionAxisBZ));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：AxisAXを視点として同様に計算
	projectionLineA = D3DXVec3Length(&AxisAX);
	projectionLineB = CalcProjectionLine(&directionAxisAX, &AxisBX, &AxisBY, &AxisBZ);
	len = fabs(D3DXVec3Dot(&interval, &directionAxisAX));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：AxisAYを視点として同様に計算
	projectionLineA = D3DXVec3Length(&AxisAY);
	projectionLineB = CalcProjectionLine(&directionAxisAY, &AxisBX, &AxisBY, &AxisBZ);
	len = fabs(D3DXVec3Dot(&interval, &directionAxisAY));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：AxisAZを視点として同様に計算
	projectionLineA = D3DXVec3Length(&AxisAZ);
	projectionLineB = CalcProjectionLine(&directionAxisAZ, &AxisBX, &AxisBY, &AxisBZ);
	len = fabs(D3DXVec3Dot(&interval, &directionAxisAZ));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	////////////////////////////////////
	//二本のベクトルの外積を分離軸とする
	////////////////////////////////////

	//分離軸：cross(AX,BX)
	D3DXVECTOR3 cross = D3DXVECTOR3(0, 0, 0);
	D3DXVec3Cross(&cross, &directionAxisAX, &directionAxisBX);	//分離軸用の外積を算出
	//AXとBXを使って作ったcrossはAX,BXに対して垂直  ->  分離軸crossとAX,BXの内積は「0」
	projectionLineA = CalcProjectionLine(&cross, &AxisAY, &AxisAZ);
	projectionLineB = CalcProjectionLine(&cross, &AxisBY, &AxisBZ);
	len = fabs(D3DXVec3Dot(&interval, &cross));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：cross(AY,BX)
	D3DXVec3Cross(&cross, &directionAxisAY, &directionAxisBX);	//分離軸用の外積を算出
	projectionLineA = CalcProjectionLine(&cross, &AxisAX, &AxisAZ);
	projectionLineB = CalcProjectionLine(&cross, &AxisBY, &AxisBZ);
	len = fabs(D3DXVec3Dot(&interval, &cross));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：cross(AZ,BX)
	D3DXVec3Cross(&cross, &directionAxisAZ, &directionAxisBX);	//分離軸用の外積を算出
	projectionLineA = CalcProjectionLine(&cross, &AxisAX, &AxisAY);
	projectionLineB = CalcProjectionLine(&cross, &AxisBY, &AxisBZ);
	len = fabs(D3DXVec3Dot(&interval, &cross));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：cross(AX,BY)
	D3DXVec3Cross(&cross, &directionAxisAX, &directionAxisBY);	//分離軸用の外積を算出
	projectionLineA = CalcProjectionLine(&cross, &AxisAY, &AxisAZ);
	projectionLineB = CalcProjectionLine(&cross, &AxisBX, &AxisBZ);
	len = fabs(D3DXVec3Dot(&interval, &cross));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：cross(AY,BY)
	D3DXVec3Cross(&cross, &directionAxisAY, &directionAxisBY);	//分離軸用の外積を算出
	projectionLineA = CalcProjectionLine(&cross, &AxisAX, &AxisAZ);
	projectionLineB = CalcProjectionLine(&cross, &AxisBX, &AxisBZ);
	len = fabs(D3DXVec3Dot(&interval, &cross));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：cross(AZ,BY)
	D3DXVec3Cross(&cross, &directionAxisAZ, &directionAxisBY);	//分離軸用の外積を算出
	projectionLineA = CalcProjectionLine(&cross, &AxisAY, &AxisAX);
	projectionLineB = CalcProjectionLine(&cross, &AxisBX, &AxisBZ);
	len = fabs(D3DXVec3Dot(&interval, &cross));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：cross(AX,BZ)
	D3DXVec3Cross(&cross, &directionAxisAX, &directionAxisBZ);	//分離軸用の外積を算出
	projectionLineA = CalcProjectionLine(&cross, &AxisAY, &AxisAZ);
	projectionLineB = CalcProjectionLine(&cross, &AxisBX, &AxisBY);
	len = fabs(D3DXVec3Dot(&interval, &cross));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：cross(AY,BZ)
	D3DXVec3Cross(&cross, &directionAxisAY, &directionAxisBZ);	//分離軸用の外積を算出
	projectionLineA = CalcProjectionLine(&cross, &AxisAX, &AxisAZ);
	projectionLineB = CalcProjectionLine(&cross, &AxisBX, &AxisBY);
	len = fabs(D3DXVec3Dot(&interval, &cross));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//分離軸：cross(AZ,BZ)
	D3DXVec3Cross(&cross, &directionAxisAZ, &directionAxisBZ);	//分離軸用の外積を算出
	projectionLineA = CalcProjectionLine(&cross, &AxisAX, &AxisAY);
	projectionLineB = CalcProjectionLine(&cross, &AxisBX, &AxisBY);
	len = fabs(D3DXVec3Dot(&interval, &cross));
	if (len > projectionLineA + projectionLineB)
	{
		return false;
	}

	//どの方向から見ても当たっているなら
	//当たっている
	return true;

}

//自分のOBBコライダーの面の位置から、衝突判定を行う相手の位置までの距離を返す
//引数 box：  判定するOBBコライダー
//引数 targetPos：判定する相手のワールド座標位置での位置(コライダーの中心位置)
//戻り値：OBBコライダーの面の位置から、衝突判定を行う相手の位置までの距離を返す
float Collider::LenOBBToPoint(BoxCollider * box, D3DXVECTOR3& targetPos)
{
	ColliderTransform(box);

	//OBBで計算
	D3DXVECTOR3 vec = D3DXVECTOR3(0, 0, 0);	//はみ出し部分：OBBコライダーから衝突相手の位置までの距離を入れる
	D3DXVECTOR3 boxPos = box->pGameObject_->GetPosition() + box->center_;	//ワールド座標で見たOBBの中心位置
	float axisLen = 0;		//中心からコライダーの面までの軸の長さ
	float times = 0;		//単位ベクトル + はみ出し部分 = 単位ベクトル * times

	for (int i = 0; i < BOX_AXIS_MAX; i++)
	{
		//中心からコライダーの面までの軸の長さを登録
		axisLen = box->GetAxisLen(i);

		//長さが0以下 => 軸がない(計算できない)
		if (axisLen <= 0)
		{
			continue;
		}

		//コライダーの中心から衝突相手(球体)の位置までのベクトル(targetPos - boxPos)と
		//x,y,z各軸の方向を表す単位ベクトル( box->GetShaftVec(i) )の内積をとって
		//各軸の方向ごとにどの程度コライダーの中心から衝突相手(球体)まで離れているかを算出
		//				↓
		//↑の計算結果を各軸の長さで割ることで
		//単位ベクトル(shaftVec)を何倍したか(times)を求める
		times = D3DXVec3Dot(&(targetPos - boxPos), &box->GetAxisVec(i)) / axisLen;


		//絶対値に変換(world座標の+方向にも-方向にも対応できるように)
		//timesは衝突相手(球体)が自分(コライダー)の位置から見て「-方向」にいるのか「+方向」にいるのかによって符号(値が変わる)
		//コライダーから衝突相手(球体)までの「距離だけ」を求めたいので、絶対値をとる
		times = fabs(times);

		//単位ベクトル(1)よりtimesが大きいなら
		//※コライダーの軸(単位ベクトル)より衝突相手(球体)までの距離が長いなら
		if (times > 1)
		{
			//その軸のはみ出し部分としてvecに加算していく
			vec += (1 - times) * axisLen * box->GetAxisVec(i);
		}

	}

	//最後に算出したはみ出し部分のベクトルの長さを返す
	return D3DXVec3Length(&vec);
}

//箱型と球体の衝突判定
//引数 box：箱型判定
//引数 sphere：２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsCircle(BoxCollider* box, SphereCollider* sphere)
{
	//コライダーを持ち主のオブジェクトに合わせて回転、拡大縮小
	ColliderTransform(box);

	D3DXVECTOR3 circlePos = sphere->pGameObject_->GetPosition() + sphere->center_;		//球体コライダーの重心
	D3DXVECTOR3 boxPos = box->pGameObject_->GetPosition() + box->center_;			//箱型コライダーの重心

	//if (circlePos.x > boxPos.x - box->_size.x - sphere->_radius &&
	//	circlePos.x < boxPos.x + box->_size.x + sphere->_radius &&
	//	circlePos.y > boxPos.y - box->_size.y - sphere->_radius &&
	//	circlePos.y < boxPos.y + box->_size.y + sphere->_radius &&
	//	circlePos.z > boxPos.z - box->_size.z - sphere->_radius &&
	//	circlePos.z < boxPos.z + box->_size.z + sphere->_radius )
	//{
	//	return true;
	//}

	//OBBコライダーと球体の中心位置を渡して
	//OBBコライダーから球体の中心までの距離を算出
	float OBBLen = LenOBBToPoint(box, circlePos);
	
	//算出した長さが球体の半径以下なら
	if (OBBLen <= sphere->radius_)
	{
		//衝突している
		return true;
	}

	return false;
}

//bool Collider::IsHitCircleVsCircle(SphereCollider * circleA, SphereCollider * circleB)
//{
//	D3DXVECTOR3 v = (circleA->center_ + circleA->pGameObject_->GetPosition()) 
//		- (circleB->center_ + circleB->pGameObject_->GetPosition());
//
//	if (D3DXVec3Length(&v) <= (circleA->radius_ + circleB->radius_))
//	{
//		return true;
//	}
//}

//球体同士の衝突判定
//引数 circleA：１つ目の球体判定
//引数 circleB：２つ目の球体判定
//引数 colPos： 衝突時の接点の位置
//引数 colCenterA： 衝突時の球体Aの中心座標
//引数 colCenterB： 衝突時の球体Bの中心座標
//戻値：接触していればtrue
bool Collider::IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB,
		D3DXVECTOR3 &colPos, D3DXVECTOR3 &colCenterA, D3DXVECTOR3 &colCenterB)
{
	//衝突時の時間と衝突時の接点の位置を二次関数で算出する

	double colTime = this->GetGameObject()->GetUpdateTime();	//衝突時間書き込み用


	//球体の初期位置(更新前)
	D3DXVECTOR3 startPosA = circleA->center_ + circleA->pGameObject_->GetPosition();
	D3DXVECTOR3 startPosB = circleB->center_ + circleB->pGameObject_->GetPosition();

	//単位時間あたりの移動ベクトル(約1フレームあたりの移動ベクトル)
	D3DXVECTOR3 moveA = circleA->GetGameObject()->GetMoveVec() * circleA->GetGameObject()->GetUpdateTime();	
	D3DXVECTOR3 moveB = circleB->GetGameObject()->GetMoveVec() * circleA->GetGameObject()->GetUpdateTime();

	//移動後の球体の位置(更新後)
	D3DXVECTOR3 endPosA = startPosA + moveA;
	D3DXVECTOR3 endPosB = startPosB + moveB;

	//AとBの中心間を結んだベクトル(衝突軸)
	D3DXVECTOR3 startInterval = startPosB - startPosA;	//A,Bが初期位置だった場合の衝突軸
	D3DXVECTOR3 endInterval = endPosB - endPosA;	//A,Bが移動後の位置だった場合の衝突軸

	D3DXVECTOR3 difInterval = endInterval - startInterval;	//移動後、移動前の衝突軸の差分ベクトル
	float radiusAB = circleA->GetRadius() + circleB->GetRadius();	//半径の合計値
	float totalRadiusSq = circleA->GetRadius() + circleB->GetRadius();	//半径の合計値を2乗した値
	float difIntervalSq = D3DXVec3LengthSq(&difInterval);	//衝突軸の差をとったベクトルの長さの2乗

	//衝突判定に解の公式が使えるか
	if (difIntervalSq == 0)
	{
		// 球体A,Bが同じ速度で平行移動している場合

		// 生成された瞬間(移動前)から衝突している？
		if ( D3DXVec3LengthSq(&(startPosB - startPosA)) > totalRadiusSq )
		{
			return false;
		}

		//衝突時の中心位置更新
		//0 = 衝突していない, その他 = 更新の必要あり
		//生成された瞬間に衝突 = 円の中心は初期値
		colTime = 0;
		circleA->GetGameObject()->SetUpdateTime(colTime);

		if (colCenterA != 0)
		{
			circleA->GetGameObject()->SetColPos(startPosA);
		}

		if (colCenterB != 0)
		{
			circleB->GetGameObject()->SetColPos(startPosB);
		}

		if (startPosB == startPosA)
		{
			//中心点が同じで平行移動しているので、中心点を衝突点として返す
			colPos = colCenterA;
			return true;
		}
		
		//A->Bのベクトルの方向にcircleAの中心から半径分進んだ位置を衝突点とする
		colPos = startPosA + (circleA->GetRadius() / radiusAB) * startInterval;
		return true;	//同じ方向に移動
	}	

	//////////////////////////////
	// 同速で平行移動していない //
	// どっかで衝突する			//
	//////////////////////////////

	//生成された時点で衝突している？
	if ( D3DXVec3LengthSq( &(startPosB - startPosA) ) <= totalRadiusSq)
	{
		colTime = 0;
		circleA->GetGameObject()->SetUpdateTime(colTime);

		//衝突位置(接点)を求める
		colPos = startPosA + circleA->GetRadius() / radiusAB * startInterval;

		//生成時に衝突 = 円の中心は初期値
		if (colCenterA != 0)
		{
			circleA->GetGameObject()->SetColPos(startPosA);
		}

		if (colCenterB != 0)
		{
			circleB->GetGameObject()->SetColPos(startPosB);
		}

		return true;
	}

	// 1.初期位置以外で衝突
	// 2.同速で平行移動していない

	// startPosB から endPosA までの衝突軸(startInterval)方向へ動いた距離
	float dot = D3DXVec3Dot(&startInterval ,&difInterval);

	//A,Bが初期位置の時の衝突軸の長さの2乗
	float startIntervalSq = D3DXVec3LengthSq(&startInterval);


	//衝突判定式
	//judge = 解の公式に当てはめた時√の中が「-値」になっていないか(実数解が存在しているか)確認するための変数
	//※√に直して計算するのが大変なので、2乗で計算する
	float judge = dot * dot - difIntervalSq * (startIntervalSq - radiusAB * radiusAB);
	if (judge < 0)
	{
		//あたってない！(実数解がない)
		return false;
	}

	//////////////////
	//衝突時間の算出//
	//////////////////

	float judgeRt = sqrtf(judge);	//judgeの平方根(2乗してない本来の長さ)

	//解の公式から得られた±の値を+と-に分ける (衝突軸 == radiusABになった時の時間)
	float tPlus = ( -dot + judgeRt) / difIntervalSq;	//衝突後すり抜けて、衝突軸の長さ == radiusABになった時
	float tMinus = ( -dot - judgeRt) / difIntervalSq;	//最初に衝突した瞬間の、衝突軸の長さ == radiusABになった時

	//小さいほうの値をMinusになるように入れ替え
	if (tMinus > tPlus)
	{
		float work = tMinus;
		tMinus = tPlus;
		tPlus = work;
	}

	//時間外衝突かどうか
	//↑過去に当たってた or 今後当たる
	//※今は当たっていないので「false」
	if (tMinus < 0.0f || tMinus > 1.0f)
	{
		return false;
	}

	//衝突時間決定
	//衝突時間 = 衝突した時の時間 * 1度の処理で球体が進む速度(単位時間)
	colTime = tMinus * circleA->GetGameObject()->GetUpdateTime();	//なぜ移動時間に衝突時間をかけるのか？
	circleA->GetGameObject()->SetColTime(colTime);

	//衝突位置の決定
	//衝突時間までに球体の中心地がどの程度移動したか
	D3DXVECTOR3 colA = circleA->GetCenter() + circleA->GetGameObject()->GetPosition() + moveA * tMinus;	//球体の中心位置が衝突したときどの位置にあるか
	D3DXVECTOR3 colB = circleA->GetCenter() + circleB->GetGameObject()->GetPosition() + moveB * tMinus;
	
	colPos = colA + circleA->GetRadius() / radiusAB * (colB - colA);	//衝突時の球の接点


	//衝突時の中心位置更新
	if (colCenterA != 0)
	{
		circleA->GetGameObject()->SetColPos(colA);
	}
	if (colCenterB != 0)
	{
		circleB->GetGameObject()->SetColPos(colB);
	}

	return true;
}

//テスト表示用の枠を描画
//引数 position：位置
//引数 rotate：回転した角度
//引数 scale：拡大率
void Collider::Draw(D3DXVECTOR3 position, D3DXVECTOR3 rotate, D3DXVECTOR3 scale)
{
	D3DXMATRIX matT, matRX, matRY, matRZ, matS, matSRT;
	//コライダーをつけたオブジェクトから情報を受け取って行列生成
	D3DXMatrixTranslation(&matT, position.x + center_.x, position.y + center_.y, position.z + center_.z);
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate.z));
	D3DXMatrixScaling(&matS, scale.x, scale.y, scale.z);

	//行列合成
	matSRT = matS * matRZ * matRX * matRY * matT;

	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matSRT);
	pMesh_->DrawSubset(0);
}

//コライダーを回転拡大縮小
void Collider::ColliderTransform(BoxCollider* collider)
{
	//コライダーを回転、拡大縮小
	rotate_ = collider->pGameObject_->GetRotate();	//コライダーをつけたオブジェクトが回転した角度
	D3DXMATRIX rotateMatX, rotateMatY, rotateMatZ;	//回転行列

	//回転した角度->回転行列に戻す
	D3DXMatrixRotationX(&rotateMatX, rotate_.x);
	D3DXMatrixRotationY(&rotateMatY, rotate_.y);
	D3DXMatrixRotationZ(&rotateMatZ, rotate_.z);

	scale_ = collider->pGameObject_->GetScale();		//コライダーをつけたオブジェクトがどの程度拡大縮小したか
	D3DXMATRIX scaleMat;	//拡大縮小行列

	//拡大縮小後の数値を行列に戻す
	D3DXMatrixScaling(&scaleMat, scale_.x, scale_.y, scale_.z);

	//拡大縮小行列と、回転行列を合成
	colMat_ = scaleMat * rotateMatX * rotateMatY * rotateMatZ;

	//コライダーのx,y,z軸を回転
	D3DXVECTOR3 vec;
	D3DXVec3TransformCoord(&vec, &collider->GetAxisVec(0), &colMat_);
	collider->SetAxisVec(vec, 0);	//回転してセットしなおし(x)
	D3DXVec3TransformCoord(&vec, &collider->GetAxisVec(1), &colMat_);
	collider->SetAxisVec(vec, 1);	//回転してセットしなおし(y)
	D3DXVec3TransformCoord(&vec, &collider->GetAxisVec(2), &colMat_);
	collider->SetAxisVec(vec, 2);	//回転してセットしなおし(z)

}

//分離軸上に投影されたboxAとboxBの重心から面までの線分の長さを計算
//引数 spalationAxis：分離軸とする単位ベクトルこのベクトルを視点として2次元で当たり判定を行う
//引数 targetAxis1：x,y,z1本も分離軸として使用していない方のコライダーの軸(一本目)(長さを含む)
//引数 targetAxis2：x,y,z1本も分離軸として使用していない方のコライダーの軸(二本目)(長さを含む)
//引数 targetAxis3：x,y,z1本も分離軸として使用していない方のコライダーの軸(三本目)(長さを含む)
//戻り値：投影線分の長さの合計値(boxAとboxBの重心から面までの線分の長さの合計値)
float Collider::CalcProjectionLine(D3DXVECTOR3 * sepalationAxis, D3DXVECTOR3 * targetAxis1, D3DXVECTOR3 * targetAxis2, D3DXVECTOR3 * targetAxis3)
{
	//※計算に使用する二本のベクトルが垂直な場合の内積は「0」になる
	//※第4引数 targetAxis3 ：二本の方向ベクトルから外積をとり、分離軸とする時に
	//	外積の計算で使用した二本のベクトルと内積をとった場合、値が「0」になるので
	//	外積を分離軸として使用する場合のみ不必要となる

	//分離軸と相手のコライダーの投影線分
	float r1 = fabs(D3DXVec3Dot(sepalationAxis, targetAxis1));
	float r2 = fabs(D3DXVec3Dot(sepalationAxis, targetAxis2));
	float r3 = 0;

	if (targetAxis3 != nullptr)
	{
		r3 = fabs(D3DXVec3Dot(sepalationAxis, targetAxis3));
	}

	return r1 + r2 + r3;
}

//衝突した面の法線を算出する
//引数 targetCollider：衝突した「相手の」コライダー(onCollisionのtargetの方)
//引数 collisionAxis：衝突軸書き込み用の変数
//引数 colliderNormal：算出した法線ベクトルを書き込む用の変数
//引数 isCorner：角に当たったかどうか
//※角に当たった時のcollisionNormalは(0,0,0)
void Collider::CalcNormalVec(BoxCollider *targetCollider, D3DXVECTOR3 &collisionAxis, D3DXVECTOR3 &normal, bool &isCorner)
{
	collisionAxis = (pGameObject_->GetPosition() + center_) - targetCollider->GetCenter();	//衝突軸を求める
	D3DXVec3Normalize(&collisionAxis, &collisionAxis);	//衝突軸を正規化

	//どの面と当たったのか判定
	//当たった面の法線ベクトルを返す
	if (fabs(collisionAxis.x) > fabs(collisionAxis.y) && fabs(collisionAxis.x) > fabs(collisionAxis.z))
	{
		if (collisionAxis.x > 0)
		{
			//右面
			normal = targetCollider->GetAxisVec(0);
			D3DXVec3Normalize(&normal, &normal);
			isCorner = false;
		}
		else
		{
			//左面
			normal = -targetCollider->GetAxisVec(0);
			D3DXVec3Normalize(&normal, &normal);
			isCorner = false;
		}
	}

	else if (fabs(collisionAxis.y) > fabs(collisionAxis.x) && fabs(collisionAxis.y) > fabs(collisionAxis.z))
	{
		if (collisionAxis.y > 0)
		{
			//上面
			normal = targetCollider->GetAxisVec(1);
			D3DXVec3Normalize(&normal, &normal);
			isCorner = false;
		}
		else
		{
			//下面
			normal = -targetCollider->GetAxisVec(1);
			D3DXVec3Normalize(&normal, &normal);
			isCorner = false;
		}
	}

	else if (fabs(collisionAxis.z) > fabs(collisionAxis.x) && fabs(collisionAxis.z) > fabs(collisionAxis.y))
	{
		if (collisionAxis.z > 0)
		{
			//奥面
			normal = targetCollider->GetAxisVec(2);
			D3DXVec3Normalize(&normal, &normal);
			isCorner = false;
		}
		else
		{
			//手前面
			normal = -targetCollider->GetAxisVec(2);
			D3DXVec3Normalize(&normal, &normal);
			isCorner = false;
		}
	}

	//角に当たった時は法線を返さず、角に当たったフラグだけ送る
	else if (fabs(collisionAxis.x) == fabs(collisionAxis.y) &&
		fabs(collisionAxis.x) == fabs(collisionAxis.z) &&
		fabs(collisionAxis.y) == fabs(collisionAxis.z))
	{
		normal = D3DXVECTOR3(0, 0, 0);
		isCorner = true;
	}

}

//めり込んだコライダーの位置を元に戻す
//引数 targetNormal：衝突した面の法線
//引数 targetCol：衝突相手のコライダー
//引数 len：実際に戻す距離
//引数 moveVec： コライダーをつけたオブジェクトの移動時の速度ベクトル
void Collider::AdjustColliderPosition(D3DXVECTOR3 & targetNormal, BoxCollider * targetCol, float & len, D3DXVECTOR3 & moveVec)
{
	//現在の箱型コライダーの中心から各頂点までのベクトルを算出

	float currentAxis = 0;	//コライダーの現在の中心点から頂点までの距離
	float dist = 0;	//コライダーから衝突した面までの距離

	if (type_ == COLLIDER_BOX)
	{
		BoxCollider* boxCol = (BoxCollider*)this;
		for (int i = 0; i < BOX_AXIS_MAX; i++)
		{
			//箱型コライダーの中心から頂点までの距離を算出
			currentAxis += fabs(D3DXVec3Dot(&(boxCol->axisVec_[i] * boxCol->axisLen_[i]), &targetNormal));
		}

		//箱型コライダーの中心から衝突した壁の位置までの距離を出す
		
		//箱型コライダーの中心
		D3DXVECTOR3 boxPos = center_ + pGameObject_->GetPosition();

		//衝突した箱の接点(衝突相手コライダーの表面の座標)を算出
		//「衝突相手のコライダーの中心」から「法線ベクトル方向」に「衝突相手のコライダーの軸の長さ」分だけ引く
		D3DXVECTOR3 collisionPos = ((targetCol->GetCenter() + targetCol->GetGameObject()->GetPosition() ) +
										targetNormal * *(targetCol->GetSize() / 2));

		//箱型コライダーの中心から衝突した壁までの距離を算出
		dist = D3DXVec3Dot(&(boxPos - collisionPos), &targetNormal);

		//条件分岐で実際に戻す距離を出す
		if (dist > 0)
		{
			//distが「+」なら(コライダーの中心が埋まっていない)
			len = currentAxis - fabs(dist);
		}
		else
		{
			//distが「-」なら(コライダーの中心が埋まっている)
			len = currentAxis + fabs(dist);
		}
	}

	else if (type_ == COLLIDER_SPHERE)
	{
		SphereCollider* sphereCol = (SphereCollider*)this;

		currentAxis = sphereCol->radius_;

		//コライダーの中心から衝突した壁の位置までの距離を出す
		D3DXVECTOR3 spherePos = center_ + pGameObject_->GetPosition()/* + moveVec*/;

		//「衝突相手のコライダーの中心」から「法線ベクトル方向」に「衝突相手のコライダーの軸の長さ」分だけ引く
		D3DXVECTOR3 collisionPos = (targetCol->GetCenter() + targetNormal * *(targetCol->GetSize() / 2));

		//コライダーの原点から衝突した壁までの距離を算出
		dist = D3DXVec3Dot(&(spherePos - collisionPos), &targetNormal);

		//条件分岐で実際に戻す距離を出す
		if (dist > 0)
		{
			//distが「+」なら(コライダーの中心が埋まっていない)
			len = currentAxis - fabs(dist);
		}
		else
		{
			//distが「-」なら(コライダーの中心が埋まっている)
			len = currentAxis + fabs(dist);
		}
	}
}
