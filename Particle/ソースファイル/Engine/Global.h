#pragma once
#include <d3dx9.h>
#include <assert.h>
#include "Input.h"
#include "IGameObject.h"
#include "SceneManager.h"

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}			//動的確保した領域の安全な解放
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete [] p; p = nullptr;}	//配列解放用
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}		//クラス解放用
#define SAFE_DESTROY(p) {if ((p)!=nullptr) { p->Destroy(); (p)=nullptr;}}	//Audio解放用

//違うcppで実装しても値を保持するための構造体
struct Global
{
	//ウィンドウの背景サイズ
	int screenWidth;
	int screenHeight;
};

//どの軸かを表す
//boxLen, boxVecの添え字
enum
{
	BOX_AXIS_X,
	BOX_AXIS_Y,
	BOX_AXIS_Z,
	BOX_AXIS_MAX,
};


#define MAP_ROW 30
#define MAP_COL 30

//データの拡張子
enum pictType
{
	TYPE_PNG,	//.png
	TYPE_JPG,	//.jpg
	TYPE_FBX,	//.fbx
	TYPE_TXT,	//.txt
	TYPE_XWB,	//.xwb
	TYPE_XSB,	//.xsb
	TYPE_HLSL,	//.hlsl
	TYPE_MAX
};

//パスや拡張子
const std::string DATA_PATH = "Data/";
const std::string UI_PATH = "Data/UI/";
const std::string SOUND_PATH = "Data/Sound/";
const std::string MODELS_PATH = "Data/Models/";
const std::string DATA_TYPE[] = { ".png", ".jpg", ".fbx", ".txt", ".xwb", ".xsb", ".hlsl" };

const int	WINDOW_WIDTH = 800;		//ウィンドウの幅
const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ

extern Global g;
