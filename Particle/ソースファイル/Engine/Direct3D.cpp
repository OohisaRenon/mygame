#include "Direct3D.h"
#include "Input.h"

LPDIRECT3D9			Direct3D::pD3d = nullptr;	    //Direct3Dオブジェクト(ポインタ)
LPDIRECT3DDEVICE9	Direct3D::pDevice = nullptr;	//Direct3Dデバイスオブジェクト(ポインタ,画面に表示する時使う)
bool				Direct3D::isDrawCollision = true;			//コリジョンを表示するか
int					Direct3D::clearTime = 0;		//画面更新をする時間
D3DXCOLOR			Direct3D::backColor = D3DXCOLOR(0, 0, 0, 255);	//背景の色(r,g,b,a)

void Direct3D::Initilize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);	// newでオブジェクトとして生成したいけど純粋仮想関数があるためnewできない
												// この場合は生成する用の関数がある(Create)
	//エラーメッセージ表示
	assert(pD3d != nullptr);

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;	            //専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));	        //中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;					//次の更新処理で描画するスワップ用の画面を何枚用意するか
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;	//更新時の画面のスワップのやり方を設定(指定の列挙型で指定)
	d3dpp.Windowed = TRUE;						//TRUE:ウィンドウに表示,FALSE:フルスクリーン
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;
	d3dpp.BackBufferHeight = g.screenHeight;
	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice);

	//エラーメッセージ表示
	assert(pDevice != nullptr);

	//アルファブレンド(半透明表示の設定)
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング(TRUE:FALSE)
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	//ライトを設置
	D3DLIGHT9 lightState;
	ZeroMemory(&lightState, sizeof(lightState));	//第一引数:構造体の名前、第二引数:構造体のサイズ
	lightState.Type = D3DLIGHT_DIRECTIONAL;			//光源の種類(点とか平行とか)
	lightState.Direction = D3DXVECTOR3(1, -1, 1);	//光が降り注ぐ方向
	lightState.Diffuse.r = 1.0f;					//拡散反射光(光の色)
	lightState.Diffuse.g = 1.0f;
	lightState.Diffuse.b = 1.0f;
	pDevice->SetLight(0, &lightState);				//第一引数:ライトの個数
	pDevice->LightEnable(0, TRUE);					//第一引数:ライトの番号

	//カメラ
	D3DXMATRIX view;
	//D3DXMatrixLookAtLH : ビュー行列を作るための関数
	//第一引数 : 書き込む行列　第二引数 : カメラ(視点)の位置
	//第三引数 : 焦点、注視点(どこを見るか)　第四引数 : カメラそのもの向き(どっちが上向きか)※デフォルトの縦撮りは(0,1,0)
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
	pDevice->SetTransform(D3DTS_VIEW, &view);		//SetTransform 第一引数 : どの変形行列か指定 第二引数 : どの行列を割り当てるか

	D3DXMATRIX proj;
	//D3DXMatrixPerspectiveFovLH : プロジェクション行列
	//第一引数 : 書き込む行列　第二引数 : 画角(カメラの見える範囲,ズーム度)※数値いじると遠近感が変わる(低いと遠近感なくなり、平らに見える)
	//第三引数 : アスペクト比(ウィンドウの比率)　第四引数 : カメラの描画できる手前座標の上限(ニアクリッピング面までの距離)
	//第五引数 : カメラの描画できる奥の座標の上限(ファークリッピング面までの距離)  ニアクリッピング面とファークリッピング面は近いほうがいい
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 1.0f, 100.0f);
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);

}

void Direct3D::BeginDraw()
{
	static float time;	//1フレームごとにカウントアップ
	time++;

	//画面クリアのタイミングを調整
	if (time >= clearTime)
	{
		//画面をクリア(毎フレーム)
		//D3DCOLOR_XRGB : RGBで色を変更
		pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
						D3DCOLOR_XRGB((unsigned int)backColor.r, (unsigned int)backColor.g, (unsigned int)backColor.b), 1.0f, 0);
		time = 0;
	}

	//描画開始
	//BeginScene : 描画処理開始時に必ず呼ぶ
	pDevice->BeginScene();

}

void Direct3D::EndDraw()
{
	//描画終了 : 描画処理終了時に必ず呼ぶ
	pDevice->EndScene();

	//スワップ
	pDevice->Present(NULL, NULL, NULL, NULL);
}

void Direct3D::Release()
{
	//開放処理
	//生成したのと逆順に開放
	SAFE_RELEASE(pDevice);
	SAFE_RELEASE(pD3d);
}

//画面のクリア時間のセッター
//引数 : clear セットしたい値
void Direct3D::SetClearTime(int clear)
{
	clearTime = clear;
}

//背景の色のセッター
//引数 : color セットしたい値
void Direct3D::SetBackColor(D3DXCOLOR color)
{
	backColor = color;
}