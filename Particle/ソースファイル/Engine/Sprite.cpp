#include "Sprite.h"
#include "Direct3D.h"

//生成した瞬間に初期化される
//中に書くと生成と代入が別処理なので遅い
Sprite::Sprite() : pSprite_(nullptr), pTexture_(nullptr), texData_(D3DXIMAGE_INFO())
{
}


Sprite::~Sprite()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pSprite_);
}

//スプライトとテクスチャの作成
//引数 : pPicture 読み込むファイルのデータ(winmainの中でファイル名指定)
void Sprite::Load(const char* pPicture)
{
	//スプライト作成
	D3DXCreateSprite(Direct3D::pDevice, &pSprite_);
	assert(pSprite_ != nullptr);

	//テクスチャ作成
	//画像サイズは2の乗数にする
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, pPicture,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, &texData_, 0, &pTexture_);

	assert(pTexture_ != nullptr);

}

//2D画像の描画
//引数 : matrix 配置する場所を変更するための移動、回転、拡大縮小後の行列
//引数 : color 色を変えたいときは指定する  指定しない時はデフォルト(1,1,1,1)で画像データの色のまま表示
void Sprite::Draw(const D3DXMATRIX& matrix, D3DXCOLOR color)
{

	//行列をセット
	pSprite_->SetTransform(&matrix);

	//ゲーム画面の描画
	pSprite_->Begin(D3DXSPRITE_ALPHABLEND);
	pSprite_->Draw(pTexture_, nullptr, nullptr, nullptr, color);
	pSprite_->End();

}

//画像データの詳細情報のゲッター
//引数 : data 書き込み用変数
void Sprite::GetTexData(D3DXIMAGE_INFO &data)
{
	data = texData_;
}
