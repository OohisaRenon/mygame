#include "Model.h"
#include <vector>

namespace Model
{
	std::vector<ModelData*> dataList;

	//モデルデータをロード
	int Load(std::string fileName, LPD3DXEFFECT pEffect)
	{
		//モデルデータは複数読み込んでvectorに追加していく
		ModelData* pData = new ModelData;

		pData->fileName = fileName;		//ファイル名に読み込んだファイル名を入れる
		bool isExist = false;
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i]->fileName == fileName)
			{
				//読み込まれた番地を伝える
				pData->pFbx = dataList[i]->pFbx;	//ファイル名 = pFbx
				isExist = true;
				break;
			}
		}
		
		//まだ読み込まれていないモデルなら
		if (isExist == false)
		{
			//モデルデータ生成
			pData->pFbx = new Fbx(pEffect);
			pData->pFbx->Load(fileName.c_str());	//.c_str():普通のchar型に変換してくれる<string>の機能
		}


		//ロード関数が呼ばれるたびにリストに追加される(要素数=モデルデータの種類)
		dataList.push_back(pData);

		//モデルデータの番号を返す
		return dataList.size() -1;
	}

	//描画
	//引数 : handle モデル番号
	void Draw(int handle)
	{
		//モデルデータ番目のマトリックスを描画する
		dataList[handle]->pFbx->Draw(dataList[handle]->matrix_);
	}

	//localMatrixに移動、回転、拡大縮小後の行列をセットする
	//引数 : handle ハンドル(モデル番号等)
	//引数 : matrix 表示する座標 (worldMatrix)
	void SetMatrix(int handle, D3DXMATRIX &matrix)
	{
		dataList[handle]->matrix_ = matrix;
	}

	//解放
	//引数 : handle モデル番号
	void Release(int handle)
	{

		bool isExist = false;
		for(unsigned int i = 0; i < dataList.size(); i++)
		{
			//同じモデルデータ(FBX)が使われているかどうか探す
			//※今消そうとしているモデルは無視(自分と自分は比較しない)
			//今見ているdataListの要素の中にあるモデルデータとdataListのモデル番号の要素の中にあるモデルデータが同じの時、同じモデルデータを使っていることになる
			if (i != handle && dataList[i] != nullptr && dataList[i]->pFbx == dataList[handle]->pFbx)
			{
				//今消そうとしているモデルと同じFBXがあったら消さずにbreak
				isExist = true;
				break;
			}

		}

		//他に同じモデルを使っているやつがいなければ解放する
		if (isExist == false)
		{
			//モデルデータ解放(Fbxを解放)
			SAFE_DELETE(dataList[handle]->pFbx);
		}

		//最後にdatListそのものを消す(必ず消す)
		SAFE_DELETE(dataList[handle]);
	}

	//すべてのモデルを消す
	void AllRelease()
	{

		//ゲーム終了時にすべてのモデルを解放する
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//Release()でdataListは消されているのに、そこにアクセスしようとするので
			//dataListが消えてる場合はそこにアクセスしない
			if (dataList[i] != nullptr)
			{
				//まだ削除してないdataListがあればRelease
				Release(i);
			}
		}

		//ベクターを使ったら最後に中身を空にする
		dataList.clear();

	}

	//レイを飛ばす処理
	//引数 : handle ハンドル(モデル番号等)
	//引数 : data レイキャスト用データ始点とか方向とか...
	void RayCast(int handle, RayCastData &data)
	{
		////※プレイヤーから地面にレイを飛ばすこととしてコメントを記述
		////data: プレイヤーから発射したレイ
		////handle:レイを飛ばす相手のハンドル(今回は地面)

		////判定する相手(今回の場合は地面)
		//D3DXVECTOR3 target = data.origin + data.direction;
		//D3DXMATRIX matInv;

		////プレイヤーモデルのローカル行列(Maya側で指定したやつ)を
		////逆行列生成して保存
		//D3DXMatrixInverse(&matInv, 0, &dataList[handle]->matrix_);

		////↑で作った行列を使って位置とかベクトルとかを変形
		////プレイヤーから発射したレイの発射位置を変形
		//D3DXVec3TransformCoord(&data.origin, &data.origin, &matInv);

		////判定する相手も変形
		//D3DXVec3TransformCoord(&target, &target, &matInv);

		////レイを飛ばす方向を変形した地面の位置から
		////変形したレイの発射位置までのベクトルを引いて
		////方向ベクトルとする
		//data.direction = target - data.origin;

		dataList[handle]->pFbx->RayCast(data, dataList[handle]->matrix_);
	}

	//pEffectのセッター
	//FBXクラスで管理しているpEffectに各ゲームオブジェクトクラスで生成したpEffectを渡す
	//引数 : effect ゲームオブジェクトクラスから受け取るシェーダーアクセス用ポインタ
	//引数 : handle モデル番号
	void SetEffect(LPD3DXEFFECT effect, int handle)
	{
		dataList[handle]->pFbx->SetEffect(effect);
	}

};