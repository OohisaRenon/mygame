#pragma once
#include <string>
#include "Fbx.h"

namespace Model
{
	struct ModelData
	{
		std::string fileName;
		Fbx* pFbx;
		D3DXMATRIX matrix_;

		//コンストラクタ
		ModelData() : fileName(""), pFbx(nullptr)
		{
			//行列を初期化(単位行列生成)
			D3DXMatrixIdentity(&matrix_);
		}
	};

	//読み込むときはファイル名を読み込む
	int Load(std::string fileName, LPD3DXEFFECT pEffect = nullptr);
	
	//描画
	//引数 : handle モデル番号
	void Draw(int handle);

	//移動、回転、拡大率をいじった後どう動くか確定させる
	//引数 : handle ハンドル(モデル番号等)
	//引数 : matrix 表示する座標 (worldMatrix)
	void SetMatrix(int handle, D3DXMATRIX &matrix);

	//解放
	//引数 : handle モデル番号
	void Release(int handle);

	//すべてのモデルを消す
	void AllRelease();

	//レイを飛ばす処理
	//引数 : handle ハンドル(モデル番号等)
	//引数 : data レイキャスト用データ始点とか方向とか...
	void RayCast(int handle, RayCastData &data);

	//pEffectのセッター
	//FBXクラスで管理しているpEffectに各ゲームオブジェクトクラスで生成したpEffectを渡す
	//引数 : effect ゲームオブジェクトクラスから受け取るシェーダーアクセス用ポインタ
	//引数 : handle モデル番号
	void SetEffect(LPD3DXEFFECT effect, int handle);
};

