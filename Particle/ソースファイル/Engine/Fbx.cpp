#include "Fbx.h"
#include "Direct3D.h"
#include <vector>


Fbx::Fbx(LPD3DXEFFECT pEffect) : pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr), pTexture_(nullptr),
pMaterial_(nullptr), pManager_(nullptr), pImporter_(nullptr), pScene_(nullptr),
polygonCountOfMaterial_(nullptr),
vertexCount_(0), polygonCount_(0), indexCount_(0), materialCount_(0),
pEffect_(pEffect)
{
}


Fbx::~Fbx()
{
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);
	SAFE_DELETE_ARRAY(pMaterial_);

	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);
		SAFE_RELEASE(pTexture_[i]);
	}
	SAFE_DELETE_ARRAY(ppIndexBuffer_);
	SAFE_DELETE_ARRAY(pTexture_);

	SAFE_RELEASE(pVertexBuffer_);
	pScene_->Destroy();
	pManager_->Destroy();

}

//モデルデータをFBXファイルからロード
//引数 : fileName 読み込みたいファイルの名前(パス)
void Fbx::Load(const char * fileName)
{
	//オブジェクト生成
	pManager_ = FbxManager::Create();
	pImporter_ = FbxImporter::Create(pManager_, "");
	pScene_ = FbxScene::Create(pManager_, "");

	pImporter_->Initialize(fileName);	//読み込みたいファイルを指定
	pImporter_->Import(pScene_);		//ファイル読み込み
	pImporter_->Destroy();			//読み込むためだけのものなので読んだら解放

	char defaultCurrentDir[MAX_PATH];		//ディレクトリ名が入る配列
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);		//現在のカレントディレクトリを記憶

	char dir[MAX_PATH];
	//第三引数:フォルダ名を指定
	_splitpath_s(fileName, nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	SetCurrentDirectory(dir);		//これ以降は引数のフォルダを見るように指定(カレントディレクトリを設定)

	//※ノード:一つ一つのデータのこと
	FbxNode* rootNode = pScene_->GetRootNode();		//Fbxファイルのルートノード取得(一番上のノード)
	int childCount = rootNode->GetChildCount();		//ルートの下に子供のノードがいくつあるか

	//子供のノードの数だけループ
	for (int i = 0; childCount > i; i++)
	{
		//ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));
	}

	SetCurrentDirectory(defaultCurrentDir);		//カレントディレクトリを初期位置に戻す
}

void Fbx::CheckNode(FbxNode * pNode)
{
	//子供のノードはメッシュのノードか？
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		materialCount_ = pNode->GetMaterialCount();		//マテリアルの個数(使った色の種類)
		pMaterial_ = new D3DMATERIAL9[materialCount_];	//配列を生成(配列の大きさはマテリアルの数)
		pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];//テクスチャの配列

		//配列の中身を一気に初期化(0を入れる)
		ZeroMemory(pTexture_, sizeof(LPDIRECT3DTEXTURE9) * materialCount_);

		for (int i = 0; i < materialCount_; i++)
		{
			FbxSurfacePhong* surface = (FbxSurfacePhong*)pNode->GetMaterial(i);		//ランバートが入る
			FbxDouble3 diffuse = surface->Diffuse;		//ランバートの色設定
			FbxDouble3 ambient = surface->Ambient;		//ランバート環境光(明るさ)設定

			ZeroMemory(&pMaterial_[i], sizeof(D3DMATERIAL9));

			pMaterial_[i].Diffuse.r = (float)diffuse[0];
			pMaterial_[i].Diffuse.g = (float)diffuse[1];
			pMaterial_[i].Diffuse.b = (float)diffuse[2];
			pMaterial_[i].Diffuse.a = 1.0f;	//不透明度

			pMaterial_[i].Ambient.r = (float)ambient[0];
			pMaterial_[i].Ambient.g = (float)ambient[1];
			pMaterial_[i].Ambient.b = (float)ambient[2];
			pMaterial_[i].Ambient.a = 1.0f;

			//※GetClassId()：生成したオブジェクトのクラス(何型)をゲットしてくる
			//※.Is(〇〇)：それが〇〇ならtrue
			if (surface->GetClassId().Is(FbxSurfacePhong::ClassId))
			{
				//マテリアルがPhongだったら
				//鏡面反射光
				FbxDouble3 specular = surface->Specular;
				pMaterial_[i].Specular.r = (float)specular[0];
				pMaterial_[i].Specular.g = (float)specular[1];
				pMaterial_[i].Specular.b = (float)specular[2];
				pMaterial_[i].Specular.a = 1.0f;

				//鏡面反射の光の強さ
				pMaterial_[i].Power = (float)surface->Shininess;
			}


			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			if (textureFile == nullptr)
			{
				//テクスチャなしならヌルポ
				pTexture_[i] = nullptr;
			}
			else
			{
				//テクスチャの画像ファイルの名前
				const char* textureFileName = textureFile->GetFileName();

				char name[_MAX_FNAME];
				char ext[_MAX_EXT];
				_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);

				//wsprintf:文字や数字を連結できる(int型を文字列に書き換えたりも可能)
				//第一引数:格納先
				//第一引数:書式(今回はファイル名+拡張子)
				wsprintf(name, "%s%s", name, ext);

				//テクスチャ作成
				//画像サイズは2の乗数にする
				D3DXCreateTextureFromFileEx(Direct3D::pDevice, name,
					0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
					D3DX_DEFAULT, 0, 0, 0, &pTexture_[i]);

				assert(pTexture_[i] != nullptr);
				//ポインタの中身が0xcdcdcdcdになってるときはVisualStasio側で仮データを入れてくれてる
			}
		}

		CheckMesh(pNode->GetMesh());
	}

	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			//さらに子供を調べてメッシュかどうか判定(再起)
			CheckNode(pNode->GetChild(i));
		}
	}
}

void Fbx::CheckMesh(FbxMesh * pMesh)
{
	//頂点
	//FbxVector4:二次元配列として使える型(頂点座標を格納)
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	vertexCount_ = pMesh->GetControlPointsCount();		//頂点数をカウント
	Vertex* vertexList = new Vertex[vertexCount_];		//頂点情報を入れる配列を作る(pos, normal, uv)

	polygonCount_ = pMesh->GetPolygonCount();			//ポリゴンの枚数を入れる
	indexCount_ = pMesh->GetPolygonVertexCount();		//全ポリゴンのインデックスの数

	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}

	//法線情報
	for (int i = 0; i < polygonCount_; i++)
	{
		//ポリゴンごとの法線を調べるためにポリゴンの数だけループ
		int startIndex = pMesh->GetPolygonVertexIndex(i);

		for (int j = 0; j < 3; j++)
		{
			//今調べているポリゴンの最初の頂点番号
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			FbxVector4 Normal;		//法線情報を入れるとこ
			pMesh->GetPolygonVertexNormal(i, j, Normal);		//法線情報読み込み
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);		//法線情報書き込み

			//UVを設定
			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);	//index番目の頂点のUV情報をuvという変数に入れる
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	//Quadと同じ(頂点情報)
	Direct3D::pDevice->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);
	pVertexBuffer_->Unlock();

	assert(pVertexBuffer_ != nullptr);
	SAFE_DELETE_ARRAY(vertexList);		//newで作ったので解放

	//インデックス情報
	ppIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];	//全体のインデックスの数
	polygonCountOfMaterial_ = new int[materialCount_];		//マテリアル(色の種類)ごとのインデックス配列

	//インデックス情報を取得し、マテリアルの数(色の種類)だけインデックスバッファを作る(※赤のインデックス配列、青のインデックス配列...)
	for (int i = 0; i < materialCount_; i++)
	{
		int* indexList = new int[indexCount_];
		int count = 0;

		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			//今見ているポリゴンが何番目のマテリアルか確認(赤色か、青色か...)
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);

			if (materialID == i)
			{
				//今見ているポリゴンの色と、配列の色が同じ時その色のインデックスリストに三頂点分追加(赤色のマテリアルなら赤色のリストに三頂点分追加)
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		//インデックス / 3 でマテリアル(色の種類)ごとのポリゴンの枚数を格納(※赤色のポリゴンが何枚か、青色のポリゴンが何枚...)
		polygonCountOfMaterial_[i] = count / 3;

		Direct3D::pDevice->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &ppIndexBuffer_[i], 0);
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);
		ppIndexBuffer_[i]->Unlock();

		assert(ppIndexBuffer_);
		SAFE_DELETE_ARRAY(indexList);	//newで作ったので解放処理

	}
}

//描画
//引数 : matrix ワールド行列
void Fbx::Draw(const D3DXMATRIX &matrix)
{
	//変形した行列をセットしてから描画の処理に移る
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);

	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));

	//ポリゴン座標部分とUV座標の部分でビット演算
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

	for (int i = 0; i < materialCount_; i++)
	{
		//テクスチャをセット
		Direct3D::pDevice->SetTexture(0, pTexture_[i]);

		//表示したい頂点バッファとインデックスバッファを指定
		Direct3D::pDevice->SetIndices(ppIndexBuffer_[i]);

		//マテリアルをセット
		Direct3D::pDevice->SetMaterial(&pMaterial_[i]);

		//第一引数:並べるポリゴンの形(基本三角形)
		//第四引数:表示したい頂点数
		//第六引数:何枚のポリゴンを出すか
		Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCountOfMaterial_[i]);


		//pEffect_にシェーダーが渡って来ていたらシェーダーをセット
 		if (pEffect_)
		{

			//※第2引数：物体の色を渡す(Mayaで設定した色を渡す)
			//Maya側で設定した色が_pMaterial[i]に入っているので、読み込んだr,g,b,aを渡す
			pEffect_->SetVector("DIFFUSE_COLOR",
				&D3DXVECTOR4(pMaterial_[i].Diffuse.r,
					pMaterial_[i].Diffuse.g,
					pMaterial_[i].Diffuse.b,
					pMaterial_[i].Diffuse.a));

			//環境光設定
 			pEffect_->SetVector("AMBIENT_COLOR",
				&D3DXVECTOR4(pMaterial_[i].Ambient.r,
					pMaterial_[i].Ambient.g,
					pMaterial_[i].Ambient.b,
					pMaterial_[i].Ambient.a));

			//鏡面反射光設定
			pEffect_->SetVector("SPECULER_COLOR",
				&D3DXVECTOR4(pMaterial_[i].Specular.r,
					pMaterial_[i].Specular.g,
					pMaterial_[i].Specular.b,
					pMaterial_[i].Specular.a));

			//テクスチャ貼ってるかどうか
			pEffect_->SetBool("IS_SET_TEXTURE", pTexture_[i] != nullptr);

			//ハイライトの大きさを渡す
			pEffect_->SetFloat("SPECULER_POWER", pMaterial_[i].Power);
		}
	}
}

//レイキャスト
//引数 : data レイキャストに必要なデータ
//引数 : matrix ワールド座標
void Fbx::RayCast(RayCastData &data, const D3DXMATRIX &matrix)
{
	//レイを飛ばした位置から一番近い面までの距離を記憶
	float minDistance = 9999.0f;

	//頂点バッファをロック
	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);

	//マテリアル毎
	for (DWORD i = 0; i < (unsigned int)materialCount_; i++)
	{
		//インデックスバッファをロック
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);

		//レイの発射位置から一番近い面までの距離を入れる

		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < (unsigned int)polygonCountOfMaterial_[i]; j++)
		{
			//3頂点
			//ローカル座標(Mayaで作ったまま)の値を入れている
			D3DXVECTOR3 v0, v1, v2;
			v0 = vCopy[iCopy[j * 3 + 0]].pos;

			//ワールド行列(行列)とローカル座標(ベクトル)を合成
			D3DXVec3TransformCoord(&v0, &v0, &matrix);

			//v1も同様に行う
			v1 = vCopy[iCopy[j * 3 + 1]].pos;
			D3DXVec3TransformCoord(&v1, &v1, &matrix);

			//v2も同様に行う
			v2 = vCopy[iCopy[j * 3 + 2]].pos;
			D3DXVec3TransformCoord(&v2, &v2, &matrix);

			// ※ここで、D3DXIntersectTri関数を使う

			//D3DXIntersectTri:モデルとレイの当たり判定戻り値はbool型	
			//第1〜第3:頂点1,頂点2,頂点3  第4:レイの発射位置,
			//第5:レイの発射方向  第6:頂点からレイの当たった点までの距離(水平方向) 第7:頂点からレイの当たった点までの距離(垂直方向)
			//第8:当たった点までの距離
			//※すべて参照渡し
			bool hit = D3DXIntersectTri(&v0, &v1, &v2, &data.origin, &data.direction, nullptr, nullptr, &data.distance);
			if (hit)
			{
				data.hit = true;
				//distanceが一番小さい時(レイを飛ばした位置から一番近い面までの距離)の計算結果が欲しい
				if (data.distance < minDistance)
				{
					minDistance = data.distance;
				}
			}

		}
		//インデックスバッファ使用終了
		ppIndexBuffer_[i]->Unlock();
	}
	data.distance = minDistance;
}

//シェーダーアクセス用ポインタのセッター
//引数 : effect シェーダーアクセス用ポインタ
void Fbx::SetEffect(LPD3DXEFFECT effect)
{
	pEffect_ = effect;
}
