#include <Windows.h>
#include <stdlib.h>
#include <commctrl.h>
#include <tchar.h>
#include "Global.h"
#include "Direct3D.h"
#include "RootJob.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"
#include "../resource.h"
#include "../Particle.h"
#include "../ConstParticleData.h"

//////////////////////////////////////////////////
//  LP~...という名前の型はポインタを指す        //
//  I~...という名前の型はインタフェースを指す   //
//////////////////////////////////////////////////

//リンカ	※これの場所はどこでもいい(1箇所)
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib, "winmm.lib")

//メモリリーク検出用ヘッダー
#ifdef _DEBUG			//デバックモードの時のみ実行
#include <crtdbg.h>
#endif

//ウィンドウ関係の定数
const char* WIN_CLASS_NAME = "Particle";


//時間計測関係の定数
const float FRAME = 60.0f;		//フレームレート
const float SECOND = 1000.0f;	//ミリ秒単で計測した時の1秒の数値

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);
void InitSpinControl(HWND hDlg, int id, WPARAM wp, LPARAM lp, int val);

//グローバル変数

Global g;
RootJob* pRootJob;

//エントリーポイント(main関数みたいなもの)
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef _DEBUG
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//乱数生成用
	srand((unsigned int)time(NULL));

	//ダイアログプロシージャ
	HWND hDlg = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG_PARTICLE_DATA), nullptr, (DLGPROC)DialogProc);

	//LPSTR ロングポインタSTR : char形のポインタと同じ
	//hInstance インスタンスハンドル : オブジェクトの識別番号
	//hPrevInst 昔使ってた。今はいらないけど、消すと前のプログラムが動かなくなるので消さない
	//lpCmdLine : プログラムを起動したときに
	//nCmdShow : 全画面表示orウィンドで表示()

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	//ウィンドウクラスの中身を初期化
	wc.cbSize = sizeof(WNDCLASSEX);	            //この構造体(クラス)のサイズ
	wc.hInstance = hInstance;	                //インスタンスハンドル(オブジェクトの番号)
	wc.lpszClassName = WIN_CLASS_NAME;	        //ウィンドウクラス名(今から作ろうとしてるウィンドウの名前)
	wc.lpfnWndProc = WndProc;	                //ウィンドウプロシージャ(何か操作が行われたときに呼ばれる関数をセット)
	wc.style = CS_VREDRAW | CS_HREDRAW;	        //スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);	//アイコン(自作可能)
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);	  //小さいアイコン(自作可能)
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);	  //マウスカーソル(自作も可能だが、windousプログラムでは白黒しか作れない)
	wc.lpszMenuName = NULL;	                    //メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）※ゲーム画面出すから変更の必要なし
	//RegisterClassEx : クラスを割り当てる
	RegisterClassEx(&wc);	//クラスを登録
	
	//ウィンドウの白い部分のサイズを指定
	RECT winRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };

	//ウィンドウの白い描画するところをちょうどよく合わせる
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);

	//ウィンドウを作成

	HWND hWnd = CreateWindow(	//HWND : ウィンドウハンドル
		//hWnd作られたウィンドウの番号を入れる
		WIN_CLASS_NAME,	       //ウィンドウクラス名(上で初期化した内容と同じじゃなきゃだめ)
		WIN_CLASS_NAME,    	//タイトルバーに表示する内容(ゲームの名前)
		WS_OVERLAPPEDWINDOW,	//スタイル（普通のウィンドウ）※最大化最小化ボタン、閉じるときの×ボタン等の有無
		CW_USEDEFAULT,	      //表示位置左（おまかせ）※0 = 左端
		CW_USEDEFAULT,	      //表示位置上（おまかせ）※0 = 上
		winRect.right - winRect.left,	      //ウィンドウ幅
		winRect.bottom - winRect.top,        //ウィンドウ高さ
		NULL,	               //親ウインドウ（なし）※親ウィンドウを消すとこのウィンドウも消える
		NULL,	               //メニュー（なし）
		hInstance,	          //インスタンス
		NULL	                //パラメータ（なし）
	);

	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);	//第一引数でどのウィンドウを表示するか指定

	assert(hWnd != NULL);

	//ウィンドウの描画するところのサイズを設定
	//externを使っているので、どこのcppからでもアクセス可能
	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT; 

	//呼び出し,初期化Direct3D
	Direct3D::Initilize(hWnd);

	//呼び出し,初期化Input
	Input::Initialize(hWnd);

	//呼び出し,初期化Audio
	Audio::Initialize();

	pRootJob = new RootJob;
	pRootJob->Initialize();

	//ゲームループに入る前に一回だけ描画しとく
	Direct3D::pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	//指定した構造体やクラスを0に初期化する
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))	//windousからメッセージがきたら...
		{
			//OSからメッセージあればそっちを優先
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//ゲームの処理

			// エンドピリオドまでちょっと細かく時間計測
			timeBeginPeriod(1);
			int nowTime = timeGetTime();		//計測開始
			static int lastDispTime = nowTime;	//今のフレーム時間
			static int lastUpdateTime = nowTime;//前フレームの時間
			static int fps = 0;

			//経過時間が1秒を超えたらFPSカウントと時間をリセット
			if (nowTime - lastDispTime > SECOND)
			{
				//FPS表示(タイトルバーに)
				char str[16];
				wsprintf(str, "FPS=%d", fps);
				SetWindowText(hWnd, str);
				fps = 0;
				lastDispTime = nowTime;
			}

			//1秒を60フレームに固定
			if ((nowTime - lastUpdateTime) * FRAME >= SECOND)
			{
				fps++;
				lastUpdateTime = nowTime;	//前フレームの時間に更新

				Input::Update();	//入力のアップデート関数

				pRootJob->UpdateSub();

				Direct3D::BeginDraw();		//描画開始

				//DirectXの描画回りの確認で一時的に切っとく
				//コライダーの描画するとこで設定切り替わってしまってるみたい
				pRootJob->DrawSub();

				Direct3D::EndDraw();		//描画終了

				timeEndPeriod(1);	//細かく時間計測するのをやめる
			}
		}
	}

	/////////////////
	//  解放処理  //
	////////////////

	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Audio::Release();
	Input::Release();
	Direct3D::Release();
	Model::AllRelease();
	Image::AllRelease();

	return 0;
}

//ウィンドウプロシージャ（何かあった時によばれる関数）-> 
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//LRESULT ローリザルト :  
	//CALLBACK : 何かあったときに呼ばれる関数
	//msg:メッセージ。何が起きたのか
	//wParam, lParam : 追加情報msgの内容によって変わる

	switch (msg)
	{
	case WM_DESTROY:		//ウィンドウが閉じられたとき
		PostQuitMessage(0);	//プログラム終了　※ウィンドウ閉じる = プログラム終了にはならない
		return 0;//自分で処理を追加したら...return0:
	}
	//return DefWindowProc : デフォルト処理のままでいい場合...returnで返す
	return DefWindowProc(hWnd, msg, wParam, lParam);
}


//ダイアログプロシージャ（何かあった時によばれる関数)
BOOL CALLBACK DialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	//Editの初期値
	//初期値にしか使わないので使いまわしok
	int initVal = 0;

	//何かしてたらtrueを返す
	switch (msg)
	{
	//ダイアログが生成されたら
	case WM_INITDIALOG:
		//ラジオボタンにデフォルトでチェックを入れる
		//グループボックスでくくったラジオボタンの中からデフォルトで一つチェック入れとく
		//※これでグループ化される
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_ALPHA_ADD), BM_SETCHECK, BST_CHECKED, (LPARAM)0);	//アルファブレンド 加算 or 乗算
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_ZBUFFER_TRUE), BM_SETCHECK, BST_CHECKED, (LPARAM)0);	//Zバッファの書き込み on or off
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_COLOR_MANUAL), BM_SETCHECK, BST_CHECKED, (LPARAM)0);	//色を入力して決める or ランダムで決める
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_ROTATE_FALSE), BM_SETCHECK, BST_CHECKED, (LPARAM)0);	//回転させるか or させない
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_MOVE_DIFFUSION), BM_SETCHECK, BST_CHECKED, (LPARAM)0);//動かし方(進行方向ベクトルの制御) 拡散 or 収縮

		
		//Spinの上限と下限の値を設定
		//UDM_SETRANGE32 : int型を扱える
		//UDM_SETRANGE : shot型までしか扱えない
		InitSpinControl(hDlg, IDC_SPIN_CLEAR_TIME, 0, CLEAR_TIME_MAX, initVal);	//画面のクリア時間

		initVal = 1;
		InitSpinControl(hDlg, IDC_SPIN_SPEED, 0, SPEED_MAX, initVal);			//速度
		InitSpinControl(hDlg, IDC_SPIN_RESISTANCE, 1, RESISTANCE_MAX, initVal);	//抵抗
		initVal = 3;
		InitSpinControl(hDlg, IDC_SPIN_SIZE, 0, PARTICLE_SIZE_MAX, initVal);	//サイズ

		initVal = 0;	//初期値更新
		InitSpinControl(hDlg, IDC_SPIN_COUNT, 0, PARTICLE_COUNT, initVal);	//個数

		initVal = 1;
		InitSpinControl(hDlg, IDC_SPIN_COUNT_PER_FRAME, 0, PARTICLE_COUNT, initVal);	//描画する数を1フレーム当たり何個ずつ増やすか


		//パーティクルの色
		//入力値(ランダムの最大値)
		initVal = 255;
		InitSpinControl(hDlg, IDC_SPIN_COLOR_ALPHA, 0, COLOR_MAX, initVal);	//透明度
		InitSpinControl(hDlg, IDC_SPIN_COLOR_R, 0, COLOR_MAX, initVal);	//赤
		InitSpinControl(hDlg, IDC_SPIN_COLOR_G, 0, COLOR_MAX, initVal);	//緑
		InitSpinControl(hDlg, IDC_SPIN_COLOR_B, 0, COLOR_MAX, initVal);	//青
		//ランダムの最小値
		initVal = 255;
		InitSpinControl(hDlg, IDC_SPIN_COLOR_RANDOM_ALPHA, 0, COLOR_MAX, initVal);	//透明度
		InitSpinControl(hDlg, IDC_SPIN_COLOR_RANDOM_R, 0, COLOR_MAX, initVal);	//赤
		InitSpinControl(hDlg, IDC_SPIN_COLOR_RANDOM_G, 0, COLOR_MAX, initVal);	//緑
		InitSpinControl(hDlg, IDC_SPIN_COLOR_RANDOM_B, 0, COLOR_MAX, initVal);	//青

		//背景の色
		initVal = 0;
		InitSpinControl(hDlg, IDC_SPIN_BACK_COLOR_R, 0, COLOR_MAX, initVal);	//赤
		InitSpinControl(hDlg, IDC_SPIN_BACK_COLOR_G, 0, COLOR_MAX, initVal);	//緑
		InitSpinControl(hDlg, IDC_SPIN_BACK_COLOR_B, 0, COLOR_MAX, initVal);	//青

		//目的地(正の値)
		InitSpinControl(hDlg, IDC_SPIN_MEETING_X, 0, MEETING_POS_MAX, initVal);	// X座標
		InitSpinControl(hDlg, IDC_SPIN_MEETING_Y, 0, MEETING_POS_MAX, initVal);	// Y座標
		InitSpinControl(hDlg, IDC_SPIN_MEETING_Z, 0, MEETING_POS_MAX, initVal);	// Z座標

		//目的地(負の値)
		InitSpinControl(hDlg, IDC_SPIN_MEETING_NEGATIVE_X, 0, MEETING_POS_MAX, initVal);	// X座標
		InitSpinControl(hDlg, IDC_SPIN_MEETING_NEGATIVE_Y, 0, MEETING_POS_MAX, initVal);	// Y座標
		InitSpinControl(hDlg, IDC_SPIN_MEETING_NEGATIVE_Z, 0, MEETING_POS_MAX, initVal);	// Z座標
	
		//回転軸	※初期値(0,1,0)
		InitSpinControl(hDlg, IDC_SPIN_ROTATION_X, 0, ANGLE_MAX, initVal);	// X
		InitSpinControl(hDlg, IDC_SPIN_ROTATION_Z, 0, ANGLE_MAX, initVal);	// Z
		initVal = 1;
		InitSpinControl(hDlg, IDC_SPIN_ROTATION_Y, 0, ANGLE_MAX, initVal);	// Y

		//生成方向(正の値)	初期値(1,0,0)
		InitSpinControl(hDlg, IDC_SPIN_CREATE_DIR_Y, 0, ANGLE_MAX, initVal);	// Y
		initVal = 0;
		InitSpinControl(hDlg, IDC_SPIN_CREATE_DIR_X, 0, ANGLE_MAX, initVal);	// X
		InitSpinControl(hDlg, IDC_SPIN_CREATE_DIR_Z, 0, ANGLE_MAX, initVal);	// Z

		//生成方向(負の値)	初期値(0,0,0)
		InitSpinControl(hDlg, IDC_SPIN_CREATE_DIR_NEGATIVE_X, 0, ANGLE_MAX, initVal);	// Y
		InitSpinControl(hDlg, IDC_SPIN_CREATE_DIR_NEGATIVE_Y, 0, ANGLE_MAX, initVal);	// X
		InitSpinControl(hDlg, IDC_SPIN_CREATE_DIR_NEGATIVE_Z, 0, ANGLE_MAX, initVal);	// Z


		//拡散度
		initVal = 30;
		InitSpinControl(hDlg, IDC_SPIN_DIFFUSION, 0, ANGLE_MAX, initVal);

		//コンボボックスに項目(テキスト追加)
		SendMessage(GetDlgItem(hDlg, IDC_COMBO_TEXTURE_TYPE), CB_INSERTSTRING, 0, (LPARAM)"〇");
		SendMessage(GetDlgItem(hDlg, IDC_COMBO_TEXTURE_TYPE), CB_INSERTSTRING, 1, (LPARAM)"〇(透明)");
		SendMessage(GetDlgItem(hDlg, IDC_COMBO_TEXTURE_TYPE), CB_INSERTSTRING, 2, (LPARAM)"〇(ガウス)");
		SendMessage(GetDlgItem(hDlg, IDC_COMBO_TEXTURE_TYPE), CB_INSERTSTRING, 3, (LPARAM)"ト音記号");
		SendMessage(GetDlgItem(hDlg, IDC_COMBO_TEXTURE_TYPE), CB_INSERTSTRING, 4, (LPARAM)"蝶");

		//CB_SETCURSEL:最初に起動したときに入っている値を設定
		SendMessage(GetDlgItem(hDlg, IDC_COMBO_TEXTURE_TYPE), CB_SETCURSEL, 0, 0);

		return TRUE;

	//ダイアログの閉じるボタンが押されたら
	case WM_CLOSE:
		//PostQuitMessage(0);	//プログラム終了　※ウィンドウ閉じる = プログラム終了にはならない
		DestroyWindow(hDlg);	//ダイアログのウィンドウを閉じる	※モーダルダイアログとモードレスダイアログで使う関数は違うらしい
		return TRUE;

	//クリックやキー入力が行われたら
	case WM_COMMAND:
	
		//INITより先にここに来ちゃうので、
		//パーティクルのクラスができるまで待つ
		if (SceneManager::GetCurrentScene() != nullptr)
		{
			if (((Particle*)SceneManager::GetCurrentScene()->GetChild(1)) != nullptr)
			{
				return ((Particle*)SceneManager::GetCurrentScene()->GetChild(1))->CommandProcess(hDlg, wp);
			}
		}

		return TRUE;

	}

	//何もしていないとfalseを返す
	return FALSE;
}

//リソースのスピンコントールの上限下限、エディットボックスの初期値を設定
//引数 : hDlg ダイアログハンドル
//引数 : id   リソースのiD
//引数 : wp   エディットボックスの下限値
//引数 : lp   エディットボックスの上限値
//引数 : val  エディットボックスの初期値に設定したい値
void InitSpinControl(HWND hDlg, int id, WPARAM wp, LPARAM lp, int val)
{
	SendMessage(GetDlgItem(hDlg, id), UDM_SETRANGE32, wp, lp);
	SendMessage(GetDlgItem(hDlg, id), UDM_SETPOS32, wp, (LPARAM)val);
}