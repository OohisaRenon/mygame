#include "Quad.h"
#include "Direct3D.h"


Quad::Quad() : pVertexBuffer_(nullptr) , pIndexBuffer_(nullptr) , pTexture_(nullptr), material_({ 0 })
{
}

Quad::~Quad()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}

void Quad::Load(const char* pPicture)
{
	//頂点の座標と色の情報
	Vertex vertexList[] = {
		D3DXVECTOR3(-1,  1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 0),		//ポリゴンの頂点座標(x,x,x), テクスチャを張り付けるUV座標(x,x,x)  
		D3DXVECTOR3( 1,  1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 0),
		D3DXVECTOR3( 1, -1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1, 1),
		D3DXVECTOR3(-1, -1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0, 1),
	};

	//頂点情報を入れとくバッファを確保
	Direct3D::pDevice->CreateVertexBuffer(sizeof(vertexList), 0,
		D3DFVF_XYZ/*頂点の座標*/ | D3DFVF_NORMAL/*法線情報*/ | D3DFVF_TEX1/*UV情報*/, D3DPOOL_MANAGED, &pVertexBuffer_, 0);
	//D3DFVFを書くときは宣言された順番で書く

	Vertex *vCopy;	//今から書き込む場所
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(vertexList));		//メモリの中身をそのままコピーする(今回は配列だが、構造体とかでも可)
	pVertexBuffer_->Unlock();							//解放処理

	assert(pVertexBuffer_ != nullptr);

	//頂点が時計回りになるように書かないと色が書かれていない裏面が表示される
	//3の倍数になるようにしないと表示できない
	//※ゲーム用のポリゴンは三角形の面を2つ合わせて四角形にする
	int indexList[] = { 0, 2, 3, 0, 1, 2 };

	//インデックス情報(特殊な領域)
	Direct3D::pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32,
		D3DPOOL_MANAGED, &pIndexBuffer_, 0);
	assert(pIndexBuffer_ != nullptr);
	DWORD *iCopy;
	pIndexBuffer_->Lock(0, 0, (void**)&iCopy, 0);
	memcpy(iCopy, indexList, sizeof(indexList));
	pIndexBuffer_->Unlock();

	//テクスチャ作成
	//画像サイズは2の乗数にする
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, pPicture,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);

	assert(pTexture_ != nullptr);


	//マテリアルの色設定
	material_.Diffuse.r = 1.0f;		//白(r:1,g:1,b:1)
	material_.Diffuse.g = 1.0f;
	material_.Diffuse.b = 1.0f;

}

void Quad::Draw(const D3DXMATRIX &matrix)
{
	//変形した行列をセットしてから描画の処理に移る
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);

	//テクスチャをセット
	Direct3D::pDevice->SetTexture(0, pTexture_);

	//マテリアルをセット
	Direct3D::pDevice->SetMaterial(&material_);

	//表示したい頂点バッファとインデックスバッファを指定
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
	Direct3D::pDevice->SetIndices(pIndexBuffer_);

	//ポリゴン座標部分とUV座標の部分でビット演算
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

	//第一引数:並べるポリゴンの形(基本三角形)
	//第四引数:表示したい頂点数
	//第六引数:何枚のポリゴンを出すか
	Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
}
