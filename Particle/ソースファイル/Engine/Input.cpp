#include <assert.h>
#include "Input.h"

//クラスみたいにInput::Initialize()と書いてもいい
namespace Input
{

	LPDIRECTINPUT8   pDInput = nullptr;		//この変数はInput以外で使わないのでcppで宣言
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;

	//BYTE:unsigned char型と同じ,各キーに割り当てる
	BYTE keyState[256] = { 0 };		//現在各キーが押されているかどうか
	BYTE prevKeyState[256];    //前フレームで各キーが押されているかどうか

	void Initialize(HWND hWnd)
	{
		//ダイレクトインプットを生成して初期化
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput, nullptr);
		assert(pDInput != nullptr );

		pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);		//キーボードからの入力受付
		assert(pKeyDevice != nullptr);
		pKeyDevice->SetDataFormat(&c_dfDIKeyboard);			//キーボード、マウス、コントローラーのどれか指定
		pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);	//裏で動かしてるクロームとかに入力した時ゲーム内でも入力が反映させるかどうか
	}

	void Update()
	{
		//memcpy  第一:コピー先、第二:コピー元、第三:コピーサイズ
		memcpy(prevKeyState, keyState, sizeof(keyState));

		pKeyDevice->Acquire();		//キーボード見失ったときもう一度探す
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);	//この関数が実行されたときのすべてのキーが押されたかどうかの情報が入る
	}

	bool IsKey(int keyCode)
	{
		if (keyState[keyCode] & 0x80)	//入力されたキーが押されているかどうか
		{
			return true;
		}
		return false;
	}

	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if ( IsKey(keyCode) && (prevKeyState[keyCode] & 0x80) == 0 )
		{
			return true;
		}
		return false;
	}

	bool IsKeyUp(int keyCode)
	{
		//前回は押していて、今は押していない
		if ((prevKeyState[keyCode] & 0x80) && (keyState[keyCode] & 0x80) == 0)
		{
			return true;
		}
		return false;
	}

	void Release()
	{
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}

}
