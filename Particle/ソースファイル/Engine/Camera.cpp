#include "Camera.h"


//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"), target_(0, 0, 0)
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{

	//プロジェクション行列生成
	//D3DXMatrixPerspectiveFovLH : プロジェクション行列
	//第一引数 : 書き込む行列　第二引数 : 画角(カメラの見える範囲,ズーム度)※数値いじると遠近感が変わる(低いと遠近感なくなり、平らに見える)
	//第三引数 : アスペクト比(ウィンドウの比率)　第四引数 : カメラの描画できる手前座標の上限(ニアクリッピング面までの距離)
	//第五引数 : カメラの描画できる奥の座標の上限(ファークリッピング面までの距離)  ニアクリッピング面とファークリッピング面は近いほうがいい
	D3DXMATRIX proj;
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 1.0f, 1000.0f);
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{
	Transform();

	//ビュー行列作成
	D3DXVECTOR3 worldPosition, worldTarget;

	//カメラ動かすときに使う
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	//D3DXMatrixLookAtLH : ビュー行列を作るための関数
	//第一引数 : 書き込む行列　第二引数 : カメラ(視点)の位置
	//第三引数 : 焦点、注視点(どこを見るか)　第四引数 : カメラそのもの向き(どっちが上向きか)※デフォルトの縦撮りは(0,1,0)
	D3DXMATRIX view;
	//D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 3, -12), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));		//定点カメラ
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));		//カメラを動かせる
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);		//SetTransform 第一引数 : どの変形行列か指定 第二引数 : どの行列を割り当てるか
}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}

//視点のセッター
//引数：target   設定したいカメラの視点
void Camera::SetTarget(D3DXVECTOR3 target)
{
	target_ = target;
}

//視点のゲッター
//戻り値：現在のカメラの視点
D3DXVECTOR3 Camera::GetTarget()
{
	return target_;
}
