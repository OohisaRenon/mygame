#include "BoxCollider.h"
#include "Direct3D.h"

//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
BoxCollider::BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size)
{
	center_ = basePos;
	size_ = size;

	//コライダーの中心から面までの各軸の長さ
	axisLen_[BOX_AXIS_X] = size.x / 2;
	axisLen_[BOX_AXIS_Y] = size.y / 2;
	axisLen_[BOX_AXIS_Z] = size.z / 2;

	//↑をベクトルにしたもの
	axisVec_[BOX_AXIS_X].x += axisLen_[BOX_AXIS_X];
	axisVec_[BOX_AXIS_Y].y += axisLen_[BOX_AXIS_Y];
	axisVec_[BOX_AXIS_Z].z += axisLen_[BOX_AXIS_Z];

	for (int i = 0; i < BOX_AXIS_MAX; i++)
	{
		//shaftVecを単位ベクトルに変換
		D3DXVec3Normalize(&axisVec_[i], &axisVec_[i]);
	}

	type_ = COLLIDER_BOX;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice, size.x, size.y, size.z, &pMesh_, 0);
#endif
}

//引数で指定した軸の長さのゲッター
//引数：shaft  x,y,zどの軸か
float BoxCollider::GetAxisLen(int axis)
{
	return axisLen_[axis];
}

//引数で指定した軸のベクトルのゲッター
//引数：shaft  x,y,zどの軸か
D3DXVECTOR3 BoxCollider::GetAxisVec(int shaft)
{
	return axisVec_[shaft];
}


//OBBコライダーの大きさのゲッター
D3DXVECTOR3 BoxCollider::GetSize()
{
	return size_;
}

//軸の長さのセッター
//引数：len   回転、拡大縮小後のx,y,z軸の長さ
//引数：num   Axisの配列の添え字( x = 0, y = 1, z = 0 )
void BoxCollider::SetAxisLen(float len, int num)
{
	axisLen_[num] = len;
}

//軸の長さのセッター
//引数：vec   回転、拡大縮小後のx,y,z軸の長さ
//引数：num   Axisの配列の添え字( x = 0, y = 1, z = 0 )
void BoxCollider::SetAxisVec(D3DXVECTOR3 vec, int num)
{
	axisVec_[num] = vec;
}

//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool BoxCollider::IsHit(Collider* target)
{
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsBox((BoxCollider*)target, this);
	else
		return IsHitBoxVsCircle(this, (SphereCollider*)target);
}