#pragma once

//パーティクルで扱う定数値の宣言をまとめたもの
//※WinMainとParticle.cppで主に使用する


//テクスチャのファイルデータ
enum textureFile
{
	TEXTURE_SPHERE,	//ポイントスプライトの [ 形 ] を変える(円)
	TEXTURE_SPHERE_CLEAR,	//TEXTURE_SPHERE のアルファ値 0 にしたもの
	TEXTURE_SPHERE_GAUSS,	//ポイントスプライトの [ 形 ] を変える(長方形)
	TEXTURE_MARK_1,		//ト音記号
	TEXTURE_BUTTERFLY,	//蝶
	TEXTURE_MAX,
};

//アルファブレンドするときの演算子(計算方法)の指定
enum alphaBlendOperator
{
	ALPHA_BLEND_ADD,	//加算
	ALPHA_BLEND_MUL,	//乗算
	ALPHA_BLEND_MAX,
};


//ダイアログのデータが最大値を超えていないか確認する関数に引数として渡す
//確認したいデータのタイプ
enum checkDataType
{
	CHECK_TYPE_COLOR,
	CHECK_TYPE_RANDOM_COLOR,
	CHECK_TYPE_BACK_COLOR,
	CHECK_TYPE_MEETING_POINT,
	CHECK_TYPE_ROTATION_AXIS,
	CHECK_TYPE_CREATE_DIR,
	CHECK_TYPE_SIZE,
	CHECK_TYPE_SPEED,
	CHECK_TYPE_COUNT,
	CHECK_TYPE_COUNT_PER_FRAME,
	CHECK_TYPE_CLEAR_TIME,
	CHECK_TYPE_DIFFUSION,
	CHECK_TYPE_RESISTANCE,
	CHECK_TYPE_MAX,
};

//パーティクルの動きのパターン
//caseの分岐で使う
enum moveType
{
	MOVE_TYPE_DEFAULT,		//デフォルト	※広がったり、縮んだり
	MOVE_TYPE_ROOP,			//ループ用エフェクト
	MOVE_TYPE_MAX,
};

//色の設定方法
enum colorSelectType
{
	COLOR_TYPE_MANUAL,
	COLOR_TYPE_RANDOM,
	COLOR_TYPE_SELECT_RANDOM,
	COLOR_TYPE_MAX
};

//テクスチャのファイル名
const std::string fileName[TEXTURE_MAX]{ "particle", "particleClear", "particleGauss", "particleMark1", "paticleButterfly" };
const unsigned int COLOR_MAX = 255;	//マテリアルの色を変更するときなどに使用する  色の値のMAX  ※最低値は0
const int CLEAR_TIME_MAX = 6000;		//画面のクリア時間の最大値
const int MEETING_POS_MAX = 500;		//集合地点の最大値
const int RESET_RANGE_MAX = 300;	//中央から広がる系のエフェクトでのパーティクルの生存範囲
const int ANGLE_MAX = 360;			//角度の最大値
const int PARTICLE_SIZE_MAX = 30;	//サイズの最大値
const int SPEED_MAX = 30;			//速度の最大値
const unsigned int PARTICLE_COUNT = 200000;	//描画するパーティクルの数

//使わない場合もある定数シリーズ
//動きに依存する
const int RESISTANCE_MAX = 30;		//抵抗の最大値
//const float RESET_POINT = 1.0f;		//位置をリセットしたときに出す範囲

//基準になる各軸ベクトル
const D3DXVECTOR3 AXIS_X = D3DXVECTOR3(1, 0, 0);	//X
const D3DXVECTOR3 AXIS_Y = D3DXVECTOR3(0, 1, 0);	//Y
const D3DXVECTOR3 AXIS_Z = D3DXVECTOR3(0, 0, 1);	//Z