#pragma once
#include "Engine/IGameObject.h"
#include "ConstParticleData.h"

//パーティクル1つ1つが持ってる情報
struct ParticleData
{
	//SetFVFとかで設定する項目
	D3DXVECTOR3 position;	//位置
	float size;				//パーティクルのサイズ
	DWORD color;			//色

	//SetFVFとかで設定する必要のない項目
	D3DXVECTOR3 moveDir;	//どっち方向に動くか
	float speed;			//パーティクルの速度	※こっちで持たせるかクラスのメンバにするか悩む
	float elapsedTime;		//生成されてから経過した時間

	ParticleData()
	{
		//0でまとめて初期化	※それ以外の値で初期化したい場合は別途初期化
		ZeroMemory(this, sizeof(ParticleData));
		size = 3.0f;
		color = D3DCOLOR_ARGB(255, 255, 255, 255);	//真っ白
		speed = 1.0f;
		elapsedTime = 0;
	}
};

//ダイアログから受け取った値を入れとく構造体
struct DialogData
{
	D3DXCOLOR color;			//色	引数 : (r,g,b,a)
	D3DXCOLOR randomColorMin;	//ランダムで色を決める場合の最小値
	D3DXCOLOR backColor;		//背景色	引数 : (r,g,b,a)
	D3DXVECTOR3 meetingPoint;	//目的地	※パーティクルが集合する位置
	D3DXVECTOR3 rotationAxis;	//回転させるときの回転軸
	D3DXVECTOR3 createDir;		//パーティクルを出したい方向 
	int size;					//サイズ
	int speed;					//速度
	int resistance;				//パーティクルを動かすときに扱う抵抗の値	※使わない場合もある
	unsigned int count;			//描画する個数
	unsigned int clearTime;		//画面のクリア時間(フレーム単位)
	unsigned int diffusion;		//拡散度
	unsigned int countPerFrame;	//1フレームあたりに何個ずつパーティクルを描画するか
	textureFile textureID;		//テクスチャのID
	alphaBlendOperator alphaBlend;	//アルファブレンドの計算方法
	colorSelectType colorSelectType;	//色の設定方法
	bool isZWrite;				//Zバッファへ深度を書き込むかどうか	(ソートをするかどうか)
	bool isRotate;				//回転させる or させない
	bool isMoveDiffusion;		//パーティクルの動き方(進行方向ベクトル)	拡散 or 収縮

	DialogData()
	{
		color = D3DXCOLOR(255, 255, 255, 255);	//(r,g,b,a)	※初期値「白」
		randomColorMin = D3DXCOLOR(1, 1, 1, 1);	//最低値なのでとりあえず「 0 」
		backColor = D3DXCOLOR(0, 0, 0, 255);	//(r,g,b,a)	※初期値「黒」
		meetingPoint = D3DXVECTOR3(0, 0, 0);	//集合地点
		rotationAxis = D3DXVECTOR3(0, 1, 0);	//回転させるときの回転軸
		createDir = D3DXVECTOR3(0, 1, 0);		//パーティクルを出したい方向
		size = 3;
		speed = 1;
		resistance = 1;
		count = 0;
		clearTime = 0;
		diffusion = 30;
		countPerFrame = 1;
		textureID = TEXTURE_SPHERE;		//ポイントスプライトの [ 形 ] を変える(円)
		alphaBlend = ALPHA_BLEND_ADD;	//加算合成
		colorSelectType = COLOR_TYPE_MANUAL;
		isZWrite = true;
		isRotate = false;
		isMoveDiffusion = true;
	}
};

//パーティクルを管理するクラス
//※一つ一つの点ではなく、画面に描画する点すべてを1クラス(1オブジェクトとしてみる)
class Particle : public IGameObject
{
	//どうやって画面に表示するか
	//HLTLで書いた処理がここに入る
	LPD3DXEFFECT pEffect_;

	ParticleData* particleList_;	//描画するパーティクルの配列
	LPDIRECT3DTEXTURE9* pParticleTexture_;		//パーティクル用テクスチャ
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;		//頂点バッファ

	DialogData dialogData_;			//ダイアログから受け取った値を一時的に入れとく

	D3DXVECTOR3 meetingPoint_;		//集合地点	※パーティクルを動かすときに使う
	moveType moveType_;				//パーティクルの動きのパターンごとに使う値を入れ替える

	//放射状に出すときの範囲指定と回転軸を設定する
	D3DXMATRIX diffMat_;	//拡散させる範囲を設定する行列	※↓二つを合成したもの
	D3DXMATRIX diffMatRY_;	//↓で設定した二次元での拡散範囲を3次元に拡張する	(Z軸での拡散範囲, 円錐状)
	D3DXMATRIX diffMatRZ_;	//拡散させる角度を二次元で設定する	(X軸での拡散範囲, 逆三角形)	

	D3DXMATRIX moveDirMat_;	//生成方向を決める行列 (↑で設定した生成範囲を好きな方向へ出せるようにする)
	float aliveTime_;			//1フレームごとに時間を計測
	unsigned int drawCount_;//描画する個数を制御する	※ダイアログの値になるまで毎フレーム加算していく

	//衛星軌道のように回転
	//座標を3次元極座標系に変換する
	float spiralRadius;	//半径
	float spiralHeight;	//高さ
	float spiralAngleY;	//回転角度	(Y軸)
	float spiralAngleZ;	//回転角度	(Z軸)

public:
	//コンストラクタ
	Particle(IGameObject* parent);

	//デストラクタ
	~Particle();

	//初期化
	void Initialize() override;

	//エフェクトの初期化
	void InitEffect();

	//テクスチャのデータを読み込んでセット
	void LoadTextureData();

	//パーティクルの初期位置や初期速度を設定する
	void InitParticle();

	//パーティクルをソートする
	//※パーティクル同士が重なった時正常にアルファブレンドの計算がされない場合があるので
	void CameraDistSort();

	//パーティクルを動かすための頂点情報を初期化
	void InitVartex();

	//更新
	void Update() override;

	//描画するパーティクルの個数が予期せぬ値になっていないか確認
	void CheckDrawCount();

	//色を変える処理
	//引数 : index 色を変えたいパーティクルの番号を指定
	//※↑基本的にループのカウントが入る
	void ColorChange(int index);

	//パーティクルを動かす
	//引数 : index ループで使っている配列の添え字
	void MoveParticles(int index);

	//行列生成して進行方向ベクトルを初期化
	//引数 : index パーティクルリストの添え字
	void InitMoveVec(int index);

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ダイアログの操作を受けたリソースのIDを受け取って処理を分ける
	// hDlg : 対象とするダイアログプロシージャ
	//  wp  : 操作されたリソースのID
	bool CommandProcess(HWND hDlg, WPARAM wp);

	//ダイアログから受け取った値が最大値を超えていないかチェックする
	//引数 : dialog 確認したいデータの入った構造体	※構造体のサイズが大きいので参照で渡す
	//引数 : type 確認したい項目
	void CheckDialogData(DialogData &dialog, checkDataType type);
};