#include "Torus.h"
#include "Engine/Model.h"
#include "Engine/Direct3D.h"

//コンストラクタ
Torus::Torus(IGameObject * parent)
	:IGameObject(parent, "Torus"), hModel_(-1), pEffect_(nullptr), pToonTex_(nullptr), pCubeTex_(nullptr), pRoughTex_(nullptr)
{
}

//デストラクタ
Torus::~Torus()
{
	SAFE_RELEASE(pRoughTex_);
	SAFE_RELEASE(pCubeTex_);
	SAFE_RELEASE(pToonTex_);
	SAFE_RELEASE(pEffect_);
}

//初期化
void Torus::Initialize()
{
	//シェーダー作成時に、作成失敗したときそのメッセージが入る
	LPD3DXBUFFER err = 0;

	//HLSLで書いたシェーダーをロード
	//第一引数：デバイスオブジェクト
	//第二引数：使いたいシェーダーのファイル名(パス)
	//第七引数：格納先のポインタ
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice,
		"CommonShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_, &err
	)))
	{
		//シェーダー作成失敗したときにテキストボックス表示
		//第一引数：デバイスオブジェクト
		//第二引数：エラーメッセージの内容
		//第三引数：ステータスバーに表示する内容
		//第四引数：なんのボタンを出すか
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダー作成エラー", MB_OK);
	}

	////トゥーンレンダリング用の影のテクスチャ作成
	//D3DXCreateTextureFromFileEx(Direct3D::pDevice_, "Data/toon.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
	//	D3DX_FILTER_NONE, D3DX_DEFAULT, 0, nullptr, nullptr, &pToonTex_);

	//モデルデータのロード
	//使いたいシェーダーも引数で渡す
	//hModel_ = Model::Load("data/zunko/zunko.fbx", pEffect_);
	hModel_ = Model::Load("data/Torus2.fbx", pEffect_);
	assert(hModel_ >= 0);

	Model::SetEffect(pEffect_, hModel_);

	assert(pEffect_ != nullptr);

	//環境マッピング用の画像をロード
	D3DXCreateCubeTextureFromFile(Direct3D::pDevice, "Data/Texture3.dds", &pCubeTex_);
	assert(pCubeTex_ != nullptr);

	D3DXCreateTextureFromFileEx(Direct3D::pDevice, "Data/Roughness.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
	D3DX_FILTER_NONE, D3DX_DEFAULT, 0, nullptr, nullptr, &pRoughTex_);
	assert(pRoughTex_ != nullptr);
	
}

//更新
void Torus::Update()
{
	//static float count = 0.0f;
	//count += 0.1f;
	////サイン関数(-1 〜 1)の値を生成
	//scale_.x = (sin(count) + 2);
	rotate_.y += 2.0f;

	if (Input::IsKeyDown(DIK_SPACE))
	{
		KillMe();
	}

	if (Input::IsKey(DIK_W))
	{
		position_.z += 0.1f;
	}

	if (Input::IsKey(DIK_A))
	{
		position_.x -= 0.1f;
	}

	if (Input::IsKey(DIK_D))
	{
		position_.x += 0.1f;
	}

	if (Input::IsKey(DIK_S))
	{
		position_.z -= 0.1f;
	}

}

//描画
void Torus::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);

	//プロジェクション行列
	D3DXMATRIX proj;
	//最後に設定したプロジェクション行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//ビュー行列
	D3DXMATRIX view;
	//最後に設定したビュー行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//シェーダーに渡すための行列を合成
	//ワールド x ビュー x プロジェクション
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//シェーダー側に合成した行列をセット
	//第1引数：HLSLのグローバル変数名
	//第2引数：オブジェクト側から渡す行列
	pEffect_->SetMatrix("WVP", &matWVP);

	//いったんワールド行列をコピー
	D3DXMATRIX mat = worldMatrix_;

	//ワールド行列から移動行列だけ消す
	//mat._41 = 0;		//   _1_2_3_4
	//mat._42 = 0;		// 1| 1 0 0 0
	//mat._43 = 0;		// 2| 0 1 0 0
						// 3| 0 0 1 0
						// 4| 0 0 0 1

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	//面が横に伸びたら法線は縦向きに
	//面が縦に伸びたら法線は横向きにしたい
	//拡大縮小行列は逆行列にしたい
	D3DXMatrixInverse(&scale, nullptr, &scale);

	//回転行列と拡大縮小行列の逆行列を合成してシェーダーに渡す
	//※拡大縮小行列の逆行列をかけたので、このベクトルの長さは1じゃない
	mat = scale * rotateZ * rotateX * rotateY;
	pEffect_->SetMatrix("RS", &mat);

	////////////////////////////////////////
	//アプリ側でライトの方向を変えたとき
	//シェーダーでも反映されるように
	///////////////////////////////////////

	//ライトの方向を決めてるベクトルをDirect3Dから取ってくる
	D3DLIGHT9 lightState;
	Direct3D::pDevice->GetLight(0, &lightState);
	//ライトの方向を決めてるベクトルを渡す
	//ライト->物体へのベクトル
	//※LambertShaderは物体->ライトのベクトルを使うので反転させる必要あり
	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	////////////////////////////
	//色をアプリ側から指定する
	///////////////////////////

	//ライトから拡散反射光の情報だけ取り出す
	//D3DCOLORVALUE color = lightState.Diffuse;
	//D3DXVECTOR4 objectColor(1,0,0,1);
	//※第2引数：物体の色を渡す(Mayaで設定した色を渡す)
	//pEffect_->SetVector("DIFFUSE_COLOR", &D3DXVECTOR4(1,1,0,1));

	//カメラの位置をシェーダーに渡す
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 5, -10));

	//ワールド行列を渡す
	pEffect_->SetMatrix("W", &worldMatrix_);

	//pEffect_->SetTexture("TEXTURE_TOON", pToonTex_);

	//環境マッピング用の.dds画像をシェーダーに渡す
	pEffect_->SetTexture("TEX_CUBE", pCubeTex_);

	//環境マッピング用の.dds画像をシェーダーに渡す
	pEffect_->SetTexture("ROUGH_MAP", pRoughTex_);

	//BeginからEndまでの範囲にシェーダーを適用する
	pEffect_->Begin(NULL, 0);

	//背面カリングを切る
	//厚みのないポリゴンを表示するとき裏面も描画したい
	//Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	
	///////////////
	//輪郭表示
	///////////////

	//何番目のpassを適用するか
	//pEffect_->BeginPass(1);
	//ゼットバッファを一時的に切る(前後関係を無視)
	//Direct3D::pDevice_->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	//Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
	//Model::Draw(hModel_);
	//Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	//Direct3D::pDevice_->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	//pEffect_->EndPass();


	//実際のモデル表示
	pEffect_->BeginPass(0);
	Model::Draw(hModel_);
	pEffect_->EndPass();

	//ここでシェーダー適用終了
	pEffect_->End();
}

//開放
void Torus::Release()
{
}
