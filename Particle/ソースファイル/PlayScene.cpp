#include "PlayScene.h"
#include "Torus.h"
#include "Particle.h"
#include "Engine/Camera.h"
#include "Engine/Camera.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), pCam_(nullptr)
{

}

//初期化
void PlayScene::Initialize()
{
	//Torus* torus = CreateGameObject<Torus>(this);
	pCam_ = CreateGameObject<Camera>(this);
	pCam_->SetPosition(D3DXVECTOR3(0, 0, -200));
	//Particle* pParticle = CreateGameObject<Particle>(this);
	CreateGameObject<Particle>(this);

}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{

}

//開放
void PlayScene::Release()
{
}

//カメラオブジェクトのゲッター
//戻り値 : カメラオブジェクト
Camera* PlayScene::GetCamObject()
{
	return pCam_;
}
