#pragma once
#include "Engine/IGameObject.h"

//ドーナツを管理するクラス
class Torus : public IGameObject
{
	int hModel_;

	//どうやって画面に表示するか
	//HLTLで書いた処理がここに入る
	LPD3DXEFFECT pEffect_;

	//トゥーンレンダリング用の影の画像
	LPDIRECT3DTEXTURE9 pToonTex_;

	//環境マッピング用の画像
	LPDIRECT3DCUBETEXTURE9 pCubeTex_;

	//ラフネス用の画像
	LPDIRECT3DTEXTURE9 pRoughTex_;

public:
	//コンストラクタ
	Torus(IGameObject* parent);

	//デストラクタ
	~Torus();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};